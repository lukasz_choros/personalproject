var startedHand =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
 'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S', '83S', '82S',
 'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S', '72S',
 'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S', '62S',
 'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '95O', '85O', '75O', '65O', '55', '54S', '53S', '52S',
 'A4O', 'K4O', 'Q4O', 'J4O', 'T4O', '94O', '84O', '74O', '64O', '54O', '44', '43S', '42S',
 'A3O', 'K3O', 'Q3O', 'J3O', 'T3O', '93O', '83O', '73O', '63O', '53O', '43O', '33', '32S',
 'A2O', 'K2O', 'Q2O', 'J2O', 'T2O', '92O', '82O', '72O', '62O', '52O', '42O', '32O', '22'
];

var EPNOB3 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S',
'A7O', 'K7O', '77', '76S', '75S',
'A6O', 'K6O', '66', '65S', '64S',
'A5O', '55', '54S',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];

var EPNOB4 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S',
'A9O', '99', '98S',
'A8O', '88',
'A7O', '77',
'A6O', '66',
'A5O', '55',
'44', '33', '22'
];

var EPNOB5 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S',
'ATO', 'KTO', 'TT', 'T9S',
'A9O', '99', '98S',
'A8O', '88',
'A7O', '77', '66', '55', '44', '33', '22'
];
var EPNOB6 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S',
'ATO', 'KTO', 'TT', 'T9S',
'A9O', '99', '98S',
'A8O', '88', '77', '66', '55', '44', '33', '22'
];
var EPNOB7 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
'AJO', 'KJO', 'JJ', 'JTS', 'J9S',
'ATO', 'TT', 'T9S',
'A9O', '99', '98S', '88', '77', '66', '55', '44', '33', '22'
];
var EPNOB8 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
'AJO', 'KJO', 'JJ', 'JTS', 'J9S',
'ATO', 'TT', 'T9S', '99', '88', '77', '66', '55', '44', '33'
];

var EPNOB9 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
'AJO', 'JJ', 'JTS', 'J9S',
'ATO', 'TT', 'T9S',
'99', '88', '77', '66', '55', '44', '33'
];
var EPNOB10 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
'AJO', 'JJ', 'JTS', 'J9S',
'ATO', 'TT', 'T9S', '99', '88', '77', '66', '55', '44'
];
var EPNOB11 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
'AJO', 'JJ', 'JTS',
'TT', 'T9S', '99', '88', '77', '66', '55'
];
var EPNOB12 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS',
'AJO', 'JJ', 'JTS',
'TT', 'T9S', '99', '88', '77', '66', '55'
];

var EPNOB1315RAISE =
['AA', 'KK', 'QQ',
];

var EPNOB1315SHOVE =
['AKS', 'AQS', 'AJS', 'ATS', 'A9S',
'AKO', 'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'AJO', 'JJ', 'JTS',
'TT', '99', '88', '77', '66', '55'
];

var EPNOB1315RAISEFOLD =
['A5S', 'A4S', 'KQO', 'T9S'
];

var EPNOB1622 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS',
'AJO', 'KJO', 'JJ', 'JTS',
'ATO', 'TT', 'T9S', '99', '88', '77', '66', '55'
];

var EPNOB2330 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS',
'AJO', 'KJO', 'JJ', 'JTS', 'J9S',
'ATO', 'TT', 'T9S', '99', '88', '77', '98S', '87S', '76S'
];

var EPNOB3250 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS',
'AJO', 'KJO', 'JJ', 'JTS', 'J9S',
'ATO', 'TT', 'T9S', '99', '88', '77', '98S', '87S', '76S', '66'
];
var EPNOB51100 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS',
'AJO', 'KJO', 'JJ', 'JTS', 'J9S',
'ATO', 'TT', 'T9S', '99', '88', '77', '98S', '87S', '76S', '66', '55', '44', '33', '22'
];


//MP SECTION
var MPNOB3 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S',
'A7O', 'K7O', '77', '76S', '75S',
'A6O', 'K6O', '66', '65S',
'A5O', 'K5O', '55', '54S',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];

var MPNOB4 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', 'K9O', '99', '98S', '97S',
'A8O', 'K8O', '88', '87S',
'A7O', '77', '76S',
'A6O', '66',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];

var MPNOB5 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'TT', 'T9S', 'T8S',
'A9O', 'K9O', '99', '98S',
'A8O', 'K8O', '88', '87S',
'A7O', '77',
'A6O', '66',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];

var MPNOB6 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S',
'ATO', 'KTO', 'TT', 'T9S', 'T8S',
'A9O', 'K9O', '99', '98S',
'A8O', '88', '87S',
'A7O', '77',
'A6O', '66',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];
var MPNOB7 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S',
'ATO', 'KTO','QTO', 'TT', 'T9S', 'T8S',
'A9O', 'K9O', '99', '98S',
'A8O', '88', '87S',
'A7O', '77',
'A6O', '66',
'A5O', '55',
'A4O', '44',
'33',
'22'
];
var MPNOB8 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S',
'ATO', 'KTO', 'QTO', 'TT', 'T9S', 'T8S',
'A9O', 'K9O', '99', '98S',
'A8O', '88', '87S',
'A7O', '77',
'A6O', '66',
'A5O', '55',
'44',
'33',
'22'
];

var MPNOB9 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S',
'ATO', 'KTO', 'QTO', 'TT', 'T9S', 'T8S',
'A9O', 'K9O', '99', '98S',
'A8O', '88', '87S',
'A7O', '77',
'A6O', '66',
'A5O', '55',
'44',
'33',
'22'
];
var MPNOB10 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S',
'ATO', 'KTO', 'QTO', 'TT', 'T9S', 'T8S',
'A9O', '99', '98S',
'A8O', '88', '87S',
'A7O', '77',
'66',
'55',
'44',
'33',
'22'
];
var MPNOB11 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S',
'ATO', 'KTO', 'TT', 'T9S', 'T8S',
'A9O', '99', '98S',
'A8O', '88',
'A7O', '77',
'66',
'55',
'44',
'33',
'22'
];
var MPNOB12 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S',
'ATO', 'KTO', 'TT', 'T9S', 'T8S',
'A9O', '99', '98S',
'A8O', '88',
'77',
'66',
'55',
'44',
'33',
'22'
];

var MPNOB1315RAISE =
['AA', 'KK', 'QQ', 'JJ'
];

var MPNOB1315SHOVE =
['AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'KQO', 'QJS', 'QTS','Q9S',
'AJO', 'KJO', 'JJ', 'JTS', 'J9S',
'ATO', 'TT', 'T9S',
'99', '98S',
'88', 
'77', 
'66', 
'55',
'44',
'33',
'22'
];

var MPNOB1315RAISEFOLD =
['QJO', 'K8S', '87S'
];

var MPNOB1622 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS','Q9S',
'AJO', 'KJO','QJO', 'JJ', 'JTS','J9S',
'ATO','KTO','QTO', 'JTO', 'TT', 'T9S','T8S',
'A9O', '99', '98S',
'88','87S',
'76S',
];

var MPNOB1622SHOVE =
['77', '66', '55'
];

var MPNOB2330 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS','Q9S',
'AJO', 'KJO','QJO', 'JJ', 'JTS','J9S',
'ATO', 'TT', 'T9S','T8S',
'99', '98S',
'88','87S',
'77', '76S',
'66'
];

var MPNOB3250 =
['AA','AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KQS', 'KK', 'KJS', 'KTS', 'K9S','K8S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S',
'ATO', 'TT', 'T9S' ,'T8S',
'99', '98S',
'88', '87S',
'77', '76S',
'66', '65S',
'55', '54S'
];

var MPNOB51100 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S','K8S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S',
'ATO', 'TT', 'T9S' ,'T8S',
'99', '98S', '97S',
'88', '87S', '86S',
'77', '76S',
'66', '65S',
'55', '54S',
'44',
'33',
'22'
];


//CO CO
var CONOB3 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S','T6S',
'A9O', 'K9O', 'Q9O','J9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', 'K8O', 'Q8O','J8O', 'T8O', '98O', '88', '87S', '86S',
'A7O', 'K7O', 'Q7O', 'J7O', '77', '76S',
'A6O', 'K6O', '66', '65S',
'A5O', 'K5O','55',
'A4O', 'K4O', '44',
'A3O', 'K3O', '33',
'A2O', 'K2O', '22'
];

var CONOB4 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S','T6S',
'A9O', 'K9O', 'Q9O','J9O', 'T9O', '99', '98S', '97S',
'A8O', 'K8O', 'Q8O','J8O', '88', '87S', 
'A7O', 'K7O', 'Q7O', 'J7O', '77', '76S',
'A6O', 'K6O', '66', '65S',
'A5O', 'K5O','55',
'A4O', 'K4O', '44',
'A3O', '33',
'A2O', '22'
];

var CONOB5 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', 'K9O', 'Q9O','J9O', 'T9O', '99', '98S', '97S',
'A8O', 'K8O', 'Q8O', '88', '87S',
'A7O', '77', 'K7O', '76S',
'A6O', 'K6O', '66',
'A5O', 'K5O','55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];

var CONOB6 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', 'K9O', 'Q9O','J9O', 'T9O', '99', '98S', '97S',
'A8O', 'K8O', '88', '87S', '86S',
'A7O', 'K7O', '77','76S',
'A6O', 'K6O', '66',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];
var CONOB7 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', 'K9O', 'Q9O', '99', '98S', '97S',
'A8O', 'K8O', '88', '87S', '86S',
'A7O','77', 'K7O', '76S',
'A6O', '66',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];
var CONOB8 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', 'K9O', '99', '98S', '97S',
'A8O','77','88', '87S', '86S',
'A7O', '76S',
'A6O', '66',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];

var CONOB9 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', 'K9O', '99', '98S', '97S',
'A8O', '88', '87S',
'A7O', '77', '76S',
'A6O', '66',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];

var CONOB10 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S',
'A9O', '99', '98S', '97S',
'A8O', '88', '87S',
'A7O', '77', '76S',
'A6O', '66',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];

var CONOB11 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S',
'A9O', '99', '98S', '97S',
'A8O', '88', '87S',
'A7O', '77', '76S',
'A6O', '66',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];

var CONOB12 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S',
'A9O', '99', '98S', '97S',
'A8O', '88', '87S',
'A7O', '77', '76S',
'A6O', '66',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];

var CONOB1315RAISE =
['AA', 'KK', 'QQ', 'AKS', 'AQS', 'AKO', 'JJ', 'TT'
];

var CONOB1315SHOVE =
['AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQS', 'KJS', 'KTS', 'K9S', 'K8S',
'AQO', 'KQO', 'QJS', 'QTS','Q9S', 'Q8S', 'J8S', 'T8S',
'AJO', 'KTO', 'KJO', 'JJ', 'JTS', 'J9S',
'ATO', 'T9S', 'QJO',
'A9O', '99', '98S',
'A8O', '88', '87S',
'A7O', '77',
'A6O', '66',
'A5O', '55',
'44',
'33',
'22'
];

var CONOB1315RAISEFOLD =
['JTO', 'K7S', 'K6S','K5S', 'QTO'
];

var CONOB1622RAISE =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S',
'AKO', 'KK', 'KQS', 'KJS', 'K8S', 'K7S',
'AQO', 'KQO', 'QQ', 'QJS', 'Q8S',
'AJO', 'JJ','J8S',
'ATO','KTO','QTO', 'JTO', 'TT','T8S', 'T7S',
'T9O', '99', '97S',
'88', '87S', '86S',
'77', '76S',
'66','65S',
'A5O', '54S'
];

var CONOB1622SHOVE =
['A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS', 'K9S',
'QTS', 'Q9S',
'KJO', 'QJO', 'JTS', 'J9S',
'T9S',
'A9O','98S',
'A8O',
'55',
'44',
'33',
'22'
];

var CONOB2330 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS','Q9S', 'Q8S',
'AJO', 'KJO','QJO', 'JJ', 'JTS','J9S', 'J8S',
'ATO', 'KTO','QTO', 'JTO','TT', 'T9S','T8S', 'T7S',
'A9O', 'T9O', '99', '98S', '97S',
'A8O', '88', '87S', '86S',
'77', '76S',
'66', '65S',
'A5O', '55', '54S',
'44',
'33',
'22'
];

var CONOB3250 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS','Q9S', 'Q8S',
'AJO', 'KJO','QJO', 'JJ', 'JTS','J9S', 'J8S',
'ATO', 'KTO','QTO', 'JTO','TT', 'T9S','T8S', 'T7S',
'A9O', 'T9O', '99', '98S', '97S',
'A8O', '88', '87S', '86S',
'77', '76S', '75S',
'66', '65S', '64S',
'55', '54S',
'44', '43S',
'33',
'22'
];

var CONOB51100 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS','Q9S', 'Q8S',
'AJO', 'KJO','QJO', 'JJ', 'JTS','J9S', 'J8S', 'J7S',
'ATO','KTO','QTO', 'JTO', 'TT', 'T9S','T8S', 'T7S',
'A9O', 'K9O', 'T9O', '99', '98S', '97S',
'A8O', '88', '87S', '86S',
'77', '76S', '75S',
'66', '65S', '64S',
'A5O', '55', '54S',
'44', '43S',
'33',
'22'
];

//BU BU
var BUNOB3 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S','T6S','T5S',
'A9O', 'K9O', 'Q9O','J9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', 'K8O', 'Q8O','J8O', 'T8O', '98O', '88', '87S', '86S',
'A7O', 'K7O', 'Q7O', 'J7O','T7O', '97O', '77', '76S',
'A6O', 'K6O', 'Q6O', '66',
'A5O', 'K5O', 'Q5O','55',
'A4O', 'K4O', 'Q4O', '44',
'A3O', 'K3O', '33',
'A2O', 'K2O', '22'
];

var BUNOB4 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S','T6S',
'A9O', 'K9O', 'Q9O','J9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', 'K8O', 'Q8O','J8O', 'T8O', '98O', '88', '87S', '86S',
'A7O', 'K7O', 'Q7O', 'J7O', '77', '76S',
'A6O', 'K6O', 'Q6O','66',
'A5O', 'K5O', 'Q5O', '55',
'A4O', 'K4O', '44',
'A3O', 'K3O', '33',
'A2O', 'K2O', '22'
];

var BUNOB5 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S','T6S',
'A9O', 'K9O', 'Q9O','J9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', 'K8O', 'Q8O','J8O', 'T8O', '88', '87S', '86S',
'A7O', 'K7O', 'Q7O', '77', '76S',
'A6O', 'K6O', 'Q6O','66',
'A5O', 'K5O', '55',
'A4O', 'K4O', '44',
'A3O', 'K3O', '33',
'A2O', 'K2O', '22'
];

var BUNOB6 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S','T6S',
'A9O', 'K9O', 'Q9O','J9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', 'K8O', 'Q8O','J8O', 'T8O', '88', '87S', '86S',
'A7O', 'K7O', 'Q7O', '77', '76S',
'A6O', 'K6O', '66', '65S',
'A5O', 'K5O', '55',
'A4O', 'K4O', '44',
'A3O', 'K3O', '33',
'A2O', 'K2O', '22'
];

var BUNOB7 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S','T6S',
'A9O', 'K9O', 'Q9O','J9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', 'K8O', 'Q8O', '88', '87S', '86S',
'A7O', 'K7O', '77', '76S',
'A6O', 'K6O', '66', '65S',
'A5O', 'K5O', '55', '54S',
'A4O', 'K4O', '44',
'A3O', 'K3O', '33',
'A2O', 'K2O', '22'
];

var BUNOB8 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S','T6S',
'A9O', 'K9O', 'Q9O','J9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', 'K8O', 'Q8O', '88', '87S', '86S',
'A7O', 'K7O', '77', '76S',
'A6O', 'K6O', '66', '65S',
'A5O', 'K5O', '55', '54S',
'A4O', 'K4O', '44',
'A3O', '33',
'A2O', '22'
];

var BUNOB9 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S','T6S',
'A9O', 'K9O', 'Q9O','J9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', 'K8O', '88', '87S', '86S',
'A7O', 'K7O', '77', '76S',
'A6O', 'K6O', '66', '65S', '54S',
'A5O', 'K5O','55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];
var BUNOB10 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', 'K9O', 'Q9O','J9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', 'K8O', '88', '87S', '86S',
'A7O', 'K7O', '77', '76S',
'A6O', 'K6O', '66', '65S',
'A5O', '55', '54S',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];

var BUNOB11 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', 'K9O', 'Q9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', 'K8O', '88', '87S', '86S',
'A7O', '77', '76S',
'A6O', '66', '65S',
'A5O', '55', '54S',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];


var BUNOB12 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', 'K9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', '88', '87S', '86S',
'A7O', '77', '76S',
'A6O', '66', '65S',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];


var BUNOB1315RAISE =
['AA', 'KK', 'QQ', 'AKS', 'AQS', 'AKO', 'JJ', 'TT'
];

var BUNOB1315SHOVE =
['AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S',
'AQO', 'KQO', 'QJS', 'QTS','Q9S', 'Q8S', 'J8S', 'T8S', 'Q7S', 'J7S', 'T7S','Q6S',
'AJO', 'KJO', 'JJ', 'JTS', 'J9S', 'KTO','76S','86S',
'ATO', 'T9S', 'K9O', 'QJO', 'QTO', 'JTO','T9O', '97S', '65S',
'A9O', '99', '98S',
'A8O', '88', '87S',
'A7O', '77',
'A6O', '66',
'A5O', '55',
'A4O','44',
'A3O', '33',
'A2O', '22'
];

var BUNOB1315RAISEFOLD =
['K3S', 'K2S', 'Q5S'
];

var BUNOB1622RAISE =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S',
'AKO', 'KK', 'KQS', 'KJS', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'Q7S', 'Q6S',
'AJO', 'JJ','J7S', 'JTS',
'ATO', 'TT', 'T7S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '96S',
'K8O', 'Q8O', '98O', '88', '86S',
'77', '75S',
'66','65S', '64S',
'54S'
];

var BUNOB1622SHOVE =
['A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS', 'K9S', 'K8S', 'K7S',
'QTS', 'Q9S', 'Q8S',
'KJO', 'QJO', 'J8S', 'J9S',
'KTO', 'QTO', 'JTO', 'J8S', 'J9S',
'T9S','T8S',
'A9O','98S','97S','87S','86S', '76S',
'A8O',
'55',
'44',
'33','A7O','A6O','A5O','A4O','A3O','A2O',
'22'
];

var BUNOB2330RAISE =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 
'ATO', 'KTO', 'QJO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S',
'A8O', 'K8O', 'Q8O', '98O', '88', '87S', '86S',
'A7O', '76S', '75S',
'A6O', '66', '65S', '64S',
'A5O', '55', '54S',
'A4O', '43S','77',
'A3O',
'A2O',
];
var BUNOB2330SHOVE =
[
'55',
'44',
'33',
'22'
];

var BUNOB3250 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S',
'A7O', '87O', '77', '76S', '75S',
'A6O', '66', '65S', '64S',
'A5O', '55', '54S',
'A4O', '44', '43S',
'A3O', '33',
'A2O', '22'
];

var BUNOB51100 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S',
'A7O', '87O', '77', '76S', '75S', '74S',
'A6O', '66', '65S', '64S', '63S',
'A5O', '55', '54S', '53S',
'A4O', '44', '43S',
'A3O', '33','32S',
'A2O', '22'
];

//SB SB
var SBNOB3 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S', '83S', '82S',
'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S', '72S',
'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S', '62S',
'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '95O', '85O', '75O', '65O', '55', '54S', '53S', '52S',
'A4O', 'K4O', 'Q4O', 'J4O', 'T4O', '94O', '84O', '74O', '64O', '54O', '44', '43S', '42S',
'A3O', 'K3O', 'Q3O', 'J3O', 'T3O', '93O', '83O', '73O', '63O', '53O', '43O', '33', '32S',
'A2O', 'K2O', 'Q2O', 'J2O', 'T2O', '92O', '82O', '72O', '22'
];

var SBNOB4 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
 'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S', '83S', '82S',
 'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S',
 'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S',
 'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '95O', '85O', '75O', '65O', '55', '54S', '53S',
 'A4O', 'K4O', 'Q4O', 'J4O', 'T4O', '94O', '84O', '44', '43S',
 'A3O', 'K3O', 'Q3O', 'J3O', 'T3O', '93O', '33',
 'A2O', 'K2O', 'Q2O', 'J2O', 'T2O', '22'
];

var SBNOB5 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
 'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S', '83S', '82S',
 'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S',
 'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S',
 'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '95O', '85O', '75O', '65O', '55', '54S', '53S',
 'A4O', 'K4O', 'Q4O', 'J4O', 'T4O', '44', '43S',
 'A3O', 'K3O', 'Q3O', 'J3O', 'T3O', '33',
 'A2O', 'K2O', 'Q2O', 'J2O', 'T2O', '22'
];

var SBNOB6 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
 'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S', '83S',
 'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S',
 'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S',
 'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '95O', '75O', '65O', '55', '54S', '53S',
 'A4O', 'K4O', 'Q4O', 'J4O', 'T4O', '44', '43S',
 'A3O', 'K3O', 'Q3O', 'J3O', '33',
 'A2O', 'K2O', 'Q2O', 'J2O', '22'
];

var SBNOB7 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
 'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S',
 'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S',
 'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S',
 'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '65O', '55', '54S', '53S',
 'A4O', 'K4O', 'Q4O', 'J4O', '44', '43S',
 'A3O', 'K3O', 'Q3O', 'J3O', '33',
 'A2O', 'K2O', 'Q2O', 'J2O', '22'
];

var SBNOB8 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
 'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S',
 'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S',
 'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S',
 'A5O', 'K5O', 'Q5O', 'J5O', '65O', '55', '54S', '53S',
 'A4O', 'K4O', 'Q4O', 'J4O', '44', '43S',
 'A3O', 'K3O', 'Q3O', 'J3O', '33',
 'A2O', 'K2O', 'Q2O', '22'
];

var SBNOB9 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S',
 'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S',
 'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S',
 'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S',
 'A5O', 'K5O', 'Q5O', 'J5O', '65O', '55', '54S', '53S',
 'A4O', 'K4O', 'Q4O', 'J4O', '44', '43S',
 'A3O', 'K3O', 'Q3O', '33',
 'A2O', 'K2O', 'Q2O', '22'
];

var SBNOB10 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S',
 'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S',
 'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S',
 'A6O', 'K6O', 'Q6O', 'J6O',  '86O', '76O', '66', '65S', '64S', '63S',
 'A5O', 'K5O', 'Q5O',  '65O', '55', '54S', '53S',
 'A4O', 'K4O', 'Q4O', '44', '43S',
 'A3O', 'K3O', 'Q3O', '33',
 'A2O', 'K2O', 'Q2O', '22'
];

var SBNOB11 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S',
 'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S',
 'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S',
 'A6O', 'K6O', 'Q6O', '86O', '76O', '66', '65S', '64S', '63S',
 'A5O', 'K5O', 'Q5O',  '65O', '55', '54S', '53S',
 'A4O', 'K4O', 'Q4O', '44', '43S',
 'A3O', 'K3O', 'Q3O', '33',
 'A2O', 'K2O', 'Q2O', '22'
];


var SBNOB12 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S',
 'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S',
 'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S',
 'A6O', 'K6O', 'Q6O', '76O', '66', '65S', '64S', '63S',
 'A5O', 'K5O', 'Q5O', '55', '54S', '53S',
 'A4O', 'K4O', 'Q4O', '44', '43S',
 'A3O', 'K3O', '33',
 'A2O', 'K2O', '22'
];



var SBNOB1315 =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S',
 'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S',
 'A7O', 'K7O', 'Q7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S',
 'A6O', 'K6O', 'Q6O', '76O', '66', '65S', '64S', '63S',
 'A5O', 'K5O', 'Q5O', '55', '54S', '53S',
 'A4O', 'K4O', '44', '43S',
 'A3O', 'K3O', '33',
 'A2O', 'K2O', '22'
];

var SBNOB1622LIMPFOLD =
['K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
 'Q8O', 'J8O', 'T8O', '98O', '87S', '86S', '85S', '84S', '83S', '82S',
 'Q7O', 'J7O', 'T7O', '97O', '87O', '76S', '75S', '74S', '73S', '72S',
 '76O', '65S', '64S', '63S', '62S',
 '65O', '54S', '53S', '52S',
 '43S', '42S',
 '32S',
];

var SBNOB1622RAISE =
['AA', 'AKS', 'AQS', 'AJS', 'ATS',
 'AKO', 'KK', 'KQS',
 'AQO', 'KQO', 'QQ',
 'AJO', 'JJ',
 'ATO', 'TT',
 '99',
 '88',
 'K6O',
 'K5O',
 'K4O',
 'K3O',
 'K2O',
];

var SBNOB1622SHOVE =
['A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'KJS', 'KTS', 'K9S',
 'QJS', 'QTS',
 'KJO', 'QJO', 'JJ', 'JTS',
 'KTO', 'QTO', 'JTO', 'T9S',
 'A9O', 'K9O',
 'A8O', 'K8O',
 'A7O', 'K7O', '77',
 'A6O', '66',
 'A5O', '55',
 'A4O', '44',
 'A3O', '33',
 'A2O', '22'
];

var SBNOB1622RAISEFOLD =
[
 'Q6O', 'J6O', 'T6O', '96O', '86O',
 'Q5O', '75O',
 'Q4O',
 'Q3O',
 'Q2O',
];

var SBNOB2330RAISE =
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S',
 'AKO', 'KK', 'KQS', 'KJS',
 'AQO', 'KQO', 'QQ',
 'AJO', 'JJ',
 'ATO', 'TT',
 '99',
 '88',
 '77',
 '66',
];

var SBNOB2330SHOVE = 
['44', '33', '22'
]
var SBNOB2330RAISESHOVE = 
['55'
]


var SBNOB2330LIMPFOLD =
[
"K8S","K7S","K6S","K5S","K4S","K3S","K2S","Q2S","Q3S","Q4S","Q5S","Q6S","Q7S","Q8S","J8S","J7S","J6S","J5S","J4S","J3S","J2S","T2S","T3S","T4S","T5S","T6S","T7S","97S","96S","95S","94S","93S","92S","82S","83S","84S","85S","86S","76S","75S","74S","73S","72S","62S","63S","64S","65S","54S","53S","52S","42S","43S","32S","A2O","A3O","A4O","A5O","K5O","K6O","A6O","A7O","K7O","K8O","A8O","Q8O","Q7O","J7O","J8O","T8O","T7O","97O","98O","87O","76O","65O"
];

var SBNOB2330LIMP =
['A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'KTS', 'K9S',
 'AQO', 'KQO', 'QJS', 'QTS', 'Q9S',
 'AJO', 'KJO', 'QJO', 'JTS', 'J9S',
 'ATO', 'KTO', 'QTO', 'JTO', 'T9S', 'T8S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S',
 '87S'
];

var SBNOB2330RAISEFOLD =
[ 'Q6O', 'J6O', 'T6O', '96O', '86O',
 'Q5O', '75O',
 'K4O',
 'K3O',
 'K2O',
 'Q4O',
 'Q3O',
 'Q2O'
];


var SBNOB3250RAISE =
["AA","AKS","AQS","AJS","ATS","A9S","A8S","A7S","K9S","KTS","KJS","KQS","KK","AKO","AQO","KQO","QQ","QJS","QTS","JTS","JJ","QJO","KJO","AJO","ATO","KTO","TT","A9O","99","88","77","66","55","44","33","22"
];


var SBNOB3250LIMPFOLD =
[
'A5S','A4S','A3S','A2S','K4O','K3O','K2O',"K6S","K5S","K4S","K3S","K2S","Q2S","Q3S","Q4S","Q5S","Q6S","Q7S","J7S","J6S","J5S","J4S","J3S","J2S","T2S","T3S","T4S","T5S","T6S","T7S","97S","96S","95S","94S","93S","92S","82S","83S","84S","85S","86S","76S","75S","74S","73S","72S","62S","63S","64S","65S","54S","53S","52S","42S","43S","32S","A2O","A3O","A4O","A5O","K5O","K6O","A6O","A7O","K7O","K8O","Q8O","Q7O","J7O","J8O","T8O","T7O","97O","98O","87O","76O","65O"
];

var SBNOB3250LIMP =
['A8O',"A6S","K8S","K7S","Q9S","Q8S","J9S","J8S","T9S","T8S","98S","87S","QTO","JTO","K9O","Q9O","J9O","T9O","A8O"];

var SBNOB3250RAISEFOLD =
[ 'Q6O', 'J6O', 'T6O', '96O', '86O',
 'Q5O', '85O','75O', '64O','54O',
 'Q4O',
 'Q3O',
 'Q2O'
];



var SBNOB51100RAISE =
["AA","AKS","AQS","AJS","ATS","A9S","A8S","A7S",'A6S','T9S',
"K9S","KTS","KJS","KQS","KK","AKO","AQO","KQO","QQ","QJS",
"QTS","JTS","JJ","QJO","KJO","AJO","ATO",'A9O','A8O',"KTO","TT","A9O",
"99","88","77","66","55","44","33","22"
];


var SBNOB51100LIMPFOLD =
[
'A5S','A4S','A3S','A2S','K4O','K3O','K2O',"K6S","K5S","K4S","K3S","K2S",
"Q2S","Q3S","Q4S","Q5S","Q6S","Q7S","J7S","J6S","J5S","J4S","J3S","J2S",
"T2S","T3S","T4S","T5S","T6S","T7S","97S","96S","95S","94S","93S","92S",
"82S","83S","84S","85S","86S","76S","75S","74S","73S","72S","62S","63S",
"64S","65S","54S","53S","52S","42S","43S","32S","A2O","A3O","A4O","A5O",
"K5O","K6O","A6O","A7O","K7O","K8O","Q8O","Q7O","J7O","J8O","T8O","T7O",
"97O","98O","87O","76O","65O"
];

var SBNOB51100LIMP =
["A6S","K8S","K7S","Q9S","Q8S","J9S",
"J8S","T8S","98S","87S","QTO","JTO","K9O","Q9O","J9O","T9O","A8O"];

var SBNOB51100RAISEFOLD =
[ 'Q6O', 'J6O', 'T6O', '96O', '86O',
 'Q5O', '85O','75O', '64O','54O',
 'Q4O',
 'Q3O',
 'Q2O'
];
///////////////
////////////////
////////////////

var EPOB02OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', '99', '98S', '97S', '96S', 
'A8O', '88', '87S', '86S', '85S', 
'77',  '76S', '75S',
'66',  '65S', '64S', 
'55', '54S',
'44',
'33', 
'22'
];

var EPOB24OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'QQ', 
'AJO', 'JJ',
'TT',
'99', 
'88',
'77', 
'66',
'55',
'44' 
];

var EPOB46OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ', 
'TT',
'99',
'88', 
'77'
];

var EPOB68OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 
'AQO', 'QQ', 
'AJO', 'JJ', 
'TT',
'99',
'88'
];
var EPOB810OEP = [
'AA', 'AKS', 'AQS', 'AJS', 
'AKO', 'KK', 'KQS',
'AQO', 'QQ', 
'JJ',
'TT',
'99',
'88',
];
var EPOB1012OEP = [
'AA', 'AKS', 'AQS', 'AJS', 
'AKO', 'KK', 'KQS', 
'AQO', 'QQ',
'JJ', 
'TT',
'99', 
'88'
];
var EPOB1215OEP = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS', 
'AQO', 'QQ',
'JJ',
'TT',
'99'
];
var EPOB1520OEP = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'JJ',
'TT',
'99'
];


var EPOB2025OEPRAISE = [
'AA', 
'KK'
];
var EPOB2025OEP = [
'AKS', 'AQS', 'AJS',
'AKO',
'QQ',
'JJ'
];
var EPOB2025OEPCALL = [
'KQS',
'AQO',
'TT',
'99'
]

//25-35
var EPOB2535OEPRAISECALL50 = [
'AA'
];
var EPOB2535OEPRAISECALL75 = [
'KK' 
];
var EPOB2535OEPRAISE = [
'AKS',
'AKO',
'QQ',
'JJ'
];
var EPOB2535OEPCALL = [
'AQS', 
 'KQS',
 'AQO', 
 'QJS',
 'JTS',
 'TT',
 '99'
]

var EPOB2535OEP3BETFOLD50 = [
'A3S',
'A2S',
'KQO',  
'AJO'
]

var EPOB2535OEP3BETFOLD = [
'AJS', 'A5S', 'A4S',
 'T9S'
]

//36-50
var EPOB3650OEPRAISECALL50 = [
'AA', 'JJ'
];
var EPOB3650OEPRAISECALL75 = [
'AKS',
 'AKO', 'KK',
 'QQ', 
];

var EPOB3650OEPCALL = [
'AQS', 'AJS',
 'KQS', 'KJS',
 'AQO', 'QJS',
 'JTS' ,
 'TT',
 '99', 
 '88', 

];

var EPOB3650OEP3BETFOLD75 = [
'A3S',
'A2S'
];

var EPOB3650OEP3BETFOLD = [
'A5S', 'A4S' ,'T9S', '98S'
];

//51-80

var EPOB5180OEPRAISECALL50 = [
'AQS', 'JJ'
];

var EPOB5180OEPRAISE = [
'AA', 'KK', 'QQ', 'AKS', 'AKO'
];
var EPOB5180OEPRAISECALL25 = [
'AQO'
];

var EPOB5180OEPCALL = [
'AJS',
 'KQS', 'KJS',
 'QJS',
 'JTS', 
 'TT',
 '99',
 '88',
 '77',
];

var EPOB5180OEP3BETFOLD = [
'A5S', 'A4S', 'A3S', 'A2S',
 'T9S',
 '98S'
];

//////
var EPOB81OEPRAISECALL50 = [
 'JJ'
];

var EPOB81OEPRAISE = [
'AA', 'KK', 'QQ', 'AKS', 'AKO','AQS'
];
var EPOB81OEPRAISECALL25 = [
'AQO'
];

var EPOB81OEPCALL = [
'AJS',
 'KQS', 'KJS',
 'QJS',
 'JTS',
 'TT',
 '99',
 '88',
 '77','66','55','T9S'
];

var EPOB81OEP3BETFOLD = [
'A5S', 'A4S', 'A3S', 'A2S',
 '87S',
 '98S'
];

///////MPOB
///////
var MPOB02OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', '99', '98S', '97S', '96S',
'A8O', '88', '87S', '86S', '85S',
'77', '76S', '75S',
'66', '65S', '64S',
'55', '54S',
'44',
'33',
'22'
];
var MPOB24OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'QQ',
'AJO', 'JJ',
'ATO','TT',
'99',
'88',
'77',
'66',
'55',
'44'
];

var MPOB46OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88',
'77',
'66'
];

var MPOB68OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88'
];
var MPOB810OEP = [
'AA', 'AKS', 'AQS', 'AJS','ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO',
'JJ',
'TT',
'99',
'88',
];
var MPOB1012OEP = [
'AA', 'AKS', 'AQS', 'AJS','ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'JJ',
'TT',
'99',
'88',
'77'
];
var MPOB1215OEP = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'KJS','QJS',
'JJ',
'TT',
'99',
'88'
];
var MPOB1520OEP = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS','KJS',
'AQO', 'QQ',
'JJ',
'TT',
'99'
];


var MPOB2025OEPRAISE = [
'AA',
'KK'
];
var MPOB2025OEP = [
'AKS', 'AQS', 'AJS',
'AKO',
'QQ',
'JJ'
];
var MPOB2025OEPCALL = [
'KQS',
'AQO',
'TT',
'99'
]

//25-35
var MPOB2535OEPRAISE = [
'AA','KK','AKS',
'AKO',
'QQ',
'JJ'
];
var MPOB2535OEPCALL = [
'AQS',
'KQS',
'AQO',
'AJS',
'ATS',
'KJS',
'QJS',
'JTS',
'T9S',
'TT',
'99',
'88'
]

var MPOB2535OEP3BETFOLD50 = [
'A3S',
'A2S',
]

var MPOB2535OEP3BETFOLD = [
'AJO', 'A5S', 'A4S','98S', 'KQO'
]

//36-50
var MPOB3650OEPRAISECALL50 = [
'AQS'
];

var MPOB3650OEPRAISE = [
'AA','AKS',
'AKO', 'KK',
'QQ', 'JJ'
];

var MPOB3650OEPCALL = [
'AJS', 'ATS',
'KQS', 'KJS',
'AQO', 'QJS',
'JTS', 'T9S',
'TT',
'99',
'88',
'77'
];

var MPOB3650OEP3BETFOLD25 = [
'KTS', 'QTS'
];
var MPOB3650OEPCALLFOLD50 = [
'AJO', 'KQO'
];

var MPOB3650OEP3BETFOLD = [
'A5S', 'A4S', '87S', '98S'
];
var MPOB3650OEP3BETFOLD75 = [
'A3S', 'A2S', 'J9S', '98S'
];

//51-80
var MPOB5180OEPCALLFOLD50 = [
'AJO', 'KQO'
];
var MPOB5180OEP3BETFOLD50 = [
'KTS', 'QTS', 'T8S'
];
var MPOB5180OEPRAISECALL50 = [
'TT'
];

var MPOB5180OEPRAISE = [
'AA','AKS',
'AKO', 'KK',
'QQ', 'JJ','AQS'
];

var MPOB5180OEPCALL = [
'AJS', 'ATS',
'KQS', 'KJS',
'AQO', 'QJS',
'JTS', 'T9S',
'TT',
'99',
'88',
'77',
'66',
'55'
];

var MPOB5180OEP3BETFOLD = [
'A5S', 'A4S', 'A3S', 'A2S',
'J9S',
'98S', 
'87S'
];

//////81
var MPOB81OEP3BETFOLD50 = [
'KTS', 'QTS', 'T8S'
];
var MPOB81OEP3BETFOLDCALL50 = [
'AJO', 'KQO'
];
var MPOB81OEPRAISECALL50 = [
'AJS', 'KQS', 'TT','AQO'
];

var MPOB81OEPRAISE = [
'AA', 'KK', 'QQ', 'AKS', 'AKO', 'AQS','JJ'
];

var MPOB81OEPCALL = [
'AJS',
'KQS', 'KJS','ATS',
'QJS',
'JTS',
'T9S',
'TT',
'99',
'88',
'77',
'66',
'55',
'44',
'33',
'22'
];

var MPOB81OEP3BETFOLD = [
'A5S', 'A4S', 'A3S', 'A2S',
'87S', 'J9S',
'98S'
];


///////MPvsMPOB
///////
var MPOB02OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', '99', '98S', '97S', '96S',
'A8O', '88', '87S', '86S', '85S',
'77', '76S', '75S',
'66', '65S', '64S',
'55', '54S',
'44',
'33',
'22'
];
var MPOB24OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'QQ',
'AJO', 'JJ',
'ATO', 'TT',
'99',
'88',
'77',
'66',
'55',
'44'
];

var MPOB46OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88',
'77',
'66'
];

var MPOB68OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88'
];
var MPOB810OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO',
'JJ',
'TT',
'99',
'88',
];
var MPOB1012OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'JJ',
'TT',
'99',
'88',
'77'
];
var MPOB1215OMP = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'KJS', 'QJS',
'JJ',
'TT',
'99',
'88'
];
var MPOB1520OMP = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'QQ',
'JJ',
'TT',
'99'
];


var MPOB2025OMPRAISE = [
'AA',
'KK'
];
var MPOB2025OMP = [
'AKS', 'AQS', 'AJS',
'AKO',
'QQ',
'JJ'
];
var MPOB2025OMPCALL = [
'KQS',
'AQO',
'TT',
'99'
]

//25-35
var MPOB2535OMPRAISE = [
'AA', 'KK', 'AKS',
'AKO',
'QQ',
'JJ'
];
var MPOB2535OMPCALL = [
'AQS',
'KQS',
'AQO',
'AJS',
'ATS',
'KJS',
'QJS',
'JTS',
'T9S',
'TT',
'99',
'88'
]

var MPOB2535OMP3BETFOLD50 = [
'A3S',
'A2S',
]

var MPOB2535OMP3BETFOLD = [
'AJO', 'A5S', 'A4S', '98S', 'KQO'
]

//36-50
var MPOB3650OMPRAISECALL50 = [
'AQS'
];

var MPOB3650OMPRAISE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ', 'JJ'
];

var MPOB3650OMPCALL = [
'AJS', 'ATS',
'KQS', 'KJS',
'AQO', 'QJS',
'JTS', 'T9S',
'TT',
'99',
'88',
'77'
];

var MPOB3650OMP3BETFOLD25 = [
'KTS', 'QTS'
];
var MPOB3650OMP3BETFOLDCALL50 = [
'AJO', 'KQO'
];

var MPOB3650OMP3BETFOLD = [
'A5S', 'A4S', '87S', '98S'
];
var MPOB3650OMP3BETFOLD75 = [
'A3S', 'A2S', 'J9S', '98S'
];

//51-80
var MPOB5180OMP3BETFOLDCALL50 = [
'AJO', 'KQO'
];
var MPOB5180OMP3BETFOLD50 = [
'KTS', 'QTS', 'T8S'
];
var MPOB5180OMPRAISECALL50 = [
'TT'
];

var MPOB5180OMPRAISE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ', 'JJ', 'AQS'
];

var MPOB5180OMPCALL = [
'AJS', 'ATS',
'KQS', 'KJS',
'AQO', 'QJS',
'JTS', 'T9S',
'TT',
'99',
'88',
'77',
'66',
'55'
];

var MPOB5180OMP3BETFOLD = [
'A5S', 'A4S', 'A3S', 'A2S',
'J9S',
'98S',
'87S'
];

//////81
var MPOB81OMP3BETFOLD50 = [
'KTS', 'QTS', 'T8S'
];
var MPOB81OMP3BETFOLDCALL50 = [
'AJO', 'KQO'
];
var MPOB81OMPRAISECALL50 = [
'AJS', 'KQS', 'TT', 'AQO'
];

var MPOB81OMPRAISE = [
'AA', 'KK', 'QQ', 'AKS', 'AKO', 'AQS', 'JJ'
];

var MPOB81OMPCALL = [
'AJS',
'KQS', 'KJS', 'ATS',
'QJS',
'JTS',
'T9S',
'TT',
'99',
'88',
'77',
'66',
'55',
'44',
'33',
'22'
];

var MPOB81OMP3BETFOLD = [
'A5S', 'A4S', 'A3S', 'A2S',
'87S', 'J9S',
'98S'
];
////////////////////////
////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

var COOB02OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', '99', '98S', '97S', '96S',
'A8O', '88', '87S', '86S', '85S',
'77', '76S', '75S',
'66', '65S', '64S',
'55', '54S',
'44',
'33',
'22'
];
var COOB02OEPWHITE = [
    'A7O', 'A6O','A5O','A4O','A3O', 'A2O'
];
var COOB02OEPSILVER = [
    'Q5S'
];

var COOB24OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'KQO', 'QQ',
'AJO', 'JJ',
'ATO', 'TT',
'99',
'88',
'77',
'66',
'55',
'44',
'33',
'22'
];

var COOB46OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88',
'77',
'66',
'55'
];

var COOB68OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88'
];
var COOB810OEP = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO',
'JJ',
'TT',
'99',
'88',
];
var COOB1012OEP = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'QQ',
'JJ',
'TT',
'99',
'88',
'77'
];
var COOB1215OEP = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'JJ',
'TT',
'99',
'88',
'77'
];
var COOB1520OEP = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'QQ',
'JJ',
'TT',
'99',
'88'
];


var COOB2025OEPRAISE = [
'AA',
'KK'
];
var COOB2025OEP = [
'AKS', 'AQS', 'AJS',
'AKO',
'QQ',
'JJ'
];
var COOB2025OEPCALL = [
'KQS','KJS',
'AQO',
'TT',
'99'
]

//25-35
var COOB2535OEPRAISE = [
'AA', 'KK', 'AKS',
'AKO',
'QQ',
'JJ',
'TT',
];
var COOB2535OEPCALL = [
'AQS',
'KQS',
'AQO',
'AJS',
'ATS',
'KJS',
'QJS',
'JTS',
'T9S',
'99',
'88',
'77'
]

var COOB2535OEP3BETFOLD50 = [

]

var COOB2535OEP3BETFOLD = [
'AJO', 'A5S', 'A4S', '98S', 'KQO','A3S',
'A2S','87S','J9S'
]

//36-50
var COOB3650OEPRAISECALL50 = [
'AQS'
];

var COOB3650OEPRAISE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ', 'JJ'
];

var COOB3650OEPCALL = [
'AJS', 'ATS',
'KQS', 'KJS',
'AQO', 'QJS',
'JTS', 'T9S',
'TT',
'99',
'88',
'77'
];

var COOB3650OEP3BETFOLD25 = [

];
var COOB3650OEP3BETFOLDCALL25 = [
'AJO', 'KQO','A9S'
];

var COOB3650OEP3BETFOLD = [
'A5S', 'A4S', '87S', '98S', 'A3S', 'A2S', 'J9S', '98S', 'KTS', 'QTS',  'A3S', 'A2S',
];
var COOB3650OEP3BETFOLD75 = [
'A3S', 'A2S', 'J9S', '98S'
];



//51-80
var COOB5180OEP3BETFOLDCALL50 = [
'AJO', 'KQO'
];
var COOB5180OEP3BETFOLD50 = [
'KTS', 'QTS', 'T8S'
];
var COOB5180OEPRAISECALL50 = [
'TT'
];

var COOB5180OEPRAISE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ', 'JJ', 'AQS'
];

var COOB5180OEPCALL = [
'AJS', 'ATS',
'KQS', 'KJS',
'AQO', 'QJS',
'JTS', 'T9S',
'TT',
'99',
'88',
'77',
'66',
'55'
];

var COOB5180OEP3BETFOLD = [
'A5S', 'A4S', 'A3S', 'A2S',
'J9S',
'98S',
'87S'
];

//////81
var COOB81OEP3BETFOLD50 = [
'KTS', 'QTS', 'T8S'
];
var COOB81OEP3BETFOLDCALL50 = [
'AJO', 'KQO'
];
var COOB81OEPRAISECALL50 = [
'AJS', 'KQS', 'TT', 'AQO'
];

var COOB81OEPRAISE = [
'AA', 'KK', 'QQ', 'AKS', 'AKO', 'AQS', 'JJ'
];

var COOB81OEPCALL = [
'AJS',
'KQS', 'KJS', 'ATS',
'QJS',
'JTS',
'T9S',
'TT',
'99',
'88',
'77',
'66',
'55',
'44',
'33',
'22'
];

var COOB81OEP3BETFOLD = [
'A5S', 'A4S', 'A3S', 'A2S',
'87S', 'J9S',
'98S'
    ];
/////////////////////////////

/////////////////////////////

/////////////////////////////
var COOB02OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', '99', '98S', '97S', '96S',
'A8O', '88', '87S', '86S', '85S',
'77', '76S', '75S',
'66', '65S', '64S',
'55', '54S',
'44',
'33',
'22'
];
var COOB02OMPWHITE = [
    'A7O', 'A6O', 'A5O', 'A4O', 'A3O', 'A2O', "K9O","Q9O","J9O","T9O","K8O","Q8O","J8O","T8O","98O","87O","K7O"
];
var COOB02OMPSILVER = [
    'Q5S', "Q4S","Q3S","Q2S","J2S","J3S","J4S","J5S","J6S","T6S","T5S","95S","74S","53S","43S"
];

var COOB24OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'KQO', 'QQ',
'AJO', 'JJ',
'ATO', 'TT',
'99',
'88',
'77',
'66',
'55',
'44',
'33',
'22', "A8S","A7S","A6S","A5S","A4S","A3S","A2S","KTS","QTS","JTS","QJS","A9O"
];

var COOB46OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88',
'77',
'66',
'55', "A9S","A8S","A7S","KJS","KTS","QJS","KQO","44","33", "A9O", "ATO"
];

var COOB68OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88', "A9S","77","66","55","ATO"
];
var COOB810OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88', "A9S","77","66","ATO"
];
var COOB1012OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS','KJS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88',"77","66","ATO"
];
var COOB1215OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS','KJS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88',"77","66","ATO"
];
var COOB1520OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS','KJS','KTS','QJS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88',"77","66"
];


var COOB2025OMPRAISE = [
'AA',
'KK','QQ'
];
var COOB2025OMP = [
"AKS","AKO","AQO","JJ","TT","99","88"
];
var COOB2025OMPCALL = [
"AQS","AJS","ATS","KQS","KJS","KTS","QJS","QTS","JTS"
];

//25-35
var COOB2535OMPRAISE = [
'AA', 'KK', 'AKS','AQS',
'AKO',
'QQ',
'JJ',
'TT',
'99'
];
var COOB2535OMPCALL = [
'KQS',
'AQO',
'AJS',
'ATS',
'KJS',
'QJS',
'JTS',
'T9S',
'88',
'77', "KTS","QTS","98S",'T9S','98S'
];


var COOB2535OMP3BETFOLD50 = [
'ATO', 'J8S', 'T8S', '76S'

];


var COOB2535OMP3BETFOLD = [
"A9S","K9S","Q9S","A5S","A4S","A3S","A2S",'87S'
];

var COOB2535OMP3BETCALL50 = [
"KQO",'J9S'
];
var COOB2535OMP3BETCALL25 = [
"AJO"
];



//36-50
var COOB3650OMPRAISECALL50 = [
'TT'
];

var COOB3650OMPRAISE = [
'AA', 'AKS','AQS',
'AKO', 'KK',
'QQ', 'JJ'
];

var COOB3650OMPRAISECALL25 = [
'AQO', 'AJS','KQS'
];

var COOB3650OMPCALL = [
'ATS', 'A9S', 'AJO',
'KJS', 'KQO',
'QJS',
'JTS', 'T9S','98S', 'KTS','QTS','J9S',
'99',
'88',
'77', '66'
];


var COOB3650OMP3BETFOLD = [
'A5S', 'A4S', '87S', 'A3S', 'A2S','K9S', 'Q9S', 'A3S', 'A2S','76S'

];
var COOB3650OMP3BETFOLD50 = [
'65S', 'T8S', 'J8S'
];



//51-80
var COOB5180OMP3BETFOLDCALL50 = [
];
var COOB5180OMP3BETFOLD50 = [
'65S', 'T8S'
];
var COOB5180OMPRAISECALL50 = [
'TT','AJS','AQO','KQS'
];

var COOB5180OMPRAISE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ', 'JJ', 'AQS'
];

var COOB5180OMPCALL = [
'ATS', 'A9S', 'AJO', 'AJO', 'KQO',
'KJS', 'KQO',
'QJS',
'JTS', 'T9S','98S', 'KTS','QTS','J9S',
'99',
'88', '98S',
'77', '66',
'55'
];

var COOB5180OMP3BETFOLD = [
'A5S', 'A4S', 'A3S', 'A2S', 'A8S','K9S', 'Q9S',
'87S','J8S','76S'
];

//////81

var COOB81OMP3BETFOLDCALL50 = [
'A7S', 'A6S'
];
var COOB81OMPRAISECALL50 = [
'TT'
];
var COOB81OMPRAISECALL75 = [
'AJS', 'KQS', 'AQO'
];

var COOB81OMPRAISE = [
'AA', 'KK', 'QQ', 'AKS', 'AKO', 'AQS', 'JJ'
];

var COOB81OMPCALL = [

'KJS', 'ATS', 'AJO', 'A9S', 'A8S', 'KTS','J9S','QTS', 'KQO', 'J9S','98S',
'QJS',
'JTS',
'T9S',
'TT',
'99',
'88',
'77',
'66',
'55',
'44',
'33',
'22'
];

var COOB81OMP3BETFOLD = [
'A5S', 'A4S', 'A3S', 'A2S',
'87S','K9S', 'Q9S','J8S','76S'
];
var COOB81OMP3BETFOLD50 = [
'65S', 'T8S'

];


////
/////BUTTON//////////
////////////////////
/////////////////////////////

/////////////////////////////

/////////////////////////////
var BUOB02OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', '99', '98S', '97S', '96S',
'A8O', '88', '87S', '86S', '85S',
'77', '76S', '75S',
'66', '65S', '64S',
'55', '54S',
'44',
'33',
'22','A7O', 'A6O', 'A5O', 'A4O', 'A3O', 'A2O','K9O','Q5S'
];


var BUOB24OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'KQO', 'QQ',
'AJO', 'JJ',
'ATO', 'TT',
'99',
'88',
'77',
'66',
'55',
'44',
'33',
'22'
];

var BUOB46OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88',
'77',
'66',
'55', "44", "ATO"
];

var BUOB68OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88', , "77"
];
var BUOB810OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88', "77"
];
var BUOB1012OEP = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS', 
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88', "77"
];
var BUOB1215OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'QQ',
'JJ',
'TT',
'99',
'88', "77"
];
var BUOB1520OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'QQ',
'JJ',
'TT',
'99',
'88'
];


var BUOB2025OEPRAISE = [
'AA',
'KK', 'QQ'
];
var BUOB2025OEP = [
"AKS", "AKO", "AQO", "JJ", "TT"
];
var BUOB2025OEPCALL = [
"AQS", "AJS", "KQS", "KJS", "QJS", "99"
];

//25-35
var BUOB2535OEPRAISE = [
'AA', 'KK', 'AKS',
'AKO',
'QQ',
'JJ',
'TT',
];
var BUOB2535OEPCALL = [
'KQS','99','AQS',
'AQO',
'AJS',
'ATS',
'KJS',
'QJS',
'JTS',
'T9S',
'88',
'77', "98S", 'T9S', '98S','AJO'
];


var BUOB2535OEP3BETFOLD = [
 "A5S", "A4S", "A3S", "A2S", '87S', "KTS", "QTS",'ATO','T8S','J9S'
];


//36-50
var BUOB3650OEPRAISECALL50 = [
'AQS'
];

var BUOB3650OEPRAISE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ', 'JJ'
];


var BUOB3650OEPCALL = [
'ATS', 'A9S', 'AJO',
'KJS', 'KQO',
'QJS',
'JTS', 'T9S', '98S', 'KTS', 'QTS', 'AQO', 'AJS', 'KQS',
'99',
'88',
'77', '66', 'TT'
];


var BUOB3650OEP3BETFOLD = [
'A5S', 'A4S', '87S', 'A3S', 'A2S', 'KTS', 'QTS', 'A3S', 'A2S','J9S','T8S'

];
var BUOB3650OEP3BETFOLD50 = [
'65S', 'T8S', 'J8S'
];



//51-80

var BUOB5180OEP3BETFOLDCALL50 = [
'KTS','QTS'
];
var BUOB5180OEPRAISECALL50 = [
'AQS', 'AQO'
];
var BUOB5180OEPRAISECALL25 = [
'KQS', 'AJS'
];

var BUOB5180OEPRAISE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ', 'JJ', 'AQS'
];

var BUOB5180OEPCALL = [
'ATS', 'A9S', 'AJO', 'AJO', 'KQO','A8S',
'KJS', 'KQO',
'QJS',
'JTS', 'T9S', '98S', 'KTS', 'QTS', 'J9S',
'99', 'TT',
'88', '98S',
'77', '66',
'55'
];

var BUOB5180OEP3BETFOLD = [
'A5S', 'A4S', 'A3S', 'A2S', 
'87S', 'J9S','T8S'
];
var BUOB5180OEP3BETFOLD50 = [
'K9S', 'Q9S'
];
//////81

var BUOB81OEP3BETFOLD50 = [
'K9S', 'Q9S'
];
var BUOB81OEP3BETFOLDCALL50 = [
'KTS', 'QTS'
];
var BUOB81OEPRAISECALL50 = [
'AJS'
];
var BUOB81OEPRAISECALL25 = [
'AJO', 'KQS'
];

var BUOB81OEPRAISE = [
'AA', 'KK', 'QQ', 'AKS', 'AKO', 'AQS', 'JJ', 'AQO'
];

var BUOB81OEPCALL = [
'AJS',
'KJS', 'ATS', 'AJO', 'A9S', 'A8S', 'KTS', 'J9S', 'QTS', 'KQO', 'J9S', '98S',
'QJS',
'JTS',
'T9S',
'TT',
'99',
'88',
'77',
'66',
'55',
'44',
'33',
'22'
];

var BUOB81OEP3BETFOLD = [
'A5S', 'A4S', 'A3S', 'A2S',
'87S', 'T8S', 'J9S'
];

/////////////
/////////////
/////////// BUTTON OPEN BEFORE FROM MP
var BUOB02OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S','J6S','J5S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S','T6S',
'A9O', '99', '98S', '97S', '96S',
'A8O', '88', '87S', '86S', '85S',
'77', '76S', '75S',
'66', '65S', '64S',
'55', '54S',
'44',
'33',
'22','A7O', 'A6O', 'A5O', 'A4O', 'A3O', 'A2O','K9O','Q5S','Q4S','Q3S','K7O','K8O', 'Q9O', 'Q8O', 'J9O','J8O','T9O','T8O','87O','98O'
];


var BUOB24OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'KQO', 'QQ',
'AJO', 'JJ',
'ATO', 'TT',
'99',
'88',
'77',
'66',
'55',
'44',
'33',
'22', "A8S","A7S","A6S","A5S","A4S","A3S","A2S","QJS","KTS","QTS","JTS","A9O","A8O",'KJO'
];

var BUOB46OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88',
'77',
'66',
'55', "44", "ATO", "A9S","A8S","A7S","KJS","QJS","KQO","A9O","33"
];

var BUOB68OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88', "77", "A9S","A8S","ATO"
];
var BUOB810OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88', "77", "A9S","A8S","ATO","66","55"
];
var BUOB1012OMP = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS', 
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88', "77", "ATS","A9S","KJS","ATO","66"
];
var BUOB1215OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'QQ',
'JJ',
'TT',
'99',
'88', "77", "AJO","ATO","66"
];
var BUOB1520OMP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'QQ','AJO', '77','66',
'JJ',
'TT',
'99',
'88'
];


var BUOB2025OMPRAISE = [
'AA',
'KK', 'QQ', "AKO","AKS","AQS",
];
var BUOB2025OMP = [
"AKS",  "JJ", "TT", "AQO", "99","88","77","66",'AJO'
];
var BUOB2025OMPCALL = [
"AJS", "ATS", "KQS", "KJS", "QJS",
];

//25-35
var BUOB2535OMPRAISE = [
'AA', 'KK', 'AKS',
'AKO','99',
'QQ',
'JJ',
'TT','AQS',
'AQO',
];
var BUOB2535OMPCALL = [
'KQS',
'AJS',
'ATS',
'KJS',
'QJS',
'JTS',
'T9S',
'88',
'77', "98S", 'T9S', '98S','AJO','A9S','A8S', "KTS", "QTS", 'KQO','87S','66','76'
];


var BUOB2535OMP3BETFOLD = [
    "A5S", "A4S", "A3S", "A2S", 'T8S','KJO', 'QJO','97S', 'K9S','Q9S'
];
var BUOB2535OMP3BETFOLDCALL50 = [
'ATO', 'J9S'
];


//36-50
var BUOB3650OMPRAISECALL50 = [
'AJS','KQS'
];

var BUOB3650OMPCALLFOLD50 = [
'ATO','KJO'
];


var BUOB3650OMPRAISE = [
'AA', 'AKS',
'AKO', 'KK','AQS','AQO',
'QQ', 'JJ','TT'
];


var BUOB3650OMPCALL = [
'ATS', 'A9S', 'AJO',
'KJS', 'KQO',
'QJS',
'JTS', 'T9S', '98S', 'KTS', 'QTS', 'AJS','A8S',
'99',
'88',
'77', '66', 'KTS','55','87S','J9S'
];


var BUOB3650OMP3BETFOLD = [
'A5S', 'A4S', '97S', 'A3S', 'A2S', 'A3S', 'A2S','T8S','K9S','Q9S','76S','65S'
];

var BUOB3650OMPCALLFOLD50 = [
'ATO','KJO'
];


//51-80


var BUOB5180OMPRAISECALL75 = [
'KQS', 'AJS'
];
var BUOB5180OMPRAISECALL25 = [
'AJO'
];

var BUOB5180OMPRAISE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ', 'JJ', 'AQS', 'AQS', 'AQO','TT'
];

var BUOB5180OMPCALL = [
'ATS', 'A9S', 'KQO','A8S',
'KJS', 'KQO',
'QJS',
'JTS', 'T9S', '98S', 'KTS', 'QTS', 'J9S',
'99',
'88', '98S',
'77', '66',
'55', 'KTS','QTS', '44', '87S'
];

var BUOB5180OMP3BETFOLD = [
'A5S', 'A4S', 'A3S', 'A2S', 
'97S', 'T8S', 'K9S', 'Q9S','76S','65S'
];
var BUOB5180OMPCALLFOLD50 = [
'ATO', 'KJO'
];

//////81

var BUOB81OMP3BETFOLD25 = [
'K8S', 'J8S'
];

var BUOB81OMPRAISECALL50 = [
'AJO',
];

var BUOB81OMPRAISE = [
'AA', 'KK', 'QQ', 'AKS', 'AKO', 'AQS', 'JJ', 'AQO','AJS','TT', 'KQS'
];
var BUOB81OMPCALL = [
'KJS', 'ATS', 'A9S', 'A8S', 'KTS', 'J9S', 'QTS', 'KQO', 'J9S', '98S','87S',
'QJS',
'JTS',
'T9S',
'99',
'88',
'77',
'66',
'55',
'44',
'33',
'22'
];
var BUOB81OMPCALLFOLD50 = [
'ATO', 'KJO'
];

var BUOB81OMP3BETFOLD = [
'A3S', 'A2S','K9S','Q9S', 'T8S','97S','76S','65S'
];
var BUOB81OMP3BETFOLDCALL25 = [

];
var BUOB81OMP3BETFOLDCALL50 = [
'A7S','A5S'
];
var BUOB81OMP3BETFOLDCALL75 = [
'A4S','A6S'
];

/////////////
/////////////
/////////// BUTTON OPEN BEFORE FROM CO
var BUOB02OCO = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S','J6S','J5S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S','T6S',
'A9O', '99', '98S', '97S', '96S',
'A8O', '88', '87S', '86S', '85S',
'77', '76S', '75S',
'66', '65S', '64S',
'55', '54S',
'44',
'33',"Q2S","J4S","J3S","T5S","95S","74S","Q7O","J7O","T7O","97O","76O","K6O","K5O","K4O",
'22','A7O', 'A6O', 'A5O', 'A4O', 'A3O', 'A2O','K9O','Q5S','Q4S','Q3S','K7O','K8O', 'Q9O', 'Q8O', 'J9O','J8O','T9O','T8O','87O','98O'
];


var BUOB24OCO = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'KQO', 'QQ',
'AJO', 'JJ',
'ATO', 'TT',
'99',
'88',
'77',
'66',
'55',
'44',
'33',,"K9S","K8S","K7S","K6S","K5S","Q9S","Q8S","J9S","T9S","98S","QJO","QTO","KTO","K9O","A7O","A6O","A5O","A4O","A3O",
'22', "A8S","A7S","A6S","A5S","A4S","A3S","A2S","QJS","KTS","QTS","JTS","A9O","A8O",'KJO'
];

var BUOB46OCO = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88',
'77','22',
'66',"A6S","A5S","A4S","A3S","A2S","KTS","K9S","K8S","K7S","QTS","JTS","KJO","QJO","KTO","A8O","A7O","A6O","A5O",
'55', "44", "ATO", "A9S","A8S","A7S","KJS","QJS","KQO","A9O","33"
];

var BUOB68OCO = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',"A7S","A6S","A5S","A4S","A3S","A2S","KJS","QJS","KTS","QTS","KQO","KJO","A9O","A8O","66","55","44","33",
'88', "77", "A9S","A8S","ATO"
];
var BUOB810OCO = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',"A7S","A6S","A5S","KJS","KTS","QJS","KQO","KJO","A9O","44",
'88', "77", "A9S","A8S","ATO","66","55"
];
var BUOB1012OCO = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS', 
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88', "77", "ATS","A9S","KJS","ATO","66","A8S","A7S","A6S","A5S","KTS","QJS","KQO","A9O","55","44","33"
];
var BUOB1215OCO = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'QQ',
'JJ',
'TT',
'99',"A9S","A8S","A7S","A6S","A5S","KTS","QTS","QJS","KQO","A9O","55","44","33",
'88', "77", "AJO","ATO","66"
];
var BUOB1520OCO = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'QQ','AJO', '77','66',
'JJ',
'TT',
'99',
'88',"A9S","A8S","A7S","A5S","KTS","QJS","QTS","JTS","ATO","KQO"
];

/////20-25
var BUOB2025OCORAISE = [
'AA',
'KK', 'QQ', "AKO","AKS","AQS", "JJ", "TT","AJS","AQO", 
];
var BUOB2025OCO = [
"99","88","77","66",'55','44'
];
var BUOB2025OCOCALL = [
"A9S", "ATS", "KQS", "KJS", "QJS",'A8S','KTS','QTS','JTS','AJO','ATO','KQO'
];

//25-35
var BUOB2535OCORAISE = [
'AA', 'KK', 'AKS',
'AKO','99',
'QQ',
'JJ',
'TT','AQS','AJO',
'AQO','77','88','AJS',
'ATS','KQS'
];
var BUOB2535OCOCALL = [
'KJS',
'QJS',
'JTS',
'T9S','A7S','J9S','ATO',
'KJO','K9S','Q9S','KJS','QJS','JTS','T8S',
"98S", 'T9S', '98S','A9S','A8S', "KTS", "QTS", 'KQO','87S','66','76S'
];
var BUOB2535OCOCALLFOLD50 = [
'QJO'
];
var BUOB2535OCOSHOVE75 = [
 '66'
];
var BUOB2535OCOSHOVE50= [
'55','44','33','22'
];

var BUOB2535OCO3BETFOLD = [
"A6S", "A4S", "A3S", "A2S",'97S','K8S','K7S','K6S','Q8S','65S','86S','T7S'
];
var BUOB2535OCO3BETFOLDCALL50 = [
'A5S', 'J8S'
];


//36-50

var BUOB3650OCORAISE = [
'AA', 'AKS','AJS','KQS',
'AKO', 'KK','AQS','AQO',
'QQ', 'JJ','TT'
];


var BUOB3650OCOCALL = [
'ATS', 'A9S', 'AJO','ATO','76S',
'KJS', 'KQO',
'QJS',
'JTS', 'T9S', '98S', 'KTS', 'QTS', 'AJS','A8S',
'99',
'88',
'77', '66','55', 'KTS','55','87S','J9S','J8S','T8S','A7S','KJO','QJO','K9S','Q9S'
];


var BUOB3650OCO3BETFOLD = [
'A5S', 'A4S', '97S', 'A3S', 'A2S', 'A3S', 'A2S','T7S','K8S', 'K7S','Q8S','65S','86S','J7S'
];

var BUOB3650OCO3BETFOLDCALL50 = [
'A5S'
];
var BUOB3650OCO3BETFOLDCALL75 = [
'A6S','A4S'
];


//51-80

var BUOB5180OCORAISECALL50 = [
'AJO','KQO', 'KJS','ATS','99'
];

var BUOB5180OCORAISE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ', 'JJ', 'AQS', 'AQS', 'AQO','TT','KQS', 'AJS'

];

var BUOB5180OCOCALL = [
'A9S', 'KQO','A8S',
'QJS',
'JTS', 'T9S', '98S', 'KTS', 'QTS', 'J9S',
'88', '98S',
'77', '66',
'55', 'KTS','QTS', '44', '87S',"ATO","KJO","QJO","K9S","Q9S","A7S","J8S","T8S","76S"
];

var BUOB5180OCO3BETFOLD = [
"K8S","K7S","K6S","Q8S","J7S","T7S","97S","86S","75S","65S"
];

var BUOB5180OCO3BETFOLDCALL50 = [
'A4S'
];
var BUOB5180OCO3BETFOLDCALL25 = [
'A5S'
];
var BUOB5180OCO3BETFOLDCALL75 = [
'A3S', 'A2S','A6S',
];
var BUOB5180OCOCALLFOLD50 = [
'33', '22'
];

//////81

var BUOB81OCORAISECALL50 = [
'AJO',
];

var BUOB81OCORAISE = [
'AA', 'KK', 'QQ', 'AKS', 'AKO', 'AQS', 'JJ', 'AQO','AJS','TT', 'KQS','AJO','ATS'
];
var BUOB81OCORAISECALL50 = [
    'QJS'
];
var BUOB81OCORAISECALL75 = [
'KQO','99','KJS' 
];
var BUOB81OCOCALL = [
'A9S', 'A8S', 'KTS', 'J9S', 'QTS', 'KQO', 'J9S', '98S','87S',
'QJS',
'JTS',
'T9S',
'88',
'77',
'66',
'55',
'44',
'33','K9S','K8S','KJO','QJO','ATO','J8S','T8S','Q9S','76S',
'22', 'A5S','A7S'
];
var BUOB81OCOCALLFOLD50 = [
'A9O', 'KTO','JTO','QTO'
];

var BUOB81OCO3BETFOLD = [
'K8S', 'A2S','T7S','J7S', '86S','97S','75S','65S','K7S','K6S', 'Q8S'
];

var BUOB81OCO3BETFOLDCALL50 = [
'A2S','A3S', 'A4S','A6S'
];




///////////////SB vs EP
////////////////////
///////////
///////////////////////////

var SBOB02OEP = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
 'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S', '83S', '82S',
 'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S', '72S',
 'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S', '62S',
 'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '95O', '85O', '75O', '65O', '55', '54S', '53S', '52S',
 'A4O', 'K4O', 'Q4O', 'J4O', 'T4O', '94O', '84O', '74O', '64O', '54O', '44', '43S', '42S',
 'A3O', 'K3O', 'Q3O', 'J3O', 'T3O', '93O', '83O', '73O', '63O', '53O', '43O', '33', '32S',
 'A2O', 'K2O', 'Q2O', 'J2O', 'T2O', '92O', '82O', '72O', '62O', '52O', '42O', '32O', '22'
];


var SBOB24OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'KQO', 'QQ',
'AJO', 'JJ',
'ATO', 'TT',
'99',
'88',
'77',
'66',
'55',
'44',
'33',
'22', "A8S","A7S","A6S","A5S","A4S","A3S","A2S","QJS","KTS","QTS","JTS","A9O","K9S",'KJO'
];

var SBOB46OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88',
'77',
'66',
'55', "44", "ATO", "A9S","A8S","KJS","QJS","KQO","33","22",
"A8S","A7S","A6S","A5S","A4S","A3S","A2S","KTS"
];

var SBOB68OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88', "77","ATO",'44',"66","55"
];
var SBOB810OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88', "77","66","55"
];
var SBOB1012OEP = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS', 
'AQO', 'QQ',
'AJO', 'JJ',
'TT',
'99',
'88', "77", "ATS","KJS","66"
];
var SBOB1215OEP = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS', 'KJS',
'AQO', 'QQ',
'JJ',
'TT',
'99',
'88', "77", "66",'55'
];
var SBOB1520OEP = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS', 'KJS','QJS',
'AQO', 'QQ',
'JJ',
'TT',
'99',
'88'
];

var SBOB2025OEP = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK', 'KQS', 'KJS','QJS',
'AQO', 'QQ',
'JJ',
'TT',
'99'
];

//25-35
var SBOB2535OEPRAISE = [
'AA', 'KK', 'AKS',
'AKO',
'QQ',
'JJ',
'TT','AQS',
'AQO',
];
var SBOB2535OEPRAISECALL25 = [
'AQS'
];
var SBOB2535OEPCALL = [
'KQS','99',
'AJS',
'ATS',
'KJS',
'QJS',
'JTS'
];


var SBOB2535OEP3BETFOLD75 = [
    "A5S", "A4S", "A3S", "A2S", 'T9S'
];
var SBOB2535OEP3BETFOLD50 = [
    "AJO", "KQO"
];
var SBOB2535OEP3BETFOLDCALL50 = [
'KJS'
];


//36-50

var SBOB3650OEPRAISE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ', 'JJ'
];


var SBOB3650OEPCALL = [
'ATS', 'A9S', 'AJO','AQS','AQO',
'KJS',
'QJS',
'JTS', 'T9S', '98S',  'KQS', 'AJS',
'99',
'88',
'TT'
];


var SBOB3650OEP3BETFOLD = [
'A5S', 'A4S', '87S', 'A3S', 'A2S', 'A3S', 'A2S','98S'
];

var SBOB3650OEP3BETFOLD50 = [
'A9S',
];


//51-80


var SBOB5180OEPRAISE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ', 'JJ'
];


var SBOB5180OEPCALL = [
'ATS', 'A9S', 'AQS','AQO',
'KJS',
'QJS',
'JTS', 'T9S', '98S',  'KQS', 'AJS',
'99',
'88',
'TT','66','77'
];


var SBOB5180OEP3BETFOLD = [
'A5S', 'A4S', '87S', 'A3S', 'A2S', 'A3S', 'A2S','98S'
];

var SBOB5180OEP3BETFOLD50 = [
'A9S',
];

//////81

var SBOB81OEPRAISE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ', 'JJ'
];


var SBOB81OEPCALL = [
'ATS', 'A9S','AQS','AQO',
'KJS',
'QJS',
'JTS', 'T9S', '98S',  'KQS', 'AJS',
'99',
'88',
'TT','77','66','55'
];


var SBOB81OEP3BETFOLD = [
'A5S', 'A4S', '87S', 'A3S', 'A2S', 'A3S', 'A2S','98S'
];

var SBOB81OEP3BETFOLD50 = [
'A9S',
];
var SBOB81OEPCALLFOLD50 = [
'44','33','22'
];


///////////////SB vs MP
////////////////////
///////////
///////////////////////////

var SBOB02OMP = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
 'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S', '83S', '82S',
 'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S', '72S',
 'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S', '62S',
 'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '95O', '85O', '75O', '65O', '55', '54S', '53S', '52S',
 'A4O', 'K4O', 'Q4O', 'J4O', 'T4O', '94O', '84O', '74O', '64O', '54O', '44', '43S', '42S',
 'A3O', 'K3O', 'Q3O', 'J3O', 'T3O', '93O', '83O', '73O', '63O', '53O', '43O', '33', '32S',
 'A2O', 'K2O', 'Q2O', 'J2O', 'T2O', '92O', '82O', '72O', '62O', '52O', '42O', '32O', '22'
];


var SBOB24OMP = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
 'A9O', '99', '98S', '97S',
 'A8O', '88', '87S', '86S',
 'A7O', '77', '76S',
 'A6O', '66',
 'A5O', '55',
 'A4O', '44',
 'A3O', '33',
 'A2O', '22'
];

var SBOB46OMP = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
 'AJO', 'KJO', 'JJ', 'JTS', 'J9S',
 'ATO', 'TT', 'T9S',
 'A9O', '99',
 'A8O', '88',
 'A7O', '77',
 '66',
 '55',
 '44',
 '33',
 '22'
];

var SBOB68OMP = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S',
 'AKO', 'KK', 'KQS', 'KJS',
 'AQO', 'KQO', 'QQ', 'QJS',
 'AJO', 'KJO', 'JJ',
 'ATO', 'TT',
 'A9O', '99',
 'A8O', '88',
 '77',
 '66',
 '55',
 '44',
 '33',
 '22'
];

var SBOB810OMP = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S',
 'AKO', 'KK', 'KQS',
 'AQO', 'KQO', 'QQ',
 'AJO', 'KJO', 'JJ',
 'ATO', 'TT',
 '99',
 '88',
 '77',
 '66',
 '55',
 '44',
];

var SBOB1012OMP = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S',
 'AKO', 'KK', 'KQS','KJS',
 'AQO', 'KQO', 'QQ',
 'AJO', 'JJ',
 'ATO', 'TT',
 '99',
 '88',
 '77',
 '66',
 '55'
];
var SBOB1215OMP = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S',
 'AKO', 'KK', 'KQS','KJS',
 'AQO', 'KQO', 'QQ', 'QJS',
 'AJO', 'JJ',
 'ATO', 'TT',
 '99',
 '88',
 '77',
 '66',
 '55',
 '44'
];
var SBOB1520OMP = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S',
 'AKO', 'KK', 'KQS','KJS',
 'AQO', 'KQO', 'QQ', 'QJS',
 'AJO', 'JJ',
 'ATO', 'TT',
 '99',
 '88',
 '77',
 '66',
 '55'
];

var SBOB2025OMP = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S',
 'AKO', 'KK', 'KQS','KJS',
 'AQO', 'KQO', 'QQ',
 'AJO', 'JJ',
 'ATO', 'TT',
 '99',
 '88',
 '77',
 '66',
 '55'
];

//25-35
var SBOB2535OMPRAISE = [
 'AA', 'AKS', 'AQS',
 'AKO', 'KK',
 'AQO', 'QQ',
 'JJ',
 'TT',
 '99',
 '88'
];
var SBOB2535OMPRAISECALL50 = [
'AJS'
];
var SBOB2535OMPCALL = [
'KQS',
'KQO',
'ATS',
'KJS',
'QJS',
'JTS',
'T9S',
'AJO'
];


var SBOB2535OMP3BETFOLD = [
  'A9S', "A5S", "A4S", "A3S", "A2S","A2S",'98S'
];
var SBOB2535OMP3BETFOLD50 = [
    "ATO", 'KJO', 'K9S','Q9S','J9S'
];
var SBOB2535OMP3BETFOLDCALL25 = [
'KTS', 'QTS'
];


//36-50

var SBOB3650OMPRAISE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ', 'JJ','AQS'
];
var SBOB3650OMPRAISECALL50 = [
'TT'
];


var SBOB3650OMPCALL = [
'ATS','AQO','KQO',
'KJS','AJS','QTS',
'QJS', 'KTS',
'JTS', 'T9S', 'KQS', 'AJS',
'99',
'88',
'77'
];


var SBOB3650OMP3BETFOLD = [
'A5S', 'A4S', '87S', 'A3S', 'A2S', 'A3S', 'A2S','98S','A9S'
];

var SBOB3650OMP3BETFOLD50 = [
'A8S','K9S','Q9S','J9S','T8S'
];

var SBOB3650OMPCALLFOLD50 = [
'AJO'
];

//51-80


var SBOB5180OMPRAISE = [
'AA', 'AKS','AQS',
'AKO', 'KK',
'QQ', 'JJ'
];


var SBOB5180OMPCALL = [
'ATS','AQO','KQO',
'KJS','AJS','QTS',
'QJS', 'KTS',
'JTS', 'T9S', 'KQS', 'AJS',
'99','TT',
'88',
'77'
];


var SBOB5180OMP3BETFOLD = [
'A5S', 'A4S', '87S', 'A3S', 'A2S', 'A3S', 'A2S','98S','A9S'
];

var SBOB5180OMP3BETFOLD75 = [
'A8S','K9S','Q9S','J9S','T8S'
];
var SBOB5180OMPCALLFOLD75 = [
'AJO',
];

//////81

var SBOB81OMPRAISE = [
'AA', 'AKS','AQS',
'AKO', 'KK',
'QQ', 'JJ'
];


var SBOB81OMPCALL = [
'ATS', 'A9S','AQO', 'KQO',
'KJS', 'AJO',
'QJS',
'JTS', 'T9S', '98S',  'KQS', 'AJS','KTS','QTS',
'99',
'88',
'TT','77','66','55', '44','33','22'
];


var SBOB81OMP3BETFOLD = [
'A5S', 'A4S', '87S', 'A3S', 'A2S', 'A3S', 'A2S','98S'
];

var SBOB81OMP3BETFOLD75 = [
'A8S', 'K9S','Q9S','J9S'
];


///////////////SB vs CO
////////////////////
///////////
///////////////////////////

var SBOB02OCO = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
 'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S', '83S', '82S',
 'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S', '72S',
 'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S', '62S',
 'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '95O', '85O', '75O', '65O', '55', '54S', '53S', '52S',
 'A4O', 'K4O', 'Q4O', 'J4O', 'T4O', '94O', '84O', '74O', '64O', '54O', '44', '43S', '42S',
 'A3O', 'K3O', 'Q3O', 'J3O', 'T3O', '93O', '83O', '73O', '63O', '53O', '43O', '33', '32S',
 'A2O', 'K2O', 'Q2O', 'J2O', 'T2O', '92O', '82O', '72O', '62O', '52O', '42O', '32O', '22'
];


var SBOB24OCO = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S',
 'A8O', 'K8O', 'Q8O', '98O', '88', '87S', '86S', '85S', 
 'A7O', 'K7O', '77', '76S', '75S',
 'A6O', 'K6O', '66', '65S', '64S',
 'A5O', 'K5O', '55', '54S',
 'A4O', 'K4O', '44',
 'A3O', '33',
 'A2O', '22'
];

var SBOB46OCO = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S',
 'A9O', 'K9O', '99', '98S', '97S',
 'A8O', 'K8O', '88', '87S',
 'A7O', '77', '76S',
 'A6O', '66',
 'A5O', '55',
 'A4O', '44',
 'A3O', '33',
 'A2O', '22'
]
var SBOB68OCO = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS',
 'ATO', 'KTO', 'TT',
 'A9O', '99',
 'A8O', '88',
 'A7O', '77', 
 'A6O', '66',
 'A5O', '55',
 'A4O', '44',
 'A3O', '33',
 '22'
]

var SBOB810OCO = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS',
 'AJO', 'KJO', 'JJ',
 'ATO', 'KTO', 'TT',
 'A9O', '99',
 'A8O', '88',
 'A7O', '77', 
 '66',
 '55',
 '44',
 '33',
]

var SBOB1012OCO = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS',
 'AJO', 'KJO', 'JJ',
 'ATO', 'TT',
 'A9O', '99',
 'A8O', '88',
 '77', 
 '66',
 '55',
 '44',
 '33',
]
var SBOB1215OCO = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS',
 'AJO', 'JJ', 'JTS',
 'ATO', 'TT',
 'A9O', '99',
 'A8O', '88',
 '77', 
 '66',
 '55',
 '44',
 '33',
 '22'
]
var SBOB1520OCO = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS',
 'AJO', 'JJ', 'JTS',
 'ATO', 'TT', 'T9S',
 'A9O', '99',
 '88',
 '77', 
 '66',
 '55',
 '44',
 '33',
 '22'
]
var SBOB2025OCO = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS',
 'AJO', 'JJ', 'JTS',
 'ATO', 'TT', 'T9S',
 'A9O', '99',
 '88',
 '77', 
 '66',
 '55',
 '44',
 '33',
]

//25-35
var SBOB2535OCORAISE = [
'AA', 'AKS',,'AQO',
'AKO', 'KK',
'QQ', 'JJ','AQS','TT','99','88','77', 'AJS'
];
var SBOB2535OCORAISECALL50 = [
'ATS','AJO','KQS'
];
var SBOB2535OCOCALL = [
'ATS', 'KQO',
'KJS','QTS',
'QJS', 'KTS',
'JTS', 'T9S', 'KQS','A9S'
];


var SBOB2535OCO3BETFOLD = [
  'A8S', "A5S", "A4S", "A3S", "A2S","A2S",'98S', '87S', 'K9S','Q9S','J9S'
];
var SBOB2535OCO3BETFOLD50 = [
 'KJO', 'A7S','A6S','K8S','KJO','QJO'
];
var SBOB2535OCOCALLFOLD50 = [
'ATO'
];
var SBOB2535OCOSHOVE50 = [
'55','44','33','22'
];
var SBOB2535OCOSHOVERAISE2550 = [
'66'
];

//36-50

var SBOB3650OCORAISE = [
'AA', 'AKS','KQS','AQO',
'AKO', 'KK',
'QQ', 'JJ','AQS','TT','99', 'AJS'
];


var SBOB3650OCOCALL = [
'ATS', 'KQO','AJO',
'KJS','QTS',
'QJS', 'KTS',
'JTS', 'T9S', 'A9S','88','77','98S','A8S'
];


var SBOB3650OCO3BETFOLD = [
'A5S', 'A4S', '87S', 'A3S', 'A2S', 'A3S', 'A2S','76S','T8S','K9S','K8S'
];

var SBOB3650OCO3BETFOLD50 = [
'A7S','A6S','97S'
];
var SBOB3650OCO3BETFOLDCALL50 = [
'Q9S','J9S'
];
var SBOB3650OCOCALLFOLD50 = [
'ATO','KJO','QJO'
];

//51-80

var SBOB5180OCORAISE = [
'AA', 'AKS','KQS','AQO',
'AKO', 'KK',
'QQ', 'JJ','AQS','TT', 'AJS'
];

var SBOB5180OCORAISECALL50 = [
'ATS', '99'
];
var SBOB5180OCORAISECALL25 = [
'KJS','KQO','AJO'
];

var SBOB5180OCOCALL = [
'QTS',
'QJS', 'KTS',
'JTS', 'T9S',
'88',
'77','66','55','A9S','A8S','98S'
];


var SBOB5180OCO3BETFOLD = [
'A5S', 'A4S', '87S', 'A3S', 'A2S', 'A3S','76S','T8S','K9S','K8S'
];
var SBOB5180OCO3BETFOLD50 = [
'65S', 'Q8S','J8S','97S','86S'
];

var SBOB5180OCO3BETFOLDCALL25 = [
'Q9S','J9S'
];

var SBOB5180OCO3BETFOLDCALL75 = [
'A7S','A6S'
];
var SBOB5180OCOCALLFOLD50 = [
'ATO', 'KJO','QJO'
];

//////81

var SBOB81OCORAISE = [
'AA', 'AKS','AQS', 'AJS','AQO',
'AKO', 'KK',
'QQ', 'JJ', 'TT','KQS'
];
var SBOB81OCORAISECALL75 = [
'ATS'
];
var SBOB81OCORAISECALL50 = [
'99','KJS', 'KQO', 'AJO'
];

var SBOB81OCOCALL = [
'A8S','JTS','T9S' ,'QJS','Q9S','J9S','88', '77', '66', '55','A9S','A8S','A7S','A6S','ATO','98S','KTS','QTS'
];
var SBOB81OCOCALLFOLD50 = [
'44','33','22'
];
var SBOB81OCOCALLFOLD75 = [
'KJO','QJO'
];

var SBOB81OCO3BETFOLD = [
'A5S', 'A4S', '87S', 'A3S', 'A2S','K8S','T8S', '76S'
];

var SBOB81OCO3BETFOLD50 = [
'Q8S','J8S','97S','65S', '86S'
];

var SBOB81OCO3BETFOLDCALL50 = [
'K9S'
];




///////////////SB vs BTN
////////////////////
///////////
///////////////////////////

var SBOB02OBTN = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
 'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S', '83S', '82S',
 'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S', '72S',
 'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S', '62S',
 'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '95O', '85O', '75O', '65O', '55', '54S', '53S', '52S',
 'A4O', 'K4O', 'Q4O', 'J4O', 'T4O', '94O', '84O', '74O', '64O', '54O', '44', '43S', '42S',
 'A3O', 'K3O', 'Q3O', 'J3O', 'T3O', '93O', '83O', '73O', '63O', '53O', '43O', '33', '32S',
 'A2O', 'K2O', 'Q2O', 'J2O', 'T2O', '92O', '82O', '72O', '62O', '52O', '42O', '32O', '22'
];


var SBOB24OBTN = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S','J4S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S',
 'A8O', 'K8O', 'Q8O', '98O', '88', '87S', '86S', '85S', 
 'A7O', 'K7O', '77', '76S', '75S',
 'A6O', 'K6O', '66', '65S', '64S',
 'A5O', 'K5O', '55', '54S',
 'A4O', 'K4O', '44',
 'A3O', '33',
 'A2O', '22'
];

var SBOB46OBTN = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S','Q5S','Q4S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S','J7S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
 'A9O', 'K9O', 'Q9O', 'J9O', 'T9O','99', '98S', '97S',
 'A8O', 'K8O', '88', '87S', '86S',
 'A7O', 'K7O','77', '76S',
 'A6O', 'K6O','66', '65S',
 'A5O', '55',
 'A4O', '44',
 'A3O', '33',
 'A2O', '22'
]
var SBOB68OBTN = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S','K6S','K5S','K4S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S',
 'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S',
 'A9O', 'K9O', '99',
 'A8O', '88',
 'A7O', '77', 
 'A6O', '66',
 'A5O', '55',
 'A4O', '44',
 'A3O', '33',
 'A2O', '22'
]

var SBOB810OBTN = 
['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S',
 'ATO', 'KTO', 'QTO', 'TT',
 'A9O', 'K9O', '99',
 'A8O', '88',
 'A7O', '77', 
 'A6O', '66',
 'A5O', '55',
 'A4O', '44',
 'A3O', '33',
 'A2O', '22'
]

var SBOB1012OBTN = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS',
 'ATO', 'KTO', 'QTO','TT',
 'A9O', '99',
 'A8O', '88',
 'A7O', '77', 
 'A6O', '66',
 'A5O', '55',
 'A4O', '44',
 'A3O', '33',
 'A2O', '22'
]
var SBOB1215OBTN = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S',
 'ATO', 'KTO', 'TT', 'T9S',
 'A9O', '99',
 'A8O', '88',
 'A7O', '77', 
 'A6O', '66',
 'A5O', '55',
 'A4O', '44',
 'A3O', '33',
 'A2O', '22'
]
var SBOB1520OBTN = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S',
 'ATO', 'KTO', 'TT', 'T9S', 'T8S',
 'A9O', '99', '98S',
 'A8O', '88', '87S',
 'A7O', '77', 
 'A6O', '66',
 'A5O', '55',
 'A4O', '44',
 '33',
 '22'
]
var SBOB2025OBTN = ['AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
 'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
 'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
 'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S',
 'ATO', 'TT', 'T9S', 'T8S',
 'A9O', '99', '98S',
 'A8O', '88',
 'A7O', '77',
 '66',
 '55',
 '44',
 '33',
 '22'
]

//25-35
var SBOB2535OBTNRAISE = [
'AA', 'AKS',,'AQO',
'AKO', 'KK',
'QQ', 'JJ','AQS','TT','99','88','77', 'AJS', 'AJO','KQS', 'ATS', 'KQO'
];
var SBOB2535OBTNRAISECALL50 = [
'ATO', 'A9S'
];
var SBOB2535OBTNCALL = [
'KJS','QTS',
'QJS', 'KTS',
'JTS', 'T9S', 'KQS', 'A8S', 'K9S','Q9S','J9S', 'KJO', 'QJO'
];
var SBOB2535OBTNCALLFOLD50 = [
'A9O', 'KTO'
];

var SBOB2535OBTN3BETFOLD = [
 "A5S", "A4S", "A3S", "A2S","A2S",'98S', '87S', '76S','97S', 'T7S', 'T8S', 'J8S','Q8S', 'K8S', 'K7S', 'A7S', 'A6S'
];

var SBOB2535OBTNSHOVE = [
'55','44','33','22', '66'
];

//36-50

var SBOB3650OBTNRAISE = [
'AA', 'AKS','KQS','AQO',
'AKO', 'KK',
'QQ', 'JJ','AQS','TT', 'AJS',
];
var SBOB3650OBTNRAISECALL50 = [
'KQO','AJO', 'ATS', 'KJS'
];

var SBOB3650OBTNRAISECALL25 = [
'99'
];

var SBOB3650OBTNCALL = [
'KJS','QTS',
'QJS', 'KTS',
'JTS', 'T9S', 'A9S','88','77','98S','A8S','66','K9S','Q9S','J9S','KJO', 'QJO','ATO'
];


var SBOB3650OBTN3BETFOLD = [
'A5S', 'A4S', 'A3S', 'A2S', 'A3S', 'A2S','76S','T8S', 'K8S','A6S','A7S','Q8S', 'J8S', 'T8S','97S','T7S', 'K7S'
];

var SBOB3650OBTN3BETFOLD50 = [
'87S'
];

var SBOB3650OBTN3BETFOLDCALL50 = [
'A5S'
];

//51-80

var SBOB5180OBTNRAISE = [
'AA', 'AKS','KQS','AQO', 'ATS',
'AKO', 'KK',
'QQ', 'JJ','AQS','TT', 'AJS', 'KJS','KQO','AJO'
];

var SBOB5180OBTNRAISECALL50 = [
'99'
];

var SBOB5180OBTNCALL = [
'QTS', 'ATO', 'KJO','QJO',
'QJS', 'KTS', ,'T8S',
'JTS', 'T9S',
'88', 'Q9J','J9S',
'77','66','55','A9S','A8S','98S', '87S','Q9S','K9S'
];


var SBOB5180OBTN3BETFOLD = [
'A7S','A6S', 'A5S', 'A4S', 'A3S', 'A2S', 'A3S','76S', 'K8S', '65S', 'Q8S','J8S','97S','86S', '65S'
];

var SBOB5180OBTNCALLFOLD50 = [
'A9O', 'KTO'
];



//////81

var SBOB81OBTNRAISE = [
'AA', 'AKS','AQS', 'AJS','AQO', 'ATS',
'AKO', 'KK', 'KQO', 'AJO',
'QQ', 'JJ', 'TT','KQS'
];

var SBOB81OBTNRAISECALL50 = [
'99', 'KJS', 'QJS'
];

var SBOB81OBTNCALL = [
'A8S','JTS','T9S' ,'Q9S','J9S','88', '77', '66', '55','A8S','ATO','98S','KTS','QTS','A9S','K9S', '87S', '98S','T8S', 'KJO', 'QJO'
];
var SBOB81OBTNCALLFOLD50 = [
'44','33','22'
];
var SBOB81OBTNCALLFOLD25 = [
'QTO', 'JTO'
];

var SBOB81OBTNCALLFOLD75 = [
'KTO', 'A9O'
];

var SBOB81OBTN3BETFOLD = [
'A5S', 'A4S', 'A3S', 'A2S','K8S', '76S', 'Q8S','J8S','97S','65S', '86S', '65S', 'T&'
];

var SBOB81OBTN3BETFOLD50 = [
'K7S', 'J7S'
];

var SBOB81OBTN3BETFOLDCALL50 = [
'A7S','A6S'
];


///////////////BB vs EP
////////////////////
///////////
///////////////////////////

var BBOB03OEPSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S', '83S', '82S',
'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S', '72S',
'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S', '62S',
'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '95O', '85O', '75O', '65O', '55', '54S', '53S', '52S',
'A4O', 'K4O', 'Q4O', 'J4O', 'T4O', '94O', '84O', '74O', '64O', '54O', '44', '43S', '42S',
'A3O', 'K3O', 'Q3O', 'J3O', 'T3O', '93O', '83O', '73O', '63O', '53O', '43O', '33', '32S',
'A2O', 'K2O', 'Q2O', 'J2O', 'T2O', '92O', '82O', '72O', '62O', '52O', '42O', '32O', '22'
];


var BBOB46OEPSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S','A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
'AJO', 'KJO', 'JJ', 'JTS', 'J9S',
'ATO', 'TT', 'T9S',
'A9O', '99',
'A8O', '88',
'77',
'66',
'A5O', '55',
'A4O', '44',
'33',
'22'
];

var BBOB68OEPSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S',
'AKO', 'KK', 'KQS',
'AQO', 'KQO', 'QQ',
'AJO', 'JJ',
'ATO', 'TT',
'A9O', '99',
'A8O', '88',
'77',
'66',
'A5O', '55',
'A4O', '44',
'33',
'22'
];

var BBOB68OEPCALL = [
'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'KJO', 'QJO', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S',
'KTO', 'QTO', 'JTO', 'T9S', 'T8S', 'T7S', 'T6S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S',
'A8O', 'K8O', 'T8O', '98O', '87S', '86S', '85S',
'A7O', '87O', '76S', '75S', '74S',
'A6O', '76O','65S', '64S', '63S',
'A5O', '65O', '54S', '53S',
'A4O', '54O', '43S', '42S',
'A3O', '32S',
'A2O'
];
   

//8-10
var BBOB810OEPSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'ATO', 'TT',
'99',
'88',
'77',
'66',
'55',
'44',
];

//8-10
var BBOB810OEPCALL = [
'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'KQO', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S',
'KJO', 'QJO', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S',
'KTO', 'QTO', 'JTO', 'T9S', 'T8S', 'T7S', 'T6S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S',
'A8O', 'K8O', '98O', '87S', '86S', '85S',
'A7O', '87O', '76S', '75S', '74S',
'A6O', '76O','65S', '64S', '63S',
'A5O', '54S', '53S',
'43S', '42S',
'33', '32S',
'22'
];


///10-16
var BBOB1016OEPSHOVE = [
'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S',
'AKO', 'KQS',
'AQO', 'KQO', 'QQ',
'AJO', 'JJ',
'ATO', 'TT',
'99',
'88',
'77',
'66',
'55',
'44',
'33',
'22'
];


var BBOB1016OEPCALL = [
'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'KJO', 'QJO', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S',
'KTO', 'QTO', 'JTO', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S', '95S',
'A8O', 'K8O', 'T8O', '98O', '87S', '86S', '85S',
'A7O', '87O', '76S', '75S', '74S',
'A6O', '65S', '64S',
'A5O', '54S', '53S',
'A4O', '43S',
'A3O',
'A2O'
];
var BBOB1016OEPSHOVECALL50 = [
'AA', 'KK'
];
 


//////17-25
var BBOB1725OEPRAISE = [
'AKS',
'AKO',
'QQ'
];
var BBOB1725OEPRAISECALL50 = [
'AA', 'KK'
];
var BBOB1725OEPSHOVERAISE50 = [
'JJ',
];
var BBOB1725OEPSHOVERAISE75 = [
'TT'
];

var BBOB1725OEPSHOVE = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS',
'AQO', 'KQO',
'AJO',
'99',
'88',
'77',
'66',
'55',
];

var BBOB1725OEPCALL = [
'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S',
'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'KJO', 'QJO', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S',
'ATO', 'KTO', 'QTO', 'JTO', 'T9S', 'T8S', 'T7S', 'T6S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S', '95S',
'A8O', '98O', '87S', '86S', '85S',
'76S', '75S',
'65S', '64S',
'54S',
'43S',
];

var BBOB1725OEPSHOVECALL50 = [
'44', '33', '22'
];

var BBOB1725OEP3BETFOLD = [
'A7O',
'A6O',
'A4O',
'A3O',
'A2O'
];

var BBOB1725OEP3BETFOLDCALL50 = [
'A5O'
];




////////////////////////26-35
var BBOB2635OEPRAISE = [
'AKS',
'AKO'
];

var BBOB2635OEPRAISECALL50 = [
'AA', 'JJ', 'AQS'
];

var BBOB2635OEPRAISECALL75 = [
'QQ', 'KK'
];
var BBOB2635OEPRAISECALL25 = [
'AQO',
];

var BBOB2635OEPCALL = [
'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'KQO', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S',
'AJO', 'KJO', 'QJO', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S',
'A8O', '98O', '88', '87S', '86S', '85S',
'77', '76S', '75S',
'66', '65S', '64S',
'55', '54S',
'44','43S',
'33',
'22'
];

var BBOB2635OEP3BETFOLD = [
'A5O',
'A4O',
];

var BBOB2635OEP3BETFOLD50 = [
'A3O',
'A2O'
];




///////////////36-50
var BBOB3650OEPRAISE = [
'AKS',
'AKO', 'QQ', 'KK'
];

var BBOB3650OEPRAISECALL75 = [
'AA',
];

var BBOB3650OEPCALL = [
'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S',
'AQO','KQO', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T7S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99',
'A8O', '98O', '88', '86S', 
'77',  '75S',
'66', '64S',
'A5O', '55', '53S',
'A4O', '44','43S',
'33',
'22'
];

var BBOB3650OEP3BETFOLDCALL50 = [
'T9S', 'T8S',
'98S', '97S',
'87S',
'76S',
'65S',
'54S'
];

///////////////51
var BBOB51OEPRAISE = [
'AA', 'AKS',
'AKO', 'QQ', 'KK'
];

var BBOB51OEPCALL = [
'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO','KQO', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S',
'AJO', 'KJO', 'QJO', 'JJ', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T7S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99',
'A8O', '98O', '88', 
'77',
'66', '64S',
'A5O', '55', '53S',
'A4O', '44','43S',
'33',
'22'
];

var BBOB51OEP3BETFOLDCALL50 = [
'T9S', 'T8S',
'98S', '97S',
'87S',
'76S',
'65S'
];

var BBOB51OEP3BETFOLDCALL25 = [
'JTS', 'J9S', 'T8S',
'97S',
'86S',
'75S',
'54S'
];

///////////////BB vs MP
////////////////////
///////////
///////////////////////////

var BBOB03OMPSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S', '83S', '82S',
'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S', '72S',
'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S', '62S',
'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '95O', '85O', '75O', '65O', '55', '54S', '53S', '52S',
'A4O', 'K4O', 'Q4O', 'J4O', 'T4O', '94O', '84O', '74O', '64O', '54O', '44', '43S', '42S',
'A3O', 'K3O', 'Q3O', 'J3O', 'T3O', '93O', '83O', '73O', '63O', '53O', '43O', '33', '32S',
'A2O', 'K2O', 'Q2O', 'J2O', 'T2O', '92O', '82O', '72O', '62O', '52O', '42O', '32O', '22'
];

var BBOB46OMPSHOVELIGHT = [
'Q4S', 'Q3S', 'Q2S', 
'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
'96S', '95S', '94S',
'85S', '84S',
'75S', '74S',
'64S', '63S',
'54S', '53S',
'43S', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', 'K6O','Q6O', '86O', '76O', 'K5O', 'K4O',
'Q9O', 'J9O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O',

];
var BBOB46OMPSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S',
'A9O', 'K9O', 'T9O', '99', '98S', '97S',
'A8O', '88', '87S', '86S',
'A7O', '77', '76S',
'A6O', '66', '65S',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];

var BBOB68OMPSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S',
'AKO', 'KK', 'KQS',
'AQO', 'KQO', 'QQ',
'AJO', 'JJ',
'ATO', 'TT',
'99',
'88',
'77',
'66',
'55',
'44',
'33',
'22'
];

var BBOB68OMPCALL = [
'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'KJO', 'QJO', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S',
'KTO', 'QTO', 'JTO', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S', '95S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '87S', '86S', '85S', '84S',
'A7O', 'K7O', '87O', '76S', '75S', '74S', '73S',
'A6O', '76O','65S', '64S', '63S',
'A5O', '65O', '54S', '53S',
'A4O', '54O', '43S', '42S',
'A3O', '32S',
'A2O'
];
    

//8-10
var BBOB810OMPSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS',
'AKO', 'KK', 'KQS',
'AQO', 'QQ',
'AJO', 'JJ',
'ATO', 'TT',
'99',
'88',
'77',
'66',
'A5O', '55',
'44',
'33',
'22'
];

//8-10
var BBOB810OMPCALL = [
'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'KQO', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'KJO', 'QJO', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'KTO', 'QTO', 'JTO', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S', '95S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '87S', '86S', '85S',
'A7O', '87O', '76S', '75S', '74S',
'A6O', '76O','65S', '64S',
'A5O', '65O', '54S', '53S',
'A4O', '43S', '42S',
'A3O', '32S',
'A2O',
];


///10-16
var BBOB1016OMPSHOVE = [
'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S',
'AKO', 'KQS', 'KJS',
'AQO', 'KQO', 'QQ', 'QJS',
'AJO', 'JJ',
'ATO', 'TT',
'99',
'88',
'77',
'66',
'55',
'44',
'33',
'22'
];


var BBOB1016OMPCALL = [
'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'KJO', 'QJO', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S',
'KTO', 'QTO', 'JTO', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S', '95S', '94S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '87S', '86S', '85S', '84S',
'A7O', '87O', '76S', '75S', '74S',
'A6O', '76O', '65S', '64S', '63S',
'A5O', '54S', '53S',
'A4O', '43S', '42S',
'A3O', '32S',
'A2O'
];

var BBOB1016OMPSHOVECALL50 = [
'AA', 'KK'
];


///17-25
var BBOB1725OMPRAISE = [
'AKS', 'AA', 'KK', 'JJ',
'AKO','QQ', 'AQS'
];

var BBOB1725OMPSHOVERAISE50 = [
'TT', '99', 'AQO'
];

var BBOB1725OMPSHOVE = [
'AJS', 'ATS', 'A9S', 'A8S', 'A7S',
'KQS',
'KQO',
'AJO',
'ATO',
'88',
'77',
'66',
'55',
'44',
'33', 
'22'
];

var BBOB1725OMPCALL = [
'A5S', 'A4S', 'A3S', 'A2S',
'KTS', 'K9S', 'K8S', 'K7S', 'K6S',
'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'KJO', 'QJO', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S',
'ATO', 'KTO', 'QTO', 'JTO', 'T9S', 'T8S', 'T7S', 'T6S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S', '95S',
'A8O', '98O', '87S', '86S', '85S',
'87O', '76S', '75S',
'65S', '64S',
'54S', '53S',
'43S',
];

var BBOB1725OMPSHOVECALL50 = [
'A9O', 'QJS', 'KJS', 'A6S'
];

var BBOB1725OMP3BETFOLD = [
'A6O',
'A4O',
'A3O',
'A2O'
];

var BBOB1725OMP3BETFOLDCALL50 = [
'A5O', 'A7O',
];



////////////////////////26-35
var BBOB2635OMPRAISE = [
'AKS', 'AA', 'JJ', 'AQS',
'AKO', 'QQ', 'KK', 'TT'
];

var BBOB2635OMPRAISECALL50 = [
'AQO',
];

var BBOB2635OMPCALL = [
'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'KQO', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'AJO', 'KJO', 'QJO', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S',
'ATO', 'KTO', 'QTO', 'JTO', 'T9S', 'T8S', 'T7S', 'T6S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '96S', '95S',
'A8O', '98O', '88', '85S',
'87O', '77', '74S',
'66', '65S', '64S',
'A5O', '55', '54S', '53S',
'44','43S',
'33', '32S',
'22'
];

var BBOB2635OMP3BETFOLDCALLFOLD255025 = [
'A4O',
];

var BBOB2635OMP3BETFOLD50 = [
'A3O', 'A2O', 'A6O','A7O', 'K8O'
];
var BBOB2635OMP3BETFOLDCALL50 = [
'T9S', 'T8S',
'98S', '97S',
'87S', '86S',
'76S', '75S',
'65S'
];
var BBOB2635OMP3BETFOLDCALL25 = [
'54S'
];




///////////////36-50
var BBOB3650OMPRAISE = [
'AKS',
'AKO', 'QQ', 'KK','AA', 'JJ'
];

var BBOB3650OMPRAISECALL50 = [
'AQS'
];

var BBOB3650OMPCALL = [
'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO','KQO', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'AJO', 'KJO', 'QJO', 'J8S', 'J7S', 'J6S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T7S', 'T6S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '96S',
'A8O', '98O', '88', '85S',
'87O', '77',  '75S',
'76O', '66', '64S',
'A5O', '55', '53S',
'A4O', '44','43S',
'33', '65S', '54S',
'22'
];

var BBOB3650OMPCALL25 = 
[
'A3O', 'A2O', 'A6O','A7O',
];

var BBOB3650OMP3BETFOLDCALL50 = [
'T9S', 'T8S',
'98S', '97S',
'87S', '86S',
'76S',
'65S',
'54S'
];

var BBOB3650OMP3BETFOLDCALL25 = [
 'JTS', 'J9S', 
'75S',
];


///////////////51
var BBOB51OMPRAISE = [
'AA', 'AKS',
'AKO', 'QQ', 'KK', 'JJ'
];

var BBOB51OMPCALL = [
'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO','KQO', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'AJO', 'KJO', 'QJO', 'J7S', 'J6S', 'JTS', 'J9S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99',
'A8O', '98O', '88',
'87O', '77',
'76O', '66',
'A5O', '55', '53S',
'A4O', '44','43S',
'33',
'22'
];

var BBOB51OMP3BETFOLDCALL50 = [
'T7S', '86S',
'98S', '97S',
'87S', 'T8S',
'76S', '75S',
'65S', '96S'
];

var BBOB51OMP3BETFOLDCALL25 = [
'T6S', '64S', 'J8S', 'J7S',
'85S', '96S','T9S', 
'54S'
];

///////////////BB vs CO
////////////////////
///////////
///////////////////////////

var BBOB03OCOSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S', '83S', '82S',
'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S', '72S',
'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S', '62S',
'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '95O', '85O', '75O', '65O', '55', '54S', '53S', '52S',
'A4O', 'K4O', 'Q4O', 'J4O', 'T4O', '94O', '84O', '74O', '64O', '54O', '44', '43S', '42S',
'A3O', 'K3O', 'Q3O', 'J3O', 'T3O', '93O', '83O', '73O', '63O', '53O', '43O', '33', '32S',
'A2O', 'K2O', 'Q2O', 'J2O', 'T2O', '92O', '82O', '72O', '62O', '52O', '42O', '32O', '22'
];


var BBOB46OCOSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S','96S', '95S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O','98O', '88', '87S', '86S', '85S',
'A7O', 'K7O', 'Q7O', '87O', '77', '76S', '75S', '74S',
'A6O', 'K6O', '66', '65S', '64S',
'A5O', 'K5O', '55', '54S',
'A4O', 'K4O', '44',
'A3O', 'K3O', '33',
'A2O', 'K2O', '22'
];

var BBOB68OCOSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS',
'ATO', 'KTO', 'TT',
'A9O', '99',
'A8O', '88', 'Q8O', 'J8O', 'T8O', '98O',
'A7O', '77', '87O', '76S', '75S', '74S', '73S',
'A6O', '66','65S', '64S', '63S',
'A5O', '55', '54S', '53S',
'A4O', '44', '43S', '42S',
'A3O', '33',
'A2O', '22'
];

var BBOB68OCOCALL = [
'K4S', 'K3S', 'K2S',
'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'QTO', 'JTO', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S',
'K9O', 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S', '95S', '94S',
'K8O', 'Q8O', 'J8O', 'T8O', '98O', '87S', '86S', '85S', '84S',
'K7O', 'Q7O', 'J7O', '87O', '76S', '75S', '74S', '73S',
'K6O', 'Q6O', '76O','65S', '64S', '63S',
'K5O', 'Q5O', '65O', '54S', '53S', '52S',
'K4O','54O', '43S', '42S',
'K3O','32S', '43O', '64O', '75O', '86O', '97O', 'T7O', '96O',
'K2O',
];
    

//8-10
var BBOB810OCOSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS',
'ATO', 'KTO', 'TT',
'A9O', '99',
'A8O', '88', 'Q8O', 'J8O',
'A7O', '77', '87O', '76S',
'A6O', '66',
'A5O', '55',
'A4O', '44', 
'A3O', '33',
'A2O', '22'
];

//8-10
var BBOB810OCOCALL = [
'K8S', 'K7S', 'K6S', 'K5S','K4S', 'K3S', 'K2S',
'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'KTO', 'QTO', 'JTO', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S',
'K9O', 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S', '95S', '94S',
'K8O', 'Q8O', 'J8O', 'T8O', '98O', '87S', '86S', '85S', '84S',
'K7O', 'Q7O', '87O', '76S', '75S', '74S', '73S',
'K6O', '76O','65S', '64S', '63S', '62S',
'65O', '54S', '53S', '53S', '52S',
'43S', '42S',
'32S',
];


///10-16
var BBOB1016OCOSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S',
'ATO', 'KTO', 'TT', 'T9S',
'A9O', '99',
'A8O', '88',
'A7O', '77',
'A6O', '66',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];


var BBOB1016OCOCALL = [
'K9S', 'K8S', 'K7S', 'K6S', 'K5S','K4S', 'K3S', 'K2S',
'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'KTO', 'QTO', 'JTO', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
'K9O', 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
'K8O', 'Q8O', 'J8O', 'T8O', '98O', '87S', '86S', '85S', '84S', '83S', '82S',
'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '76S', '75S', '74S', '73S', '72S',
'86O', '76O','65S', '64S', '63S', '62S',
'65O', '54S', '53S', '53S', '52S',
'43S', '42S',
'32S',
];


///17-25
var BBOB1725OCORAISE = [
'AKS', 'AA', 'KK', 'JJ', 'AJS', 'ATS',
'AKO','QQ', 'AQS', 'TT', '99', 'AQO', 'AJO','88', '99', 'KQS'
];

var BBOB1725OCOSHOVE = [
'A9S', 'A8S', 'A7S', 'A6O', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS', 'KTS',
'KQO', 'KJO', 'QJS', 'QTS', 'JTS',
'ATO', 'A9O', 'A8O', 'A7O',
'77',
'66',
'55',
'44',
'33', 
'22'
];

var BBOB1725OCOCALL = [
'K9S', 'K8S', 'K7S', 'K6S', 'K5S','K4S', 'K3S', 'K2S',
'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'QJO', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S',
'KTO', 'QTO', 'JTO', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S',
'K9O', 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S', '95S',
'K8O', 'Q8O', 'J8O',  'T8O', '98O', '87S', '86S', '85S',
'K7O', '87O', '76S', '75S', '74S',
'76O', '65S', '64S',
'54S', '53S',
'43S', '42S', '32S'
];

var BBOB1725OCO3BETFOLD50= [
'K6O',  'K5O', 'K4O', 'K3O', 'K2O', 'Q7O', 'Q6O'
];

var BBOB1725OCOSHOVECALL50 = [
'A5O',
'A4O',
'A3O',
'A2O'
];



////////////////////////26-35
var BBOB2635OCORAISE = [
'AKS', 'AA', 'JJ', 'AQS', 'AQO', 'KQS',
'AKO', 'QQ', 'KK', 'TT', 'AJS', '88', '99'
];

var BBOB2635OCORAISECALL50 = [
'AJO', '77', 'ATS'
];

var BBOB2635OCOCALL = [
'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S',
'KQO', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S',
'AJO', 'KJO', 'QJO', 'JTS', 'J9S', 'J5S',
'ATO', 'KTO', 'QTO', 'JTO', 'T9S', 'T5S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '95S',
'A8O', '98O', '85S',
'87O', '76O', '74S',
'66', 
'A5O', '55', '54S', '53S',
'A4O', '44','43S',
'A3O', '33', '32S',
'A2O', '22', 'A3O', 'A2O', 'A6O','A7O', 'K8O'
];



var BBOB2635OCO3BETFOLD50 = [
'K7O', 'K6O', 'K5O', 'K4O', 'K3O', 'K2O'
];

var BBOB2635OCO3BETFOLDCALL50 = [
'97S', '96S', 'T7S',
'87S', '86S',
'76S', '75S',
'65S', '54S',
'64S'
];

var BBOB2635OCO3BETFOLDCALL25 = [
'K5S', 'K4S', 'K3S', 'K2S',  'Q6S', 'Q5S', 'Q4S', 'J8S', 'J7S', 'J6S', 'Q8O', 'J8O', 'T8O', 'T8S',  '98S', 'T8S', 'T6S'
];




///////////////36-50
var BBOB3650OCORAISE = [
'AKS', 'AQS', 'AKS',
'AKO', 'QQ', 'KK','AA', 'JJ', 'TT' ,'AQO'
];


var BBOB3650OCORAISECALL25 = [
'99' ,'AJS'
];

var BBOB3650OCOCALL = [
'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQS','KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S',
'KQO', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'T8S', 'Q4S',
'AJO', 'KJO', 'QJO', 'JTS', 'J9S', 'J5S',
'ATO', 'KTO', 'QTO', 'JTO', 'T9S', 'T5S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '95S',
'A8O','K8O', 'Q8O', 'J8O', 'T8O', '98O', 'J8S', 'J7S',
'87O', '76O',
'66', '88', '77',
'A5O', '55', '54S', '53S',
'A4O', '44',
'A3O', '33', '32S',
'A2O', '22', 'A3O', 'A2O', 'A6O','A7O', 'K8O', 'A6O','A7O',
];


var BBOB3650OCO3BETFOLDCALL50 = [
'98S', '97S',
'87S', '86S',
'76S', '75S',
'65S', '64S',
'54S'
];

var BBOB3650OCO3BETFOLDCALL25 = [
'43S','53S', 'K5S', 'K4S', 'K3S', 'K2S',  'Q6S', 'Q5S', 'J6S' ,'74S', '85S', 'T6S', 'T7S', '96S'
];


///////////////51
var BBOB51OCORAISE = [
'AA', 'AKS', 'TT', 'AQS', 'AQO',
'AKO', 'QQ', 'KK', 'JJ'
];
var BBOB51OCORAISECALL25 = [
'AJS', '99'
];

var BBOB51OCOCALL = [
'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'KQO', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S',
'AJO', 'KJO', 'QJO',  'J6S', 'JTS', 'J9S', 'J6S', 'J5S',
'ATO', 'KTO', 'QTO', 'JTO', 'T5S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', 'T9S',
'A8O', '98O', '88', '95S',
'A7O', '87O', '77', '74S',
'A6O', '76O', '66',
'A5O', '55', '53S',
'A4O', '44','43S',
'A3O', '33', '32S',
'A2O', '22'
];

var BBOB51OCO3BETFOLDCALL50 = [
'T7S', '86S', 'J7S',
'98S', '97S', 'T6S',
'87S', 'T8S',
'76S', '75S',
'65S', '96S'
];

var BBOB51OCO3BETFOLDCALL25 = [
'64S', 'J8S',
'85S', 
'54S'
];
var BBOB51OCOCALLFOLD50 = [
'K8O', 'Q8O', 'J8O', 'T8O'
];

///////////////BB vs BTN
////////////////////
///////////
///////////////////////////

var BBOB03OBTNSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S', '83S', '82S',
'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S', '72S',
'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S', '62S',
'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '95O', '85O', '75O', '65O', '55', '54S', '53S', '52S',
'A4O', 'K4O', 'Q4O', 'J4O', 'T4O', '94O', '84O', '74O', '64O', '54O', '44', '43S', '42S',
'A3O', 'K3O', 'Q3O', 'J3O', 'T3O', '93O', '83O', '73O', '63O', '53O', '43O', '33', '32S',
'A2O', 'K2O', 'Q2O', 'J2O', 'T2O', '92O', '82O', '72O', '62O', '52O', '42O', '32O', '22'
];


var BBOB46OBTNSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S','96S', '95S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O','98O', '88', '87S', '86S', '85S', '84S',
'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S',
'A6O', 'K6O', 'Q6O', 'J6O', '86O', '76O', '66', '65S', '64S', '63S',
'A5O', 'K5O', 'Q5O', 'J5O', '65O', '55', '54S', '53S',
'A4O', 'K4O', 'Q4O', '44', '43S',
'A3O', 'K3O', 'Q3O', '33',
'A2O', 'K2O', 'Q2O', '22'
];

var BBOB68OBTNSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S',
'A9O', 'K9O', 'Q9O', 'J9O', '99',
'A8O', 'K8O', '88',
'A7O', 'K7O', '77', '87O',
'A6O', '66',,
'A5O', '55', 
'A4O', '44',
'A3O', '33',
'A2O', '22'
];

var BBOB68OBTNCALL = [
"Q8O","J8O","Q7O","J7O","T7O","97O","K6O","Q6O","J6O","T6O","96O","86O","K5O",
"Q5O","J5O","T5O","95O","85O","75O","K4O","Q4O",
"J4O","T4O","84O","74O","64O","43O","53O","63O",
'Q4S', 'Q3S', 'Q2S',
'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'T8S', 'T7S', 'T6S', 'T5S', 'T4S',
'T9O', '98S', '97S', '96S', '95S', '94S',
'Q8O', 'J8O', 'T8O', '98O', '87S', '86S', '85S', '84S',
'Q7O', 'J7O', '87O', '76S', '75S', '74S', '73S',
'K6O', 'Q6O', '76O','65S', '64S', '63S',
'K5O', 'Q5O', '65O', '54S', '53S', '53S',
'K4O','54O', '43S', '42S',
'K3O','32S', '43O', '64O', '75O', '86O', '97O', 'T7O', '96O',
'K2O', '52S', '62S', '72S', '83S', '82S', '93S', '92S', 'T3S', 'T2S',"Q3O","Q2O", 'J2O', 'J3O'
];
    

//8-10
var BBOB810OBTNSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S',
'A9O', '99',
'A8O', '88', 'Q8O',
'A7O', '77',
'A6O', '66',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];

//8-10
var BBOB810OBTNCALL = [
'K6S', 'K5S','K4S', 'K3S', 'K2S',
'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
'K9O', 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
'K8O', 'Q8O', 'J8O', 'T8O', '98O', '87S', '86S', '85S', '84S', '83S', '82S',
'K7O', 'Q7O', '87O', '76S', '75S', '74S', '73S', '72S',
'K6O', '76O','65S', '64S', '63S', '62S',
'65O', '54S', '53S', '53S', '52S',
'43S', '42S',
'32S', '54O', '86O', 'K5O', 'K4O', 'K3O', 'K2O', 'Q6O', 'J7O', 'T7O', '97O'
];


///10-16
var BBOB1016OBTNSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S',
'A9O', '99',
'A8O', '88', 'Q8O', 'J8O', 'T8O', '98O',
'A7O', '77', '87O',
'A6O', '66',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22'
];


var BBOB1016OBTNCALL = [
'K8S', 'K7S', 'K6S', 'K5S','K4S', 'K3S', 'K2S',
'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
'K9O', 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
'K8O', 'Q8O', 'J8O', 'T8O', '98O', '87S', '86S', '85S', '84S', '83S', '82S',
'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '76S', '75S', '74S', '73S', '72S',
'86O', '76O','65S', '64S', '63S', '62S', 'K6O', 'K5O', 'K4O', 'Q6O',
'65O', '54S', '53S', '53S', '52S',
'43S', '42S',
'32S', '54O', '75O', '96O'
];


///17-25
var BBOB1725OBTNRAISE = [
'AKS', 'AA', 'KK', 'JJ', 'AJS', 'ATS', 'A9S', 'KJS', '66', '88', '77', 'ATO',
'AKO','QQ', 'AQS', 'TT', '99', 'AQO', 'AJO','88', '99', 'KQS', 'KQO',
];

var BBOB1725OBTNSHOVE = [
'A8S', 'A7S', 'A6O', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS', 'QJO',
'KJO', 'QJS', 'QTS', 'JTS',
'A9O', 'A8O', 'A7O',
'77', 'T9S',
'66',
'55',
'44',
'33', 
'22', 'A5O',
'A4O',
'A3O',
'A2O'
];

var BBOB1725OBTNCALL = [
'K9S', 'K8S', 'K7S', 'K6S', 'K5S','K4S', 'K3S', 'K2S',
'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'KTO', 'QTO', 'JTO', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S',
'K9O', 'Q9O', 'J9O', 'T9O', '98S', '97S', '96S', '95S', '94S',
'K8O', 'Q8O', 'J8O',  'T8O', '98O', '87S', '86S', '85S', '84S',
'K7O', '87O', '76S', '75S', '74S', '73S', '97O','65O',
'76O', '65S', '64S', '63S', '62S',
'54S', '53S', '52S',
'43S', '42S', '32S'
];

var BBOB1725OBTN3BETFOLD25 = [
'J7O',  'T7O', 'J6O', 'T6O', '96O', '86O', '75O', '54O'
];
var BBOB1725OBTN3BETFOLD50 = [
'K6O',  'K5O', 'K4O', 'K3O', 'K2O', 'Q7O', 'Q6O', 'Q5O', 'Q4O','Q3O', 'Q2O'
];



////////////////////////26-35
var BBOB2635OBTNRAISE = [
'AKS', 'AA', 'JJ', 'AQS', 'AQO', 'KQS', 'A9S', 'ATO',
'AKO', 'QQ', 'KK', 'TT', 'AJS', '88', '99', 'AJO', '77', 'ATS', 'KQO', 'KJS'
];


var BBOB2635OBTNCALL = [
'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S','K4S', 'K3S', 'K2S',
'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S',
'KJO', 'QJO', 'JTS', 'J9S', 'J5S',
'KTO', 'QTO', 'JTO', 'T9S', 
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O',
'A8O', 'Q8O', 'J8O', 'T8O', '98O',
'87O', '76O',
'66', 'T8S', '97O',
'A5O', '55',
'A4O', '44', '65O',
'A3O', '33',  'J8S', 'J7S', 'J6S', 'T8S',
'A2O', '22', 'A3O', 'A2O', 'A6O','A7O', 'K8O'
];



var BBOB2635OBTN3BETFOLD50 = [
'K7O', 'K6O', 'K5O', 'K4O', 'K3O', 'K2O', 'Q7O', 'Q6O', '54O'
];

var BBOB2635OBTN3BETFOLDCALL50 = [
'97S', '96S', 'T7S', 'T6S', '85S',
'87S', '86S',
'76S', '75S',
'65S', '54S',
'64S'
];

var BBOB2635OBTN3BETFOLDCALL25 = [
'T5S', 'Q3S', 'Q2S', 'Q4S',  '98S', '53S', '52S', '84S',
'43S', '32S', '63S', '42S', '95S', '74S', '73S'
];


///////////////36-50
var BBOB3650OBTNRAISE = [
'AKS', 'AQS', 'AKS', '99', 'AJS',
'AKO', 'QQ', 'KK','AA', 'JJ', 'TT' ,'AQO'
];


var BBOB3650OBTNRAISECALL50 = [
'KQS' ,'AJO', 'ATS'
];

var BBOB3650OBTNCALL = [
'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S', 
'KQO', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'T8S', 'Q4S', 'Q6S',
'AJO', 'KJO', 'QJO', 'JTS', 'J9S',
'ATO', 'KTO', 'QTO', 'JTO', 'T9S', 'T5S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '95S',
'A8O','K8O', 'Q8O', 'J8O', 'T8O', '98O', 'J8S', 'J7S',
'87O', '76O',
'66', '88', '77',
'A5O', '55', '54S', '53S',
'A4O', '44',
'A3O', '33', '32S',
'A2O', '22', 'A3O', 'A2O', 'A6O','A7O', 'K8O', 'A6O','A7O',
'K7O', 'Q7O', 'J7O' ,'T7O', '97O', '65O', '86O', '63S', '52S', '42S', '84S'
];


var BBOB3650OBTN3BETFOLDCALL50 = [
'96S', '97S', 'Q5S', 'Q4S', '95S',
'87S', '86S', 'J6S', 'J5S', 'T7S', 'T6S',
'76S', '75S',
'65S', '64S',
'54S'
];

var BBOB3650OBTN3BETFOLDCALL25 = [
'43S','53S' ,'74S', '85S', 'T5S', 'Q2S', 'Q3S', '98S'
];


///////////////51
var BBOB51OBTNRAISE = [
'AA', 'AKS', 'TT', 'AQS', 'AQO',
'AKO', 'QQ', 'KK', 'JJ', 'AJS', '99'
];
var BBOB51OBTNRAISECALL50 = [
'ATS', 'KQS', 'AJO'
];

var BBOB51OBTNCALL = [
'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K6S', 'K5S',
'KQO', 'QJS', 'QTS', 'Q9S', 'Q8S',
'KJO', 'QJO',  'J6S', 'JTS', 'J9S',
'ATO', 'KTO', 'QTO', 'JTO',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O',
'A8O', '98O', '88',
'A7O', '87O', '77',
'A6O', '76O', '66',
'A5O', '55',
'A4O', '44', '52S','42S',
'A3O', '33', '32S',
'A2O', '22', 'K8O', 'Q8O', 'J8O', 'T8O', 'K7O', '97O', '86O', '65O'
];

var BBOB51OBTN3BETFOLDCALL50 = [
'T7S', '86S', 'J7S', 'J8S', '43S',
'98S', '97S',
'87S', 'T8S', '54S',
'76S', '75S', '64S',
'65S', '96S'
];

var BBOB51OBTN3BETFOLDCALL25 = [
'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S', '63S',
'K4S', 'K3S', 'K2S', 'J6S', 'J5S', 'T9S', 'T6S', 'T5S',
'85S', '74S', '95S', '53S', '84S'
];


///////////////BB vs SB
////////////////////
///////////
///////////////////////////

var BBOB03OSBSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S', '83S', '82S',
'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S', '72S',
'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S', '62S',
'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '95O', '85O', '75O', '65O', '55', '54S', '53S', '52S',
'A4O', 'K4O', 'Q4O', 'J4O', 'T4O', '94O', '84O', '74O', '64O', '54O', '44', '43S', '42S',
'A3O', 'K3O', 'Q3O', 'J3O', 'T3O', '93O', '83O', '73O', '63O', '53O', '43O', '33', '32S',
'A2O', 'K2O', 'Q2O', 'J2O', 'T2O', '92O', '82O', '72O', '62O', '52O', '42O', '32O', '22'
];


var BBOB46OSBSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S', 'T2S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S', '94S', '93S', '92S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S', '85S', '84S', '83S', '82S',
'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S', '75S', '74S', '73S',
'A6O', 'K6O', 'Q6O', 'J6O', 'T6O', '96O', '86O', '76O', '66', '65S', '64S', '63S',
'A5O', 'K5O', 'Q5O', 'J5O', 'T5O', '95O', '85O', '75O', '65O', '55', '54S', '53S',
'A4O', 'K4O', 'Q4O', 'J4O', 'T4O', '44', '43S',
'A3O', 'K3O', 'Q3O', 'J3O', 'T3O', 'T2O', '33', 
'A2O', 'K2O', 'Q2O', 'J2O', '22'
];

var BBOB46OSBSHOVELIGHT = [
'72S', '62S', '52S', '42S', '32S', '92O',  '93O', '83O', '63O', '53O', '43O', '94O', '84O', '74O', '64O', '54O'
];
    

var BBOB68OSBSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S', 'T5S', 'T4S', 'T3S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S', '95S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S',
'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '97O', '87O', '77', '76S',
'A6O', 'K6O', 'Q6O', '66',
'A5O', 'K5O', 'Q5O', 'J5O', '55',
'A4O', 'K4O', 'Q4O', 'J4O', '44',
'A3O', 'K3O', 'Q3O',  '33', 
'A2O', 'K2O', 'Q2O', '22'
];    

//8-10
var BBOB810OSBSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S', 'J8S', 'J7S', 'J6S', 'J5S', 'J4S', 'J3S', 'J2S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S', 'T8S', 'T7S', 'T6S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99', '98S', '97S', '96S',
'A8O', 'K8O', 'Q8O', 'J8O', 'T8O', '98O', '88', '87S', '86S',
'A7O', 'K7O', 'Q7O', 'J7O', 'T7O', '77', '76S',
'A6O', 'K6O', 'Q6O', '66',
'A5O', 'K5O', 'Q5O', '55',
'A4O', 'K4O', 'Q4O', '44',
'A3O', 'K3O', 'Q3O',  '33', 
'A2O', 'K2O', 'Q2O', '22'
];


///10-16
var BBOB1016OSBSHOVE = [
'AA', 'AKS', 'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KK', 'KQS', 'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S',
'AQO', 'KQO', 'QQ', 'QJS', 'QTS', 'Q9S', 'Q8S',
'AJO', 'KJO', 'QJO', 'JJ', 'JTS', 'J9S',
'ATO', 'KTO', 'QTO', 'JTO', 'TT', 'T9S',
'A9O', 'K9O',  '99', '98S',
'A8O', 'K8O', '88', '87S',
'A7O', 'K7O', '77', '76S',
'A6O', '66',
'A5O', '55',
'A4O', '44',
'A3O', '33', 
'A2O', '22'
];


var BBOB1016OSBCALL = [
'K4S', 'K3S', 'K2S', '86S',  '97S', '96S', '95S', '84S',
'Q7S', 'Q6S', 'Q5S', 'Q4S', 'Q3S', 'Q2S',
'J8S', 'J7S', 'J6S', 'J5S', 'Q6O', 'Q9O', 'J9O', 'T9O',
'K6O','K5O','K4O','K3O','K2O',  'T8S', '65S', '85S',
'T7S', 'T6S', 'T5S',  'Q8O', 'J8O', 'T8O', '98O',
'Q7O', 'J7O', 'T7O', '97O', '87O', '65s', '64S', '63S', '75S', '74S', '73S',
'54O', '65O', '76O', '86O', '32S', '42S', '43S','52S', '53S', '54S'
];


///17-25
var BBOB1725OSBRAISE = [
"AA","AKS","AQS","AJS","ATS","AKO","KK",
"KQS","AQO","QQ","AJO","JJ","TT","99"
];

var BBOB1725OSBSHOVE = [
"A9S","A8S","A7S","A6S","A5S","A4S","A3S","A2S","KJS","KTS","QJS","QTS",
"KQO","KJO","QJO","JTS","ATO","A9O","A8O","A7O","A6O","A5O","A4O","A3O","A2O",
"88","77","66","55","44","33","22"
];

var BBOB1725OSBCALL = [
"K8S", "K7S", "K6S", "K5S", "K4S", "K3S", "K2S", "Q8S", "Q7S", "Q6S", "Q5S", "J8S",
"J7S", "J6S", "T8S", "T7S", "T6S", "97S", "96S", "87S", "86S", "85S", "76S", "75S", 
"65S", "64S", "54S", "43S", "K9O", "Q9O", "J9O", "T9O", "K8O", "Q8O", "J8O", "T8O", 
"98O", "K7O", "Q7O", "J7O", "T7O", "97O", "87O", "86O", "76O", "65O"
];

var BBOB1725OSB3BETFOLD25 = [
"J5S", "J4S", "J3S", "J2S", "T5S", "T4S", "T3S", "T2S", "95S", "94S", "93S", 
"92S", "84S", "83S", "82S", "73S", "72S", "62S", "Q6O", "J6O", "T6O", 
"96O", "K5O", "Q5O", "J5O", "T5O", "95O", "85O", "75O", "K4O", "Q4O", 
"64O", "54O", "K3O", "Q3O", "53O", "43O", "K2O", "Q2O", "42O", "32O"
];

var BBOB1725OSB3BETFOLDCALL25 = [
"K6O", "Q4S", "Q3S", "Q2S", "85S", "74S", "63S", "53S", "52S", "42S", "32S"
];
var BBOB1725OSBSHOVECALL50 = [
"K9S", "Q9S", "J9S", "T9S", "98S", "KTO", "QTO", "JTO"
];


////////////////////////26-35
var BBOB2635OSBRAISE = [
'AKS', 'AA', 'JJ', 'AQS', 'AQO', 'KQS', 'ATO',
'AKO', 'QQ', 'KK', 'TT', 'AJS', '88', '99', 'AJO', 'ATS', 'KQO',
];


var BBOB2635OSBCALL = [
'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S','K4S', 'K3S', 'K2S',
'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'KJO', 'QJO', 'JTS', 'J9S', 'J8S', 'J7S',
'KTO', 'QTO', 'JTO', 'T9S', 'T8S', 'T7S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '98S','97S',
'A8O', 'Q8O', 'J8O', 'T8O', '98O', '87S','86S',
'87O', '76O', '76S', '65S', '54S',
'A5O',
'A4O', '65O',
'A3O', 'J8S', 'J7S', 'J6S',
'A2O', 'A3O', 'A2O', 'A6O','A7O', 'K8O'
];

var BBOB2635OSBSHOVE = [
'77',
'66',
'55',
'44',
'33',
'22'
];
    

var BBOB2635OSB3BETFOLD25 = [
'J7O' ,'T7O', '97O', '86O'
];

var BBOB2635OSB3BETFOLD50 = [
'K7O', 'K6O', 'K5O', 'K4O', 'K3O', 'K2O', 'Q7O', 'J5S', 'T5S', '95S', '84S', '63S'
];
    
var BBOB2635OSB3BETFOLDCALL50 = [
'96S', 'T6S', 'Q4S', 'Q3S', 'Q2S',
'J6S', '74S', '32S', '85S', '53S',
'64S'
];

var BBOB2635OSB3BETFOLDCALL25 = [
'Q5S', '75S', '43S'
];


///////////////36-50
var BBOB3650OSBRAISE = [
'AKS', 'AQS', 'AKS', 'AJS',
'AKO', 'QQ', 'KK','AA', 'JJ', 'TT' ,'AQO'
];


var BBOB3650OSBRAISECALL50 = [
'KQS' ,'99', 'ATS'
];

var BBOB3650OSBRAISECALL25 = [
'KQO' 
];

var BBOB3650OSBCALL = [
'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S', 
'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'T8S', 'Q6S',
'AJO', 'KJO', 'QJO', 'JTS', 'J9S',
'ATO', 'KTO', 'QTO', 'JTO', 'T9S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '98S',
'A8O','K8O', 'Q8O', 'J8O', 'T8O', '98O', 'J8S',
'87O', 'K7O', '76O', '76S', '87S',
'66', '88', '77',
'A5O', '55', '65O',
'A4O', '44',
'A3O', '33',
'A2O', '22', 'A3O', 'A2O', 'A6O','A7O', 'K8O', 'A6O','A7O',
'K7O', '97O',
];


var BBOB3650OSB3BETFOLDCALL50 = [
'96S', 'Q4S', 'Q2S', 'Q3S',
'86S',  'J6S', 'T6S',
'75S', '85S',
'65S', '64S',
'54S'
];

var BBOB3650OSB3BETFOLDCALL25 = [
 'J7S', 'Q5S', 'T7S', '97S' ,'65S'
];

var BBOB3650OSB3BETFOLD50 = [
'43S','53S' ,'74S', '32S'
];

var BBOB3650OSBCALLFOLD50 = [
'Q7O','J7O' ,'T7O', '86O'
];

///////////////51
var BBOB51OSBRAISE = [
'AKS', 'AQS', 'AKS', 'AJS', 'AJO',
'AKO', 'QQ', 'KK','AA', 'JJ', 'TT' ,'AQO'
];
var BBOB51OSBRAISECALL50 = [
'KQS', 'ATS'
];

var BBOB51OSBRAISECALL25 = [
'KQO'
];
var BBOB51OSBCALL = [
'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS', 'KTS', 'K9S', 'K8S', 'K7S', 'K6S', 'K5S', 'K6S', 'K5S', 'K4S', 'K3S', 'K2S',
'QJS', 'QTS', 'Q9S', 'Q8S', 'Q7S', 'Q6S',
'KJO', 'QJO',  'J6S', 'JTS', 'J9S', 'J8S',
'ATO', 'KTO', 'QTO', 'JTO', 'T9S', '98S', '87S',
'A9O', 'K9O', 'Q9O', 'J9O', 'T9O', '99',
'A8O', '98O', '88', 'T8S',
'A7O', '77', '76S',
'A6O', '66',
'A5O', '55',
'A4O', '44',
'A3O', '33',
'A2O', '22', 'K8O',
];

var BBOB51OSB3BETFOLDCALL50 = [
 '86S', 'J6S', '85S',
'T6S',
'54S',
'75S', '64S', '96S'
];

var BBOB51OSB3BETFOLDCALL25 = [
'J7S' ,'T7S', '97S', '65S'
];

var BBOB51OSB3BETFOLD50 = [
'Q5S', 'Q4S', 'Q2S', 'Q3S' ,'74S', '53S', '43S', '32S'
];

var BBOB51OSBCALLFOLD50 = [
'K7O', 'Q8O', 'J8O' ,'76O'
];
var BBOB51OSBCALLFOLD75 = [
'T8O', '87O',
];


//////////////////////
////////////////////3BET EP VS EP
/////////////////////
////////////////////


var EP3BAHO1620OEPCALL = [
'AJS', 
'KQS', 'KJS',
'AQO', 'QJS',
'JTS',
'T9S'
];

var EP3BAHO1620OEPFOLD = [
'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS',
'KQO', 'QTS',
'AJO', 'KJO',
'ATO',
'98S',
'88',
'77'
];

var EP3BAHO1620OEPSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT',
'99'
];


//////21-25
var EP3BAHO2125OEPCALL = [
'AJS', 
'ATS',
'KQS', 'KJS',
'QJS',
'JTS',
'T9S'
];

var EP3BAHO2125OEPSHOVECALL50 = [
'AQO'
];

var EP3BAHO2125OEPFOLD = [
'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS',
'KQO', 'QTS',
'AJO', 'KJO',
'ATO',
'J9S',
'98S',
'87S',
'76S'
];

var EP3BAHO2125OEPSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT'
];

var EP3BAHO2125OEPSHOVE50 = [
'99',
'88',
'77'
];


////26-35BB
var EP3BAHO2635OEPCALL = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JTS',
'T9S',
'98S'
];

var EP3BAHO2635OEPFOLD = [
'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQO',
'AJO', 'KJO', 'J9S',
'ATO',
'87S',
'76S'
];

var EP3BAHO2635OEPSHOVE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ',
'JJ',
'TT'
];

var EP3BAHO2635OEPSHOVE50 = [
'77'
];

var EP3BAHO2635OEPSHOVECALL50 = [
'99',
'88'
];


////36-50BB
var EP3BAHO3660OEPCALL = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JJ', 'JTS',
'TT', 'T9S',
'99', '98S',
'88','87S'
];

var EP3BAHO3660OEPFOLD = [
'A6S', 'A3S', 'A2S',
'KJO', 'J9S',
'ATO',
'77', '76S',
'66'
];

var EP3BAHO3660OEPRAISE = [
'AA', 'KK'
];

var EP3BAHO3660OEPRAISE50= [
'AKS',
'AKO',
'QQ'
];

var EP3BAHO3660OEP4BETFOLD = [
'A8S', 'A7S','A5S', 'A4S'
];
var EP3BAHO3660OEP4BETFOLD50 = [
'KQO',
'AJO'
];

///////61
var EP3BAHO61OEPCALL = [
'AQS', 'AJS', 'ATS', 'A9S', 'AKS', 'A5S', 'A4S',
'AKO', 'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JJ', 'JTS',
'TT', 'T9S',
'99', '98S',
'88',
'87S', '77',
'66'
];

var EP3BAHO61OEPFOLD = [
'A6S', 'A3S', 'A2S', 'KQO',
'AJO',
'KJO', 'J9S',
'ATO',
 '76S',
'66',
'55',
'44',
'33',
'22'
];

var EP3BAHO61OEPRAISE = [
'AA', 'KK'
];

var EP3BAHO61OEPRAISE50= [
'QQ'
];

var EP3BAHO61OEP4BETFOLD = [
'A8S', 'A7S','A6S', 'A3S', 'A2S',
];

//////////////////////
////////////////////3BET EP VS MP
/////////////////////
////////////////////
var EP3BAHO1620OMPCALL = [
'AJS', 
'KQS', 'KJS',
'AQO', 'QJS',
'JTS',
'T9S'
];

var EP3BAHO1620OMPFOLD = [
'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS',
'KQO', 'QTS',
'AJO', 'KJO',
'ATO',
'98S',
'88',
'77'
];

var EP3BAHO1620OMPSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT',
'99'
];


//////21-25
var EP3BAHO2125OMPCALL = [
'AJS', 
'ATS',
'KQS', 'KJS',
'QJS',
'JTS',
'T9S'
];

var EP3BAHO2125OMPSHOVECALL50 = [
'AQO'
];

var EP3BAHO2125OMPFOLD = [
'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS',
'KQO', 'QTS',
'AJO', 'KJO',
'ATO',
'J9S',
'98S',
'87S',
'76S'
];

var EP3BAHO2125OMPSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT'
];

var EP3BAHO2125OMPSHOVE50 = [
'99',
'88',
'77'
];


////26-35BB
var EP3BAHO2635OMPCALL = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JTS',
'T9S',
'98S'
];

var EP3BAHO2635OMPFOLD = [
'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQO',
'AJO', 'KJO', 'J9S',
'ATO',
'87S',
'76S'
];

var EP3BAHO2635OMPSHOVE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ',
'JJ',
'TT'
];

var EP3BAHO2635OMPSHOVE50 = [
'77'
];

var EP3BAHO2635OMPSHOVECALL50 = [
'99',
'88'
];


////36-50BB
var EP3BAHO3660OMPCALL = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JJ', 'JTS',
'TT', 'T9S',
'99', '98S',
'88','87S'
];

var EP3BAHO3660OMPFOLD = [
'A6S', 'A3S', 'A2S',
'KJO', 'J9S',
'ATO',
'77', '76S',
'66'
];

var EP3BAHO3660OMPRAISE = [
'AA', 'KK'
];

var EP3BAHO3660OMPRAISE50= [
'AKS',
'AKO',
'QQ'
];

var EP3BAHO3660OMP4BETFOLD = [
'A8S', 'A7S','A5S', 'A4S'
];
var EP3BAHO3660OMP4BETFOLD50 = [
'KQO',
'AJO'
];

///////61
var EP3BAHO61OMPCALL = [
'AQS', 'AJS', 'ATS', 'A9S', 'AKS', 'A5S', 'A4S',
'AKO', 'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JJ', 'JTS',
'TT', 'T9S',
'99', '98S',
'88',
'87S', '77',
'66'
];

var EP3BAHO61OMPFOLD = [
'A6S', 'A3S', 'A2S', 'KQO',
'AJO',
'KJO', 'J9S',
'ATO',
    '76S',
'66',
'55',
'44',
'33',
'22'
];

var EP3BAHO61OMPRAISE = [
'AA', 'KK'
];

var EP3BAHO61OMPRAISE50= [
'QQ'
];

var EP3BAHO61OMP4BETFOLD = [
'A8S', 'A7S','A6S', 'A3S', 'A2S',
];


//////////////////////
////////////////////3BET EP VS CO
/////////////////////
////////////////////
var EP3BAHO1620OCOCALL = [
'AJS', 
'KQS', 'KJS',
'AQO', 'QJS',
'JTS',
'T9S'
];

var EP3BAHO1620OCOFOLD = [
'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS',
'KQO', 'QTS',
'AJO', 'KJO',
'ATO',
'98S',
'88',
'77'
];

var EP3BAHO1620OCOSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT',
'99'
];


//////21-25
var EP3BAHO2125OCOCALL = [
'AJS', 
'ATS',
'KQS', 'KJS',
'QJS',
'JTS',
'T9S'
];

var EP3BAHO2125OCOSHOVECALL50 = [
'AQO'
];

var EP3BAHO2125OCOFOLD = [
'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS',
'KQO', 'QTS',
'AJO', 'KJO',
'ATO',
'J9S',
'98S',
'87S',
'76S'
];

var EP3BAHO2125OCOSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT'
];

var EP3BAHO2125OCOSHOVE50 = [
'99',
'88',
'77'
];


////26-35BB
var EP3BAHO2635OCOCALL = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JTS',
'T9S',
'98S'
];

var EP3BAHO2635OCOFOLD = [
'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQO',
'AJO', 'KJO', 'J9S',
'ATO',
'87S',
'76S'
];

var EP3BAHO2635OCOSHOVE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ',
'JJ',
'TT'
];

var EP3BAHO2635OCOSHOVE50 = [
'77'
];

var EP3BAHO2635OCOSHOVECALL50 = [
'99',
'88'
];


////36-50BB
var EP3BAHO3660OCOCALL = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JJ', 'JTS',
'TT', 'T9S',
'99', '98S',
'88','87S'
];

var EP3BAHO3660OCOFOLD = [
'A6S', 'A3S', 'A2S',
'KJO', 'J9S',
'ATO',
'77', '76S',
'66'
];

var EP3BAHO3660OCORAISE = [
'AA', 'KK'
];

var EP3BAHO3660OCORAISE50= [
'AKS',
'AKO',
'QQ'
];

var EP3BAHO3660OCO4BETFOLD = [
'A8S', 'A7S','A5S', 'A4S'
];
var EP3BAHO3660OCO4BETFOLD50 = [
'KQO',
'AJO'
];

///////61
var EP3BAHO61OCOCALL = [
'AQS', 'AJS', 'ATS', 'A9S', 'AKS', 'A5S', 'A4S',
'AKO', 'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JJ', 'JTS',
'TT', 'T9S',
'99', '98S',
'88',
'87S', '77',
'66'
];

var EP3BAHO61OCOFOLD = [
'A6S', 'A3S', 'A2S', 'KQO',
'AJO',
'KJO', 'J9S',
'ATO',
    '76S',
'66',
'55',
'44',
'33',
'22'
];

var EP3BAHO61OCORAISE = [
'AA', 'KK'
];

var EP3BAHO61OCORAISE50= [
'QQ'
];

var EP3BAHO61OCO4BETFOLD = [
'A8S', 'A7S','A6S', 'A3S', 'A2S',
];

//////////////////////
////////////////////3BET EP VS BTN
/////////////////////
////////////////////
var EP3BAHO1620OBTNCALL = [
'AJS', 
'KQS', 'KJS',
'AQO', 'QJS',
'JTS',
'T9S'
];

var EP3BAHO1620OBTNFOLD = [
'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS',
'KQO', 'QTS',
'AJO', 'KJO',
'ATO',
'98S',
'88',
'77'
];

var EP3BAHO1620OBTNSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT',
'99'
];


//////21-25
var EP3BAHO2125OBTNCALL = [
'AJS', 
'ATS',
'KQS', 'KJS',
'QJS',
'JTS',
'T9S'
];

var EP3BAHO2125OBTNSHOVECALL50 = [
'AQO'
];

var EP3BAHO2125OBTNFOLD = [
'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS',
'KQO', 'QTS',
'AJO', 'KJO',
'ATO',
'J9S',
'98S',
'87S',
'76S'
];

var EP3BAHO2125OBTNSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT'
];

var EP3BAHO2125OBTNSHOVE50 = [
'99',
'88',
'77'
];


////26-35BB
var EP3BAHO2635OBTNCALL = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JTS',
'T9S',
'98S'
];

var EP3BAHO2635OBTNFOLD = [
'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQO',
'AJO', 'KJO', 'J9S',
'ATO',
'87S',
'76S'
];

var EP3BAHO2635OBTNSHOVE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ',
'JJ',
'TT'
];

var EP3BAHO2635OBTNSHOVE50 = [
'77'
];

var EP3BAHO2635OBTNSHOVECALL50 = [
'99',
'88'
];


////36-50BB
var EP3BAHO3660OBTNCALL = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JJ', 'JTS',
'TT', 'T9S',
'99', '98S',
'88','87S'
];

var EP3BAHO3660OBTNFOLD = [
'A6S', 'A3S', 'A2S',
'KJO', 'J9S',
'ATO',
'77', '76S',
'66'
];

var EP3BAHO3660OBTNRAISE = [
'AA', 'KK'
];

var EP3BAHO3660OBTNRAISE50= [
'AKS',
'AKO',
'QQ'
];

var EP3BAHO3660OBTN4BETFOLD = [
'A8S', 'A7S','A5S', 'A4S'
];
var EP3BAHO3660OBTN4BETFOLD50 = [
'KQO',
'AJO'
];

///////61
var EP3BAHO61OBTNCALL = [
'AQS', 'AJS', 'ATS', 'A9S', 'AKS', 'A5S', 'A4S',
'AKO', 'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JJ', 'JTS',
'TT', 'T9S',
'99', '98S',
'88',
'87S', '77',
'66'
];

var EP3BAHO61OBTNFOLD = [
'A6S', 'A3S', 'A2S', 'KQO',
'AJO',
'KJO', 'J9S',
'ATO',
    '76S',
'66',
'55',
'44',
'33',
'22'
];

var EP3BAHO61OBTNRAISE = [
'AA', 'KK'
];

var EP3BAHO61OBTNRAISE50= [
'QQ'
];

var EP3BAHO61OBTN4BETFOLD = [
'A8S', 'A7S','A6S', 'A3S', 'A2S',
];

//////////////////////
////////////////////3BET EP VS SB
/////////////////////
////////////////////
var EP3BAHO1620OSBCALL = [
'AJS', 'ATS',
'KQS', 'KJS',
'AQO', 'QJS',
'JTS',
'T9S',
'98S'
];

var EP3BAHO1620OSBFOLD = [
'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS',
'KQO', 'QTS',
'AJO', 'KJO',
'ATO',
'88',
'77'
];

var EP3BAHO1620OSBSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT',
'99'
];


//////21-25
var EP3BAHO2125OSBCALL = [
'AJS', 
'ATS',
'KQS', 'KJS',
'QJS',
'JTS',
'T9S',
'98S',
];

var EP3BAHO2125OSBSHOVECALL50 = [
'AQO',
'99'
];

var EP3BAHO2125OSBFOLD = [
'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS',
'KQO', 'QTS',
'AJO', 'KJO',
'ATO',
'J9S',
'87S',
'76S'
];

var EP3BAHO2125OSBSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT'
];

var EP3BAHO2125OSBSHOVE50 = [
'88',
'77'
];


////26-35BB
var EP3BAHO2635OSBCALL = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JTS', 'J9S',
'T9S',
'98S'
];
var EP3BAHO2635OSBCALLFOLD50 = [
'87S'
];

var EP3BAHO2635OSBFOLD = [
'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQO',
'AJO', 'KJO',
'ATO',
'76S'
];

var EP3BAHO2635OSBSHOVE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ',
'JJ',
'TT'
];

var EP3BAHO2635OSBSHOVE50 = [
'77'
];

var EP3BAHO2635OSBSHOVECALL50 = [
'99',
'88'
];


////36-50BB
var EP3BAHO3660OSBCALL = [
'AQS', 'AJS', 'ATS', 'A9S', 'A8S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JJ', 'JTS', 'J9S',
'TT', 'T9S',
'99', '98S',
'88','87S',
'77',
];

var EP3BAHO3660OSBFOLD = [
'A3S', 'A2S',
'KJO',
'ATO',
'76S',
'66'
];

var EP3BAHO3660OSBRAISE = [
'AA', 'KK'
];

var EP3BAHO3660OSBRAISE50= [
'AKS',
'AKO',
'QQ'
];

var EP3BAHO3660OSB4BETFOLD = [
'A6S', 'A7S', 'A4S'
];
var EP3BAHO3660OSB4BETFOLD50 = [
'KQO',
'AJO'
];

var EP3BAHO3660OSB4BETFOLDCALL50 = [
'A5S'
];

///////61
var EP3BAHO61OSBCALL = [
'AQS', 'AJS', 'ATS', 'A9S', 'A8S', 'AKS', 'A5S', 'A4S',
'AKO', 'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JJ', 'JTS', 'J9S',
'TT', 'T9S',
'99', '98S',
'88',
'87S', '77',
'76S', '66',
'55',
];

var EP3BAHO61OSBFOLD = [
'A6S', 'A3S', 'A2S', 'KQO',
'AJO',
'KJO',
'ATO',
'44',
'33',
'22'
];

var EP3BAHO61OSBRAISE = [
'AA', 'KK'
];

var EP3BAHO61OSBRAISE50= [
'QQ'
];

var EP3BAHO61OSB4BETFOLD = [
'A7S','A6S', 'A3S', 'A2S',
];

//////////////////////
////////////////////3BET EP VS BB
/////////////////////
////////////////////
var EP3BAHO1620OBBCALL = [
'AJS',
'KQS',
'QJS',
'JTS',
];


var EP3BAHO1620OBBFOLD = [
'ATS','A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS',
'KTS',
'AQO','KQO', 'QTS',
'AJO', 'KJO',
'ATO',
'J9S',
'T9S',
'98S',
'87S', '88',
'76S', '77',

];

var EP3BAHO1620OBBSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT'
];

var EP3BAHO1620OBBSHOVE50 = [
'99'
];


//////21-25
var EP3BAHO2125OBBCALL = [
'AJS',
'KQS',
'QJS',
'JTS',
];


var EP3BAHO2125OBBFOLD = [
'ATS','A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS',
'KTS',
'AQO','KQO', 'QTS',
'AJO', 'KJO',
'ATO',
'J9S',
'T9S',
'98S',
'87S', '88',
'76S', '77',

];

var EP3BAHO2125OBBSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT'
];

var EP3BAHO2125OBBSHOVE50 = [
'99'
];


////26-35BB
var EP3BAHO2635OBBCALL = [
'AQS', 'AJS',
'KQS',
'AQO', 'QJS',
'JTS',
'T9S',
];
var EP3BAHO2635OBBCALLFOLD50 = [
'ATS', '99', 'AQO'
];

var EP3BAHO2635OBBFOLD = [
'A9S','A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJS', 'KTS',  'QTS',
'KQO','87S', '98S',
'AJO', 'KJO', 'J9S',
'ATO',
'76S',
'77',
'88'
];

var EP3BAHO2635OBBSHOVE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ',
'JJ',
'TT'
];


////36-60BB
var EP3BAHO3660OBBCALL = [
'AKS', 'AKO','AQS', 'AJS', 'ATS', 'A9S', 'A8S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS',
'JJ', 'JTS',
'TT', 'T9S',
'99',
'88',
'77',
];

var EP3BAHO3660OBBFOLD = [
'AJO', 'KQO',
'KJO', 'QTS',
'J9S',
'ATO',
'87S',
'76S',
'66'
];

var EP3BAHO3660OBBRAISE = [
'AA', 'KK'
];

var EP3BAHO3660OBBRAISE50= [
'QQ'
];

var EP3BAHO3660OBB4BETFOLD25 = [
'A6S', 'A3S', 'A2S', 
];
var EP3BAHO3660OBB4BETFOLD50 = [
'A8S', 'A7S', 'A5S', 'A4S'
];

var EP3BAHO3660OBBCALLFOLD50 = [
'98S'
];

///////61
var EP3BAHO61OBBCALL = [
'AQS', 'AJS', 'ATS', 'A9S', 'AKS',
'AKO', 'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JJ', 'JTS',
'TT', 'T9S',
'99', '98S',
'88',
'87S', '77',
'66',
'55',
];

var EP3BAHO61OBBFOLD = [
'A3S', 'A2S', 'KQO', 'K9S',
'AJO',
'KJO',
'ATO',
'76S',
'44',
'33',
'22'
];

var EP3BAHO61OBBRAISE = [
'AA', 'KK'
];

var EP3BAHO61OBBRAISE50= [
'QQ'
];

var EP3BAHO61OBB4BETFOLD50 = [
'A7S','A4S',
];
var EP3BAHO61OBB4BETFOLD25 = [
'A3S', 'A2S', 'A6S', 
];
var EP3BAHO61OBB4BETFOLDCALL50 = [
 'A5S', 'A8S'
];

//////////////////////
////////////////////3BET MP VS MP
/////////////////////
////////////////////
var MP3BAHO1620OMPCALL = [
'AJS', 
'KQS', 'KJS',
'AQO', 'QJS',
'JTS',
'T9S'
];

var MP3BAHO1620OMPFOLD = [
'ATS', 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS',
'KQO', 'QTS',
'AJO', 'KJO',
'ATO',
'98S',
'88',
'77'
];

var MP3BAHO1620OMPSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT',
'99'
];


//////21-25
var MP3BAHO2125OMPCALL = [
'AJS', 
'ATS',
'KQS', 'KJS',
'QJS',
'JTS',
'T9S'
];

var MP3BAHO2125OMPSHOVECALL50 = [
'AQO'
];

var MP3BAHO2125OMPFOLD = [
'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS',
'KQO', 'QTS',
'AJO', 'KJO',
'ATO',
'J9S',
'98S',
'87S',
'76S'
];

var MP3BAHO2125OMPSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT'
];

var MP3BAHO2125OMPSHOVE50 = [
'99',
'88',
'77'
];


////26-35BB
var MP3BAHO2635OMPCALL = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JTS',
'T9S',
'98S'
];

var MP3BAHO2635OMPFOLD = [
'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQO',
'AJO', 'KJO', 'J9S',
'ATO',
'87S',
'76S'
];

var MP3BAHO2635OMPSHOVE = [
'AA', 'AKS',
'AKO', 'KK',
'QQ',
'JJ',
'TT'
];

var MP3BAHO2635OMPSHOVE50 = [
'77'
];

var MP3BAHO2635OMPSHOVECALL50 = [
'99',
'88'
];


////36-50BB
var MP3BAHO3660OMPCALL = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JJ', 'JTS',
'TT', 'T9S',
'99', '98S',
'88','87S'
];

var MP3BAHO3660OMPFOLD = [
'A6S', 'A3S', 'A2S',
'KJO', 'J9S',
'ATO',
'77', '76S',
'66'
];

var MP3BAHO3660OMPRAISE = [
'AA', 'KK'
];

var MP3BAHO3660OMPRAISE50= [
'AKS',
'AKO',
'QQ'
];

var MP3BAHO3660OMP4BETFOLD = [
'A8S', 'A7S','A5S', 'A4S'
];
var MP3BAHO3660OMP4BETFOLD50 = [
'KQO',
'AJO'
];

///////61
var MP3BAHO61OMPCALL = [
'AQS', 'AJS', 'ATS', 'A9S', 'AKS', 'A5S', 'A4S',
'AKO', 'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JJ', 'JTS',
'TT', 'T9S',
'99', '98S',
'88',
'87S', '77',
'66'
];

var MP3BAHO61OMPFOLD = [
'A6S', 'A3S', 'A2S', 'KQO',
'AJO',
'KJO', 'J9S',
'ATO',
'76S',
'66',
'55',
'44',
'33',
'22'
];

var MP3BAHO61OMPRAISE = [
'AA', 'KK'
];

var MP3BAHO61OMPRAISE50= [
'QQ'
];

var MP3BAHO61OMP4BETFOLD = [
'A8S', 'A7S','A6S', 'A3S', 'A2S',
];


//////////////////////
////////////////////3BET MP VS CO
/////////////////////
////////////////////
var MP3BAHO1620OCOCALL = [
'ATS', 'A9S', 
'KQS', 'KJS', 'KTS',
'QJS', 'QTS',
'JTS',
'T9S'
];

var MP3BAHO1620OCOFOLD = [
'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS', 'K9S', 'K8S',
'QTS', 'Q9S', 'QJO', 'KTO', 'QTO', 'JTO', 'A9O',
'AJO', 'KJO', 'J9S',
'ATO',
'98S', 'T8S','87S','76S'
];

var MP3BAHO1620OCOSHOVE = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK',
'AQO', 'QQ',
'JJ',
'TT',
'99',
'88'
];
var MP3BAHO1620OCOSHOVE50 = [
'77', '66', '55'
];
var MP3BAHO1620OCOCALLFOLD50 = [
'KQO'
];
//////21-25
var MP3BAHO2125OCOCALL = [
'A9S', 'A8S',
'KJS', 'KTS',
'QJS','QTS',
'JTS',
'T9S',
'98S',
'87S'
];

var MP3BAHO2125OCOSHOVECALL50 = [
'ATS', 'KQS'
];

var MP3BAHO2125OCOFOLD = [
'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQO', 'K9S', 'K8S','Q9S',
'KJO',
'QJO',
'ATO', 'T8S',
'J9S',
'76S'
];

var MP3BAHO2125OCOSHOVE = [
'AA', 'AKS', 'AQS', 'AQO',
'AKO', 'KK',
'AJS', 'QQ',
'JJ',
'TT',
'99',
'88',
'77',
'66'
];

var MP3BAHO2125OCOSHOVE50 = [
'AJO'
];
var MP3BAHO2125OCOSHOVE25 = [
'A5S', 'A4S'
];


////26-35BB
var MP3BAHO2635OCOCALL = [
'ATS', 'A9S', 'A8S',
'KQS', 'KJS', 'KTS',
'QJS', 'QTS',
'JTS', '87S',
'T9S',
'98S'
];
var MP3BAHO2635OCOCALLFOLD50 = [
'K9S', 'KQO', 'AJO'
];

var MP3BAHO2635OCOFOLD = [
'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJO', 'QJO', 'J9S', 'Q9S', 'K8S',
'ATO',
'76S',
'T8S'
];

var MP3BAHO2635OCOSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT',
'99'
];

var MP3BAHO2635OCOSHOVE50 = [
'66'
];
var MP3BAHO2635OCOSHOVE25 = [
'A5S', 'A4S'
];
var MP3BAHO2635OCOSHOVE75 = [
'77'
];

var MP3BAHO2635OCOSHOVECALL50 = [
'KQS'
];

var MP3BAHO2635OCOSHOVECALL75 = [
'AJS', 'AQO',
'88'
];


////36-60BB
var MP3BAHO3660OCOCALL = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
 'JTS',
'TT', 'T9S',
'99', '98S',
'88','87S','76S',
'77'
];
var MP3BAHO3660OCOCALLFOLD50 = [
'K9S', 'AJO', 'KQO', 'J9S'
];

var MP3BAHO3660OCOFOLD = [
'Q9S', 'QJO',
'KJO',
'T8S', 'K8S',
'66' ,'54S',
'55', '65S'
];

var MP3BAHO3660OCORAISE = [
'AA', 'KK', 'QQ',
];

var MP3BAHO3660OCORAISE50= [
'AKS',
'AKO',
'JJ'
];

var MP3BAHO3660OCO4BETFOLD = [
'A8S', 'A5S', 'A4S', 'A3S', 'A2S'
];
var MP3BAHO3660OCO4BETFOLD50 = [
'ATO', 'KJO', 'A7S', 'A6S'
];

///////61
var MP3BAHO61OCOCALL = [
'AQS', 'AJS', 'ATS', 'A9S', 'AKS', 'A5S', 'A4S',
'AKO', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'QJS', 'QTS',
'JJ', 'JTS', 'J9S',
'TT', 'T9S',
'99', '98S',
'88', '76S',
'87S', '77',
'66',
'55',
'44',
'33',
'22'
];
var MP3BAHO61OCOCALLFOLD50 = [
'AJO', 'KQO'
];

var MP3BAHO61OCOFOLD = [
'A6S', 'A3S', 'A2S', 'K8S', 'Q9S',
'QJO', 'T8S', '97S', '86S',
'KJO',
'ATO', '54S','65S'
];

var MP3BAHO61OCORAISE = [
'AA', 'KK'
];

var MP3BAHO61OCORAISE50= [
'QQ'
];

var MP3BAHO61OCO4BETFOLD = [
'A8S', 'A7S','A6S', 'A3S', 'A2S',
];

//////////////////////
////////////////////3BET MP VS BTN
/////////////////////
////////////////////
var MP3BAHO1620OBTNCALL = [
'ATS', 'A9S', 
'KQS', 'KJS', 'KTS',
'QJS', 'QTS',
'JTS',
'T9S'
];

var MP3BAHO1620OBTNFOLD = [
'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS', 'K9S', 'K8S',
'QTS', 'Q9S', 'QJO', 'KTO', 'QTO', 'JTO', 'A9O',
'AJO', 'KJO', 'J9S',
'ATO',
'98S', 'T8S','87S','76S'
];

var MP3BAHO1620OBTNSHOVE = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK',
'AQO', 'QQ',
'JJ',
'TT',
'99',
'88'
];
var MP3BAHO1620OBTNSHOVE50 = [
'77', '66', '55'
];
var MP3BAHO1620OBTNCALLFOLD50 = [
'KQO'
];
//////21-25
var MP3BAHO2125OBTNCALL = [
'A9S', 'A8S',
'KJS', 'KTS',
'QJS','QTS',
'JTS',
'T9S',
'98S',
'87S'
];

var MP3BAHO2125OBTNSHOVECALL50 = [
'ATS', 'KQS'
];

var MP3BAHO2125OBTNFOLD = [
'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQO', 'K9S', 'K8S','Q9S',
'KJO',
'QJO',
'ATO', 'T8S',
'J9S',
'76S'
];

var MP3BAHO2125OBTNSHOVE = [
'AA', 'AKS', 'AQS', 'AQO',
'AKO', 'KK',
'AJS', 'QQ',
'JJ',
'TT',
'99',
'88',
'77',
'66'
];

var MP3BAHO2125OBTNSHOVE50 = [
'AJO'
];
var MP3BAHO2125OBTNSHOVE25 = [
'A5S', 'A4S'
];


////26-35BB
var MP3BAHO2635OBTNCALL = [
'ATS', 'A9S', 'A8S',
'KQS', 'KJS', 'KTS',
'QJS', 'QTS',
'JTS', '87S',
'T9S',
'98S'
];
var MP3BAHO2635OBTNCALLFOLD50 = [
'K9S', 'KQO', 'AJO'
];

var MP3BAHO2635OBTNFOLD = [
'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJO', 'QJO', 'J9S', 'Q9S', 'K8S',
'ATO',
'76S',
'T8S'
];

var MP3BAHO2635OBTNSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT',
'99'
];

var MP3BAHO2635OBTNSHOVE50 = [
'66'
];
var MP3BAHO2635OBTNSHOVE25 = [
'A5S', 'A4S'
];
var MP3BAHO2635OBTNSHOVE75 = [
'77'
];

var MP3BAHO2635OBTNSHOVECALL50 = [
'KQS'
];

var MP3BAHO2635OBTNSHOVECALL75 = [
'AJS', 'AQO',
'88'
];


////36-60BB
var MP3BAHO3660OBTNCALL = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
    'JTS',
'TT', 'T9S',
'99', '98S',
'88','87S','76S',
'77'
];
var MP3BAHO3660OBTNCALLFOLD50 = [
'K9S', 'AJO', 'KQO', 'J9S'
];

var MP3BAHO3660OBTNFOLD = [
'Q9S', 'QJO',
'KJO',
'T8S', 'K8S',
'66' ,'54S',
'55', '65S'
];

var MP3BAHO3660OBTNRAISE = [
'AA', 'KK', 'QQ',
];

var MP3BAHO3660OBTNRAISE50= [
'AKS',
'AKO',
'JJ'
];

var MP3BAHO3660OBTN4BETFOLD = [
'A8S', 'A5S', 'A4S', 'A3S', 'A2S'
];
var MP3BAHO3660OBTN4BETFOLD50 = [
'ATO', 'KJO', 'A7S', 'A6S'
];

///////61
var MP3BAHO61OBTNCALL = [
'AQS', 'AJS', 'ATS', 'A9S', 'AKS', 'A5S', 'A4S',
'AKO', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'QJS', 'QTS',
'JJ', 'JTS', 'J9S',
'TT', 'T9S',
'99', '98S',
'88', '76S',
'87S', '77',
'66',
'55',
'44',
'33',
'22'
];
var MP3BAHO61OBTNCALLFOLD50 = [
'AJO', 'KQO'
];

var MP3BAHO61OBTNFOLD = [
'A6S', 'A3S', 'A2S', 'K8S', 'Q9S',
'QJO', 'T8S', '97S', '86S',
'KJO',
'ATO', '54S','65S'
];

var MP3BAHO61OBTNRAISE = [
'AA', 'KK'
];

var MP3BAHO61OBTNRAISE50= [
'QQ'
];

var MP3BAHO61OBTN4BETFOLD = [
'A8S', 'A7S','A6S', 'A3S', 'A2S',
];


//////////////////////
////////////////////3BET MP VS SB
/////////////////////
////////////////////
var MP3BAHO1620OSBCALL = [
'ATS', 'A9S', '98S',
'KQS', 'KJS', 'KTS',
'QJS', 'QTS',
'JTS',
'T9S'
];

var MP3BAHO1620OSBFOLD = [
'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KTS', 'K9S', 'K8S',
'QTS', 'Q9S', 'QJO', 'KTO', 'QTO', 'JTO', 'A9O',
'AJO', 'KJO', 'J9S',
'ATO',
'T8S','87S','76S'
];

var MP3BAHO1620OSBSHOVE = [
'AA', 'AKS', 'AQS', 'AJS',
'AKO', 'KK',
'AQO', 'QQ',
'JJ',
'TT',
'99',
'88'
];
var MP3BAHO1620OSBSHOVE50 = [
'77', '66', '55'
];
var MP3BAHO1620OSBCALLFOLD50 = [
'KQO'
];
//////21-25
var MP3BAHO2125OSBCALL = [
'A9S', 'A8S', 'ATS', 'KQS',
'KJS', 'KTS',
'QJS','QTS',
'JTS',
'T9S',
'98S',
'87S'
];

var MP3BAHO2125OSBFOLD = [
'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQO', 'K9S', 'K8S','Q9S',
'KJO',
'QJO',
'ATO', 'T8S',
'J9S',
'76S'
];

var MP3BAHO2125OSBSHOVE = [
'AA', 'AKS', 'AQS', 'AQO',
'AKO', 'KK',
'AJS', 'QQ',
'JJ',
'TT',
'99',
'88',
'77',
'66'
];

var MP3BAHO2125OSBSHOVE50 = [
'AJO'
];
var MP3BAHO2125OSBSHOVE25 = [
'A5S', 'A4S'
];


////26-35BB
var MP3BAHO2635OSBCALL = [
'ATS', 'A9S', 'A8S',
'KQS', 'KJS', 'KTS',
'QJS', 'QTS', 'K9S',
'JTS', '87S',
'T9S',
'98S'
];
var MP3BAHO2635OSBCALLFOLD50 = [
'KQO', 'AJO'
];

var MP3BAHO2635OSBFOLD = [
'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KJO', 'QJO', 'J9S', 'Q9S', 'K8S',
'ATO',
'76S',
'T8S'
];

var MP3BAHO2635OSBSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ', 'AJS', 'AQO',
'88',
'JJ',
'TT',
'99',
'77'
];

var MP3BAHO2635OSBSHOVE50 = [
'66'
];
var MP3BAHO2635OSBSHOVE25 = [
'A5S', 'A4S'
];

var MP3BAHO2635OSBSHOVECALL50 = [
'KQS'
];



////36-60BB
var MP3BAHO3660OSBCALL = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS',
'AQO', 'QJS', 'QTS',
'JTS', 'K9S',
'TT', 'T9S',
'99', '98S',
'88','87S',
'77',
'66' ,
'55', 
];
var MP3BAHO3660OSBCALLFOLD50 = [
'AJO', 'KQO', 'J9S'
];

var MP3BAHO3660OSBFOLD = [
'Q9S', 'QJO',
'KJO','ATO', 'KJO',
'T8S', 'K8S',
'54S',
'65S'
];

var MP3BAHO3660OSBRAISE = [
'AA', 'KK', 'QQ',
];

var MP3BAHO3660OSBRAISE50= [
'AKS',
'JJ'
];

var MP3BAHO3660OSBRAISE75= [
'AKO',
];
var MP3BAHO3660OSB4BETFOLD = [
'A8S','A7S', 'A6S', '76S',
];
var MP3BAHO3660OSB4BETFOLDCALL50 = [
'A5S', 'A4S', 'A3S', 'A2S', 'AJO', 'KQO',
];

///////61
var MP3BAHO61OSBCALL = [
'AQS', 'AJS', 'ATS', 'A9S', 'AKS', 'A5S', 'A4S', 'A3S', 'A2S',
'AKO', 'KQS', 'KJS', 'KTS', 'K9S',
'AQO', 'QJS', 'QTS', 'Q9S',
'JJ', 'JTS', 'J9S',
'TT', 'T9S',
'99', '98S',
'88', '76S', '65S',
'87S', '77',
'66',
'55',
'44',
'33',
'22'
];
var MP3BAHO61OSBCALLFOLD50 = [
'AJO', 'KQO'
];

var MP3BAHO61OSBFOLD = [
'K8S',
'QJO', 'T8S', '97S', '86S',
'KJO',
'ATO', '54S'
];

var MP3BAHO61OSBRAISE = [
'AA', 'KK'
];

var MP3BAHO61OSBRAISE50= [
'QQ'
];

var MP3BAHO61OSB4BETFOLD = [
'A7S','A6S',
];

var MP3BAHO61OSB4BETFOLDCALL50 = [
'A8S'
];
var MP3BAHO61OSB4BETFOLD50 = [
'K8S',
];

//////////////////////
////////////////////3BET MP VS BB
/////////////////////
////////////////////
// var MP3BAHO1620OBBCALL = [
// 'AJS', 'ATS',
// 'KQS', 'KJS',
// 'AQO', 'QJS',
// 'JTS',
// 'T9S',
// '98S'
// ];

// var MP3BAHO1620OBBFOLD = [
// 'A9S', 'A8S', 'A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
// 'KTS',
// 'KQO', 'QTS',
// 'AJO', 'KJO',
// 'ATO',
// '88',
// '77'
// ];

// var MP3BAHO1620OBBSHOVE = [
// 'AA', 'AKS', 'AQS',
// 'AKO', 'KK',
// 'QQ',
// 'JJ',
// 'TT',
// '99'
// ];


//////21-25
var MP3BAHO2125OBBCALL = [
'AJS', 'ATS', 'KQS', 
'KJS', 'AQO',
'QJS',
'JTS',
'T9S',
'98S'
];

var MP3BAHO2125OBBFOLD = [
'A9S', 'A8S','A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQO', 'K9S', 'K8S','Q9S', 'KTS', 'QTS',
'KJO', 'AJO',
'QJO', '87S',
'ATO', 'T8S',
'J9S',
'76S',
'77',
'66'
];

var MP3BAHO2125OBBSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT',
'99',
];

var MP3BAHO2125OBBSHOVE50 = [
'88'
];


////26-35BB
var MP3BAHO2635OBBCALL = [
'AJS', 'ATS', 'KQS', 'A9S',
'KJS', 'AQO',
'QJS',
'JTS',
'T9S',
'98S'
];

var MP3BAHO2635OBBFOLD = [
'A8S','A7S', 'A6S', 'A5S', 'A4S', 'A3S', 'A2S',
'KQO', 'K9S', 'K8S','Q9S', 'KTS', 'QTS',
'KJO', 'AJO',
'QJO', '87S',
'ATO', 'T8S',
'J9S',
'76S',
'77',
'66'
];

var MP3BAHO2635OBBSHOVE = [
'AA', 'AKS', 'AQS',
'AKO', 'KK',
'QQ',
'JJ',
'TT',
];

var MP3BAHO2635OBBSHOVE50 = [
'88'
];
var MP3BAHO2635OBBSHOVECALL50 = [
'99'
];

////36-60BB
var MP3BAHO3660OBBCALL = [
'AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS', 'JJ',
'AQO', 'QJS', 'QTS',
'JTS',
'TT', 'T9S',
'99', '98S',
'88','87S',
'77',
'66' ,
'55', 
];

var MP3BAHO3660OBBFOLD = [
'Q9S', 'QJO', 'AJO', 'KQO',  'K9S',
'KJO','ATO', 'KJO', 'A8S','A7S', 'A6S', '76S',
'T8S', 'K8S', 'J9S',
'54S',
'65S'
];

var MP3BAHO3660OBBRAISE = [
'AA', 'KK', 'QQ',
];

var MP3BAHO3660OBBRAISE50= [
'AKS'
];

var MP3BAHO3660OBBRAISE75= [
'AKO',
];

var MP3BAHO3660OBB4BETFOLDCALL50 = [
'A5S', 'A4S', 'A3S', 'A2S'
];

///////61
var MP3BAHO61OBBCALL = [
'AKS', 'AKO','AQS', 'AJS', 'ATS', 'A9S',
'KQS', 'KJS', 'KTS', 'JJ', '76S',
'AQO', 'QJS', 'QTS',
'JTS',
'TT', 'T9S',
'99', '98S',
'88','87S',
'77',
'66' ,
'55', 
];
var MP3BAHO61OBBCALLFOLD50 = [
'55',
'44',
'33',
'22'
];

var MP3BAHO61OBBFOLD = [
'Q9S', 'QJO', 'AJO', 'KQO', 'K9S',
'KJO','ATO', 'KJO', 'A8S',
'T8S', 'K8S', 'J9S', '86S', '97S', 'T8S',
'54S',
'65S'
];

var MP3BAHO61OBBRAISE = [
'AA', 'KK', 'QQ'
];

var MP3BAHO61OBBRAISE50= [
'AKS'
];

var MP3BAHO61OBB4BETFOLDCALL50 = [
'A8S', 'A5S', 'A4S', 'A3S', 'A2S'
];
var MP3BAHO61OBB4BETFOLD50 = [
'A7S', 'A6S'
];

var fullList = [{ "name": "EPNOB3", "color": "shove", "cards": EPNOB3 },
{ "name": "EPNOB4", "color": "shove", "cards": EPNOB4 },
{ "name": "EPNOB5", "color": "shove", "cards": EPNOB5 },
{ "name": "EPNOB6", "color": "shove", "cards": EPNOB6 },
{ "name": "EPNOB7", "color": "shove", "cards": EPNOB7 },
{ "name": "EPNOB8", "color": "shove", "cards": EPNOB8 },
{ "name": "EPNOB9", "color": "shove", "cards": EPNOB9 },
{ "name": "EPNOB10", "color": "shove", "cards": EPNOB10 },
{ "name": "EPNOB11", "color": "shove", "cards": EPNOB11 },
{ "name": "EPNOB12", "color": "shove", "cards": EPNOB12 },
{ "name": "EPNOB1315", "color": "lightraise", "cards": EPNOB1315RAISEFOLD },
{ "name": "EPNOB1315", "color": "raise", "cards": EPNOB1315RAISE },
{ "name": "EPNOB1315", "color": "shove", "cards": EPNOB1315SHOVE },
{ "name": "EPNOB1622", "color": "raise", "cards": EPNOB1622 },
{ "name": "EPNOB2330", "color": "raise", "cards": EPNOB2330 },
{ "name": "EPNOB3250", "color": "raise", "cards": EPNOB3250 },
{ "name": "EPNOB51100", "color": "raise", "cards": EPNOB51100 },
{ "name": "MPNOB3", "color": "shove", "cards": MPNOB3 },
{ "name": "MPNOB4", "color": "shove", "cards": MPNOB4 },
{ "name": "MPNOB5", "color": "shove", "cards": MPNOB5 },
{ "name": "MPNOB6", "color": "shove", "cards": MPNOB6 },
{ "name": "MPNOB7", "color": "shove", "cards": MPNOB7 },
{ "name": "MPNOB8", "color": "shove", "cards": MPNOB8 },
{ "name": "MPNOB9", "color": "shove", "cards": MPNOB9 },
{ "name": "MPNOB10", "color": "shove", "cards": MPNOB10 },
{ "name": "MPNOB11", "color": "shove", "cards": MPNOB11 },
{ "name": "MPNOB12", "color": "shove", "cards": MPNOB12 },
{ "name": "MPNOB1315", "color": "lightraise", "cards": MPNOB1315RAISEFOLD },
{ "name": "MPNOB1315", "color": "raise", "cards": MPNOB1315RAISE },
{ "name": "MPNOB1315", "color": "shove", "cards": MPNOB1315SHOVE },
{ "name": "MPNOB1622", "color": "raise", "cards": MPNOB1622 },
{ "name": "MPNOB1622", "color": "shove", "cards": MPNOB1622SHOVE },
{ "name": "MPNOB2330", "color": "raise", "cards": MPNOB2330 },
{ "name": "MPNOB3250", "color": "raise", "cards": MPNOB3250 },
{ "name": "MPNOB51100", "color": "raise", "cards": MPNOB51100 },
{ "name": "CONOB3", "color": "shove", "cards": CONOB3 },
{ "name": "CONOB4", "color": "shove", "cards": CONOB4 },
{ "name": "CONOB5", "color": "shove", "cards": CONOB5 },
{ "name": "CONOB6", "color": "shove", "cards": CONOB6 },
{ "name": "CONOB7", "color": "shove", "cards": CONOB7 },
{ "name": "CONOB8", "color": "shove", "cards": CONOB8 },
{ "name": "CONOB9", "color": "shove", "cards": CONOB9 },
{ "name": "CONOB10", "color": "shove", "cards": CONOB10 },
{ "name": "CONOB11", "color": "shove", "cards": CONOB11 },
{ "name": "CONOB12", "color": "shove", "cards": CONOB12 },
{ "name": "CONOB1315", "color": "lightraise", "cards": CONOB1315RAISEFOLD },
{ "name": "CONOB1315", "color": "raise", "cards": CONOB1315RAISE },
{ "name": "CONOB1315", "color": "shove", "cards": CONOB1315SHOVE },
{ "name": "CONOB1622", "color": "raise", "cards": CONOB1622RAISE },
{ "name": "CONOB1622", "color": "shove", "cards": CONOB1622SHOVE },
{ "name": "CONOB2330", "color": "raise", "cards": CONOB2330 },
{ "name": "CONOB3250", "color": "raise", "cards": CONOB3250 },
{ "name": "CONOB51100", "color": "raise", "cards": CONOB51100 },
{ "name": "BUNOB3", "color": "shove", "cards": BUNOB3 },
{ "name": "BUNOB4", "color": "shove", "cards": BUNOB4 },
{ "name": "BUNOB5", "color": "shove", "cards": BUNOB5 },
{ "name": "BUNOB6", "color": "shove", "cards": BUNOB6 },
{ "name": "BUNOB7", "color": "shove", "cards": BUNOB7 },
{ "name": "BUNOB8", "color": "shove", "cards": BUNOB8 },
{ "name": "BUNOB9", "color": "shove", "cards": BUNOB9 },
{ "name": "BUNOB10", "color": "shove", "cards": BUNOB10 },
{ "name": "BUNOB11", "color": "shove", "cards": BUNOB11 },
{ "name": "BUNOB12", "color": "shove", "cards": BUNOB12 },
{ "name": "BUNOB1315", "color": "lightraise", "cards": BUNOB1315RAISEFOLD },
{ "name": "BUNOB1315", "color": "raise", "cards": BUNOB1315RAISE },
{ "name": "BUNOB1315", "color": "shove", "cards": BUNOB1315SHOVE },
{ "name": "BUNOB1622", "color": "raise", "cards": BUNOB1622RAISE },
{ "name": "BUNOB1622", "color": "shove", "cards": BUNOB1622SHOVE },
{ "name": "BUNOB2330", "color": "raise", "cards": BUNOB2330RAISE },
{ "name": "BUNOB2330", "color": "shove", "cards": BUNOB2330SHOVE },
{ "name": "BUNOB3250", "color": "raise", "cards": BUNOB3250 },
{ "name": "BUNOB51100", "color": "raise", "cards": BUNOB51100 },
{ "name": "SBNOB3", "color": "shove", "cards": SBNOB3 },
{ "name": "SBNOB4", "color": "shove", "cards": SBNOB4 },
{ "name": "SBNOB5", "color": "shove", "cards": SBNOB5 },
{ "name": "SBNOB6", "color": "shove", "cards": SBNOB6 },
{ "name": "SBNOB7", "color": "shove", "cards": SBNOB7 },
{ "name": "SBNOB8", "color": "shove", "cards": SBNOB8 },
{ "name": "SBNOB9", "color": "shove", "cards": SBNOB9 },
{ "name": "SBNOB10", "color": "shove", "cards": SBNOB10 },
{ "name": "SBNOB11", "color": "shove", "cards": SBNOB11 },
{ "name": "SBNOB12", "color": "shove", "cards": SBNOB12 },
{ "name": "SBNOB1315", "color": "shove", "cards": SBNOB1315 },
{ "name": "SBNOB1622", "color": "raise", "cards": SBNOB1622RAISE },
{ "name": "SBNOB1622", "color": "shove", "cards": SBNOB1622SHOVE },
{ "name": "SBNOB1622", "color": "limpfold", "cards": SBNOB1622LIMPFOLD },
{ "name": "SBNOB1622", "color": "lightraise", "cards": SBNOB1622RAISEFOLD },
//
{ "name": "SBNOB2330", "color": "raise", "cards": SBNOB2330RAISE },
{ "name": "SBNOB2330", "color": "shove", "cards": SBNOB2330SHOVE },
{ "name": "SBNOB2330", "color": "raiseshove", "cards": SBNOB2330RAISESHOVE },
{ "name": "SBNOB2330", "color": "lightraise", "cards": SBNOB2330RAISEFOLD },
{ "name": "SBNOB2330", "color": "limp", "cards": SBNOB2330LIMP },
{ "name": "SBNOB2330", "color": "limpfold", "cards": SBNOB2330LIMPFOLD },

{ "name": "SBNOB3250", "color": "raise", "cards": SBNOB3250RAISE },
{ "name": "SBNOB3250", "color": "lightraise", "cards": SBNOB3250RAISEFOLD },
{ "name": "SBNOB3250", "color": "limp", "cards": SBNOB3250LIMP },
{ "name": "SBNOB3250", "color": "limpfold", "cards": SBNOB3250LIMPFOLD },
//
{ "name": "SBNOB51100", "color": "raise", "cards": SBNOB51100RAISE },
{ "name": "SBNOB51100", "color": "lightraise", "cards": SBNOB51100RAISEFOLD },
{ "name": "SBNOB51100", "color": "limp", "cards": SBNOB51100LIMP },
{ "name": "SBNOB51100", "color": "limpfold", "cards": SBNOB51100LIMPFOLD },

///OPEN BEFORE
//////////////
{ "name": "EPOB02OEP", "color": "shove", "cards": EPOB02OEP },
{ "name": "EPOB24OEP", "color": "shove", "cards": EPOB24OEP },
{ "name": "EPOB46OEP", "color": "shove", "cards": EPOB46OEP },
{ "name": "EPOB68OEP", "color": "shove", "cards": EPOB68OEP },
{ "name": "EPOB810OEP", "color": "shove", "cards": EPOB810OEP },
{ "name": "EPOB1012OEP", "color": "shove", "cards": EPOB1012OEP },
{ "name": "EPOB1215OEP", "color": "shove", "cards": EPOB1215OEP },
{ "name": "EPOB1520OEP", "color": "shove", "cards": EPOB1520OEP },
//
{ "name": "EPOB2025OEP", "color": "shove", "cards": EPOB2025OEP },
{ "name": "EPOB2025OEP", "color": "raise", "cards": EPOB2025OEPRAISE },
{ "name": "EPOB2025OEP", "color": "call", "cards": EPOB2025OEPCALL },
//
{ "name": "EPOB2535OEP", "color": "raisecall50", "cards": EPOB2535OEPRAISECALL50 },
{ "name": "EPOB2535OEP", "color": "raisecall75", "cards": EPOB2535OEPRAISECALL75 },
{ "name": "EPOB2535OEP", "color": "call", "cards": EPOB2535OEPCALL },
{ "name": "EPOB2535OEP", "color": "raise", "cards": EPOB2535OEPRAISE },
{ "name": "EPOB2535OEP", "color": "betfold50", "cards": EPOB2535OEP3BETFOLD50 },
{ "name": "EPOB2535OEP", "color": "betfold", "cards": EPOB2535OEP3BETFOLD },
//
{ "name": "EPOB3650OEP", "color": "raisecall50", "cards": EPOB3650OEPRAISECALL50 },
{ "name": "EPOB3650OEP", "color": "raisecall75", "cards": EPOB3650OEPRAISECALL75 },
{ "name": "EPOB3650OEP", "color": "call", "cards": EPOB3650OEPCALL },
{ "name": "EPOB3650OEP", "color": "betfold75", "cards": EPOB3650OEP3BETFOLD75 },
{ "name": "EPOB3650OEP", "color": "betfold", "cards": EPOB3650OEP3BETFOLD },

//
{ "name": "EPOB5180OEP", "color": "raisecall50", "cards": EPOB5180OEPRAISECALL50 },
{ "name": "EPOB5180OEP", "color": "raisecall25", "cards": EPOB5180OEPRAISECALL25 },
{ "name": "EPOB5180OEP", "color": "call", "cards": EPOB5180OEPCALL },
{ "name": "EPOB5180OEP", "color": "raise", "cards": EPOB5180OEPRAISE },
{ "name": "EPOB5180OEP", "color": "betfold", "cards": EPOB5180OEP3BETFOLD },
//

{ "name": "EPOB81OEP", "color": "raisecall50", "cards": EPOB81OEPRAISECALL50 },
{ "name": "EPOB81OEP", "color": "raisecall25", "cards": EPOB81OEPRAISECALL25 },
{ "name": "EPOB81OEP", "color": "call", "cards": EPOB81OEPCALL },
{ "name": "EPOB81OEP", "color": "raise", "cards": EPOB81OEPRAISE },
{ "name": "EPOB81OEP", "color": "betfold", "cards": EPOB81OEP3BETFOLD },

/////////////////MPOB
/////////////////
{ "name": "MPOB02OEP", "color": "shove", "cards": MPOB02OEP },
{ "name": "MPOB24OEP", "color": "shove", "cards": MPOB24OEP },
{ "name": "MPOB46OEP", "color": "shove", "cards": MPOB46OEP },
{ "name": "MPOB68OEP", "color": "shove", "cards": MPOB68OEP },
{ "name": "MPOB810OEP", "color": "shove", "cards": MPOB810OEP },
{ "name": "MPOB1012OEP", "color": "shove", "cards": MPOB1012OEP },
{ "name": "MPOB1215OEP", "color": "shove", "cards": MPOB1215OEP },
{ "name": "MPOB1520OEP", "color": "shove", "cards": MPOB1520OEP },
//
{ "name": "MPOB2025OEP", "color": "shove", "cards": MPOB2025OEP },
{ "name": "MPOB2025OEP", "color": "raise", "cards": MPOB2025OEPRAISE },
{ "name": "MPOB2025OEP", "color": "call", "cards": MPOB2025OEPCALL },
//
{ "name": "MPOB2535OEP", "color": "call", "cards": MPOB2535OEPCALL },
{ "name": "MPOB2535OEP", "color": "raise", "cards": MPOB2535OEPRAISE },
{ "name": "MPOB2535OEP", "color": "betfold50", "cards": MPOB2535OEP3BETFOLD50 },
{ "name": "MPOB2535OEP", "color": "betfold", "cards": MPOB2535OEP3BETFOLD },
//
{ "name": "MPOB3650OEP", "color": "raisecall50", "cards": MPOB3650OEPRAISECALL50 },
{ "name": "MPOB3650OEP", "color": "call", "cards": MPOB3650OEPCALL },
{ "name": "MPOB3650OEP", "color": "betfold75", "cards": MPOB3650OEP3BETFOLD75 },
{ "name": "MPOB3650OEP", "color": "betfold", "cards": MPOB3650OEP3BETFOLD },
{ "name": "MPOB3650OEP", "color": "raise", "cards": MPOB3650OEPRAISE },
{ "name": "MPOB3650OEP", "color": "call", "cards": MPOB3650OEPCALL },
{ "name": "MPOB3650OEP", "color": "betfold25", "cards": MPOB3650OEP3BETFOLD25 },
{ "name": "MPOB3650OEP", "color": "callfold50", "cards": MPOB3650OEPCALLFOLD50 },

//
{ "name": "MPOB5180OEP", "color": "raisecall50", "cards": MPOB5180OEPRAISECALL50 },
{ "name": "MPOB5180OEP", "color": "raise", "cards": MPOB5180OEPRAISE },
{ "name": "MPOB5180OEP", "color": "call", "cards": MPOB5180OEPCALL },
{ "name": "MPOB5180OEP", "color": "betfold50", "cards": MPOB5180OEP3BETFOLD50 },
{ "name": "MPOB5180OEP", "color": "betfold", "cards": MPOB5180OEP3BETFOLD },
{ "name": "MPOB5180OEP", "color": "callfold50", "cards": MPOB5180OEPCALLFOLD50},
//

{ "name": "MPOB81OEP", "color": "raisecall50", "cards": MPOB81OEPRAISECALL50 },
{ "name": "MPOB81OEP", "color": "call", "cards": MPOB81OEPCALL },
{ "name": "MPOB81OEP", "color": "raise", "cards": MPOB81OEPRAISE },
{ "name": "MPOB81OEP", "color": "betfold", "cards": MPOB81OEP3BETFOLD },
{ "name": "MPOB81OEP", "color": "betfold50", "cards": MPOB81OEP3BETFOLD50 },
{ "name": "MPOB81OEP", "color": "callfold50", "cards": MPOB81OEP3BETFOLDCALL50 },

////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
//MPVSMP
{ "name": "MPOB02OMP", "color": "shove", "cards": MPOB02OMP },
{ "name": "MPOB24OMP", "color": "shove", "cards": MPOB24OMP },
{ "name": "MPOB46OMP", "color": "shove", "cards": MPOB46OMP },
{ "name": "MPOB68OMP", "color": "shove", "cards": MPOB68OMP },
{ "name": "MPOB810OMP", "color": "shove", "cards": MPOB810OMP },
{ "name": "MPOB1012OMP", "color": "shove", "cards": MPOB1012OMP },
{ "name": "MPOB1215OMP", "color": "shove", "cards": MPOB1215OMP },
{ "name": "MPOB1520OMP", "color": "shove", "cards": MPOB1520OMP },
//
{ "name": "MPOB2025OMP", "color": "shove", "cards": MPOB2025OMP },
{ "name": "MPOB2025OMP", "color": "raise", "cards": MPOB2025OMPRAISE },
{ "name": "MPOB2025OMP", "color": "call", "cards": MPOB2025OMPCALL },
//
{ "name": "MPOB2535OMP", "color": "call", "cards": MPOB2535OMPCALL },
{ "name": "MPOB2535OMP", "color": "raise", "cards": MPOB2535OMPRAISE },
{ "name": "MPOB2535OMP", "color": "betfold50", "cards": MPOB2535OMP3BETFOLD50 },
{ "name": "MPOB2535OMP", "color": "betfold", "cards": MPOB2535OMP3BETFOLD },
//
{ "name": "MPOB3650OMP", "color": "raisecall50", "cards": MPOB3650OMPRAISECALL50 },
{ "name": "MPOB3650OMP", "color": "call", "cards": MPOB3650OMPCALL },
{ "name": "MPOB3650OMP", "color": "betfold75", "cards": MPOB3650OMP3BETFOLD75 },
{ "name": "MPOB3650OMP", "color": "betfold", "cards": MPOB3650OMP3BETFOLD },
{ "name": "MPOB3650OMP", "color": "raise", "cards": MPOB3650OMPRAISE },
{ "name": "MPOB3650OMP", "color": "call", "cards": MPOB3650OMPCALL },
{ "name": "MPOB3650OMP", "color": "betfold25", "cards": MPOB3650OMP3BETFOLD25 },
{ "name": "MPOB3650OMP", "color": "callfold50", "cards": MPOB3650OMP3BETFOLDCALL50 },

//
{ "name": "MPOB5180OMP", "color": "raisecall50", "cards": MPOB5180OMPRAISECALL50 },
{ "name": "MPOB5180OMP", "color": "raise", "cards": MPOB5180OMPRAISE },
{ "name": "MPOB5180OMP", "color": "call", "cards": MPOB5180OMPCALL },
{ "name": "MPOB5180OMP", "color": "callfold50", "cards": MPOB5180OMP3BETFOLDCALL50 },
{ "name": "MPOB5180OMP", "color": "betfold", "cards": MPOB5180OMP3BETFOLD },
{ "name": "MPOB5180OMP", "color": "betfold50", "cards": MPOB5180OMP3BETFOLD50 },
//

{ "name": "MPOB81OMP", "color": "raisecall50", "cards": MPOB81OMPRAISECALL50 },
{ "name": "MPOB81OMP", "color": "call", "cards": MPOB81OMPCALL },
{ "name": "MPOB81OMP", "color": "raise", "cards": MPOB81OMPRAISE },
{ "name": "MPOB81OMP", "color": "betfold", "cards": MPOB81OMP3BETFOLD },
{ "name": "MPOB81OMP", "color": "betfold50", "cards": MPOB81OMP3BETFOLD50 },
{ "name": "MPOB81OMP", "color": "callfold50", "cards": MPOB81OMP3BETFOLDCALL50 },

////////////////////////////////////
////////////////////////////////////
////////////////////////////////////

{ "name": "COOB02OEP", "color": "shove", "cards": COOB02OEP },
{ "name": "COOB02OEP", "color": "white", "cards": COOB02OEPWHITE },
{ "name": "COOB02OEP", "color": "orange", "cards": COOB02OEPSILVER },
{ "name": "COOB24OEP", "color": "shove", "cards": COOB24OEP },
{ "name": "COOB46OEP", "color": "shove", "cards": COOB46OEP },
{ "name": "COOB68OEP", "color": "shove", "cards": COOB68OEP },
{ "name": "COOB810OEP", "color": "shove", "cards": COOB810OEP },
{ "name": "COOB1012OEP", "color": "shove", "cards": COOB1012OEP },
{ "name": "COOB1215OEP", "color": "shove", "cards": COOB1215OEP },
{ "name": "COOB1520OEP", "color": "shove", "cards": COOB1520OEP },
//
{ "name": "COOB2025OEP", "color": "shove", "cards": COOB2025OEP },
{ "name": "COOB2025OEP", "color": "raise", "cards": COOB2025OEPRAISE },
{ "name": "COOB2025OEP", "color": "call", "cards": COOB2025OEPCALL },
//
{ "name": "COOB2535OEP", "color": "call", "cards": COOB2535OEPCALL },
{ "name": "COOB2535OEP", "color": "raise", "cards": COOB2535OEPRAISE },
{ "name": "COOB2535OEP", "color": "betfold50", "cards": COOB2535OEP3BETFOLD50 },
{ "name": "COOB2535OEP", "color": "betfold", "cards": COOB2535OEP3BETFOLD },
//
{ "name": "COOB3650OEP", "color": "raisecall50", "cards": COOB3650OEPRAISECALL50 },
{ "name": "COOB3650OEP", "color": "call", "cards": COOB3650OEPCALL },
{ "name": "COOB3650OEP", "color": "betfold75", "cards": COOB3650OEP3BETFOLD75 },
{ "name": "COOB3650OEP", "color": "betfold", "cards": COOB3650OEP3BETFOLD },
{ "name": "COOB3650OEP", "color": "raise", "cards": COOB3650OEPRAISE },
{ "name": "COOB3650OEP", "color": "call", "cards": COOB3650OEPCALL },
{ "name": "COOB3650OEP", "color": "betfold25", "cards": COOB3650OEP3BETFOLD25 },
{ "name": "COOB3650OEP", "color": "betfoldcall25", "cards": COOB3650OEP3BETFOLDCALL25 },

//
{ "name": "COOB5180OEP", "color": "raisecall50", "cards": COOB5180OEPRAISECALL50 },
{ "name": "COOB5180OEP", "color": "raise", "cards": COOB5180OEPRAISE },
{ "name": "COOB5180OEP", "color": "call", "cards": COOB5180OEPCALL },
{ "name": "COOB5180OEP", "color": "callfold50", "cards": COOB5180OEP3BETFOLDCALL50 },
{ "name": "COOB5180OEP", "color": "betfold", "cards": COOB5180OEP3BETFOLD },
{ "name": "COOB5180OEP", "color": "betfold50", "cards": COOB5180OEP3BETFOLD50 },
//

{ "name": "COOB81OEP", "color": "raisecall50", "cards": COOB81OEPRAISECALL50 },
{ "name": "COOB81OEP", "color": "call", "cards": COOB81OEPCALL },
{ "name": "COOB81OEP", "color": "raise", "cards": COOB81OEPRAISE },
{ "name": "COOB81OEP", "color": "betfold", "cards": COOB81OEP3BETFOLD },
{ "name": "COOB81OEP", "color": "betfold50", "cards": COOB81OEP3BETFOLD50 },
{ "name": "COOB81OEP", "color": "callfold50", "cards": COOB81OEP3BETFOLDCALL50 },
/////////////////////////////////////////
///////////////////////////////////////
//////////////////////////////////////////////////
////////////////////////////////////////////////
{ "name": "COOB02OMP", "color": "shove", "cards": COOB02OMP },
{ "name": "COOB02OMP", "color": "white", "cards": COOB02OMPWHITE },
{ "name": "COOB02OMP", "color": "orange", "cards": COOB02OMPSILVER },
{ "name": "COOB24OMP", "color": "shove", "cards": COOB24OMP },
{ "name": "COOB46OMP", "color": "shove", "cards": COOB46OMP },
{ "name": "COOB68OMP", "color": "shove", "cards": COOB68OMP },
{ "name": "COOB810OMP", "color": "shove", "cards": COOB810OMP },
{ "name": "COOB1012OMP", "color": "shove", "cards": COOB1012OMP },
{ "name": "COOB1215OMP", "color": "shove", "cards": COOB1215OMP },
{ "name": "COOB1520OMP", "color": "shove", "cards": COOB1520OMP },
//
{ "name": "COOB2025OMP", "color": "shove", "cards": COOB2025OMP },
{ "name": "COOB2025OMP", "color": "raise", "cards": COOB2025OMPRAISE },
{ "name": "COOB2025OMP", "color": "call", "cards": COOB2025OMPCALL },
//
{ "name": "COOB2535OMP", "color": "call", "cards": COOB2535OMPCALL },
{ "name": "COOB2535OMP", "color": "raise", "cards": COOB2535OMPRAISE },
{ "name": "COOB2535OMP", "color": "betfold50", "cards": COOB2535OMP3BETFOLD50 },
{ "name": "COOB2535OMP", "color": "betfold", "cards": COOB2535OMP3BETFOLD },
{ "name": "COOB2535OMP", "color": "callfold50", "cards": COOB2535OMP3BETCALL50 },
{ "name": "COOB2535OMP", "color": "callfold75", "cards": COOB2535OMP3BETCALL25 },

//
{ "name": "COOB3650OMP", "color": "raisecall50", "cards": COOB3650OMPRAISECALL50 },
{ "name": "COOB3650OMP", "color": "call", "cards": COOB3650OMPCALL },
{ "name": "COOB3650OMP", "color": "betfold50", "cards": COOB3650OMP3BETFOLD50 },
{ "name": "COOB3650OMP", "color": "betfold", "cards": COOB3650OMP3BETFOLD },
{ "name": "COOB3650OMP", "color": "raise", "cards": COOB3650OMPRAISE },
{ "name": "COOB3650OMP", "color": "raisecall25", "cards": COOB3650OMPRAISECALL25 },
{ "name": "COOB3650OMP", "color": "call", "cards": COOB3650OMPCALL },

//
{ "name": "COOB5180OMP", "color": "raisecall50", "cards": COOB5180OMPRAISECALL50 },
{ "name": "COOB5180OMP", "color": "raise", "cards": COOB5180OMPRAISE },
{ "name": "COOB5180OMP", "color": "call", "cards": COOB5180OMPCALL },
{ "name": "COOB5180OMP", "color": "betfoldcall50", "cards": COOB5180OMP3BETFOLDCALL50 },
{ "name": "COOB5180OMP", "color": "betfold", "cards": COOB5180OMP3BETFOLD },
{ "name": "COOB5180OMP", "color": "betfold50", "cards": COOB5180OMP3BETFOLD50 },
//

{ "name": "COOB81OMP", "color": "raisecall50", "cards": COOB81OMPRAISECALL50 },
{ "name": "COOB81OMP", "color": "call", "cards": COOB81OMPCALL },
{ "name": "COOB81OMP", "color": "raise", "cards": COOB81OMPRAISE },
{ "name": "COOB81OMP", "color": "betfold50", "cards": COOB81OMP3BETFOLD50 },
{ "name": "COOB81OMP", "color": "betfold", "cards": COOB81OMP3BETFOLD },
{ "name": "COOB81OMP", "color": "raisecall75", "cards": COOB81OMPRAISECALL75},
{ "name": "COOB81OMP", "color": "callfold50", "cards": COOB81OMP3BETFOLDCALL50 },

////BUTTTON VS EP//////////////////////////
//
///
////////////////////////////////////
{ "name": "BUOB02OEP", "color": "shove", "cards": BUOB02OEP },
{ "name": "BUOB24OEP", "color": "shove", "cards": BUOB24OEP },
{ "name": "BUOB46OEP", "color": "shove", "cards": BUOB46OEP },
{ "name": "BUOB68OEP", "color": "shove", "cards": BUOB68OEP },
{ "name": "BUOB810OEP", "color": "shove", "cards": BUOB810OEP },
{ "name": "BUOB1012OEP", "color": "shove", "cards": BUOB1012OEP },
{ "name": "BUOB1215OEP", "color": "shove", "cards": BUOB1215OEP },
{ "name": "BUOB1520OEP", "color": "shove", "cards": BUOB1520OEP },
//
{ "name": "BUOB2025OEP", "color": "shove", "cards": BUOB2025OEP },
{ "name": "BUOB2025OEP", "color": "raise", "cards": BUOB2025OEPRAISE },
{ "name": "BUOB2025OEP", "color": "call", "cards": BUOB2025OEPCALL },
//
{ "name": "BUOB2535OEP", "color": "call", "cards": BUOB2535OEPCALL },
{ "name": "BUOB2535OEP", "color": "raise", "cards": BUOB2535OEPRAISE },
{ "name": "BUOB2535OEP", "color": "betfold", "cards": BUOB2535OEP3BETFOLD },

//
{ "name": "BUOB3650OEP", "color": "raisecall50", "cards": BUOB3650OEPRAISECALL50 },
{ "name": "BUOB3650OEP", "color": "call", "cards": BUOB3650OEPCALL },
{ "name": "BUOB3650OEP", "color": "betfold", "cards": BUOB3650OEP3BETFOLD },
{ "name": "BUOB3650OEP", "color": "raise", "cards": BUOB3650OEPRAISE },

//
{ "name": "BUOB5180OEP", "color": "raisecall50", "cards": BUOB5180OEPRAISECALL50 },
{ "name": "BUOB5180OEP", "color": "raisecall25", "cards": BUOB5180OEPRAISECALL25 },
{ "name": "BUOB5180OEP", "color": "raise", "cards": BUOB5180OEPRAISE },
{ "name": "BUOB5180OEP", "color": "call", "cards": BUOB5180OEPCALL },
{ "name": "BUOB5180OEP", "color": "betfoldcall50", "cards": BUOB5180OEP3BETFOLDCALL50 },
{ "name": "BUOB5180OEP", "color": "betfold", "cards": BUOB5180OEP3BETFOLD },
{ "name": "BUOB5180OEP", "color": "betfold50", "cards": BUOB5180OEP3BETFOLD50 },
//

{ "name": "BUOB81OEP", "color": "raisecall50", "cards": BUOB81OEPRAISECALL50 },
{ "name": "BUOB81OEP", "color": "call", "cards": BUOB81OEPCALL },
{ "name": "BUOB81OEP", "color": "raise", "cards": BUOB81OEPRAISE },
{ "name": "BUOB81OEP", "color": "betfold50", "cards": BUOB81OEP3BETFOLD50 },
{ "name": "BUOB81OEP", "color": "betfold", "cards": BUOB81OEP3BETFOLD },
{ "name": "BUOB81OEP", "color": "raisecall25", "cards": BUOB81OEPRAISECALL25 },
{ "name": "BUOB81OEP", "color": "betfoldcall50", "cards": BUOB81OEP3BETFOLDCALL50 },

//////////////////
////////////////////////////
///////////////////////BTN VS MP
{ "name": "BUOB02OMP", "color": "shove", "cards": BUOB02OMP },
{ "name": "BUOB24OMP", "color": "shove", "cards": BUOB24OMP },
{ "name": "BUOB46OMP", "color": "shove", "cards": BUOB46OMP },
{ "name": "BUOB68OMP", "color": "shove", "cards": BUOB68OMP },
{ "name": "BUOB810OMP", "color": "shove", "cards": BUOB810OMP },
{ "name": "BUOB1012OMP", "color": "shove", "cards": BUOB1012OMP },
{ "name": "BUOB1215OMP", "color": "shove", "cards": BUOB1215OMP },
{ "name": "BUOB1520OMP", "color": "shove", "cards": BUOB1520OMP },
//
{ "name": "BUOB2025OMP", "color": "shove", "cards": BUOB2025OMP },
{ "name": "BUOB2025OMP", "color": "raise", "cards": BUOB2025OMPRAISE },
{ "name": "BUOB2025OMP", "color": "call", "cards": BUOB2025OMPCALL },
//
{ "name": "BUOB2535OMP", "color": "call", "cards": BUOB2535OMPCALL },
{ "name": "BUOB2535OMP", "color": "raise", "cards": BUOB2535OMPRAISE },
{ "name": "BUOB2535OMP", "color": "betfold", "cards": BUOB2535OMP3BETFOLD },
{ "name": "BUOB2535OMP", "color": "betfoldcall50", "cards": BUOB2535OMP3BETFOLDCALL50 },

//
{ "name": "BUOB3650OMP", "color": "raisecall50", "cards": BUOB3650OMPRAISECALL50 },
{ "name": "BUOB3650OMP", "color": "call", "cards": BUOB3650OMPCALL },
{ "name": "BUOB3650OMP", "color": "betfold", "cards": BUOB3650OMP3BETFOLD },

{ "name": "BUOB3650OMP", "color": "raise", "cards": BUOB3650OMPRAISE },
{ "name": "BUOB3650OMP", "color": "callfold50", "cards": BUOB3650OMPCALLFOLD50 },


{ "name": "BUOB5180OMP", "color": "raisecall75", "cards": BUOB5180OMPRAISECALL75 },
{ "name": "BUOB5180OMP", "color": "raisecall25", "cards": BUOB5180OMPRAISECALL25 },
{ "name": "BUOB5180OMP", "color": "raise", "cards": BUOB5180OMPRAISE },
{ "name": "BUOB5180OMP", "color": "call", "cards": BUOB5180OMPCALL },
{ "name": "BUOB5180OMP", "color": "callfold50", "cards": BUOB5180OMPCALLFOLD50 },
{ "name": "BUOB5180OMP", "color": "betfold", "cards": BUOB5180OMP3BETFOLD },


{ "name": "BUOB81OMP", "color": "raisecall50", "cards": BUOB81OMPRAISECALL50 },
{ "name": "BUOB81OMP", "color": "call", "cards": BUOB81OMPCALL },
{ "name": "BUOB81OMP", "color": "raise", "cards": BUOB81OMPRAISE },
{ "name": "BUOB81OMP", "color": "betfold25", "cards": BUOB81OMP3BETFOLD25 },
{ "name": "BUOB81OMP", "color": "betfold", "cards": BUOB81OMP3BETFOLD },
{ "name": "BUOB81OMP", "color": "betfoldcall50", "cards": BUOB81OMP3BETFOLDCALL50 },
{ "name": "BUOB81OMP", "color": "betfoldcall25", "cards": BUOB81OMP3BETFOLDCALL25 },
{ "name": "BUOB81OMP", "color": "betfoldcall75", "cards": BUOB81OMP3BETFOLDCALL75 },
{ "name": "BUOB81OMP", "color": "callfold50", "cards": BUOB81OMPCALLFOLD50 },


//////////////////
////////////////////////////
///////////////////////BTN VS CO
{ "name": "BUOB02OCO", "color": "shove", "cards": BUOB02OCO },
{ "name": "BUOB24OCO", "color": "shove", "cards": BUOB24OCO },
{ "name": "BUOB46OCO", "color": "shove", "cards": BUOB46OCO },
{ "name": "BUOB68OCO", "color": "shove", "cards": BUOB68OCO },
{ "name": "BUOB810OCO", "color": "shove", "cards": BUOB810OCO },
{ "name": "BUOB1012OCO", "color": "shove", "cards": BUOB1012OCO },
{ "name": "BUOB1215OCO", "color": "shove", "cards": BUOB1215OCO },
{ "name": "BUOB1520OCO", "color": "shove", "cards": BUOB1520OCO },
//
{ "name": "BUOB2025OCO", "color": "shove", "cards": BUOB2025OCO },
{ "name": "BUOB2025OCO", "color": "raise", "cards": BUOB2025OCORAISE },
{ "name": "BUOB2025OCO", "color": "call", "cards": BUOB2025OCOCALL },
//
{ "name": "BUOB2535OCO", "color": "call", "cards": BUOB2535OCOCALL },
{ "name": "BUOB2535OCO", "color": "raise", "cards": BUOB2535OCORAISE },
{ "name": "BUOB2535OCO", "color": "betfold", "cards": BUOB2535OCO3BETFOLD },
{ "name": "BUOB2535OCO", "color": "betfoldcall50", "cards": BUOB2535OCO3BETFOLDCALL50 },
{ "name": "BUOB2535OCO", "color": "shove75", "cards": BUOB2535OCOSHOVE75 },
{ "name": "BUOB2535OCO", "color": "shove50", "cards": BUOB2535OCOSHOVE50 },
{ "name": "BUOB2535OCO", "color": "callfold50", "cards": BUOB2535OCOCALLFOLD50 },

//
{ "name": "BUOB3650OCO", "color": "call", "cards": BUOB3650OCOCALL },
{ "name": "BUOB3650OCO", "color": "betfold", "cards": BUOB3650OCO3BETFOLD },

{ "name": "BUOB3650OCO", "color": "raise", "cards": BUOB3650OCORAISE },
{ "name": "BUOB3650OCO", "color": "betfoldcall75", "cards": BUOB3650OCO3BETFOLDCALL75 },
{ "name": "BUOB3650OCO", "color": "betfoldcall50", "cards": BUOB3650OCO3BETFOLDCALL50 },


{ "name": "BUOB5180OCO", "color": "raisecall50", "cards": BUOB5180OCORAISECALL50 },
{ "name": "BUOB5180OCO", "color": "raise", "cards": BUOB5180OCORAISE },
{ "name": "BUOB5180OCO", "color": "call", "cards": BUOB5180OCOCALL },
{ "name": "BUOB5180OCO", "color": "callfold50", "cards": BUOB5180OCOCALLFOLD50 },
{ "name": "BUOB5180OCO", "color": "betfold", "cards": BUOB5180OCO3BETFOLD },
{ "name": "BUOB5180OCO", "color": "betfoldcall75", "cards": BUOB5180OCO3BETFOLDCALL75 },
{ "name": "BUOB5180OCO", "color": "betfoldcall25", "cards": BUOB5180OCO3BETFOLDCALL25 },
{ "name": "BUOB5180OCO", "color": "betfoldcall50", "cards": BUOB5180OCO3BETFOLDCALL50 },

{ "name": "BUOB81OCO", "color": "raisecall50", "cards": BUOB81OCORAISECALL50 },
{ "name": "BUOB81OCO", "color": "call", "cards": BUOB81OCOCALL },
{ "name": "BUOB81OCO", "color": "raise", "cards": BUOB81OCORAISE },
{ "name": "BUOB81OCO", "color": "raisecall75", "cards": BUOB81OCORAISECALL75 },
{ "name": "BUOB81OCO", "color": "betfold", "cards": BUOB81OCO3BETFOLD },
{ "name": "BUOB81OCO", "color": "betfoldcall50", "cards": BUOB81OCO3BETFOLDCALL50 },
{ "name": "BUOB81OCO", "color": "callfold50", "cards": BUOB81OCOCALLFOLD50 },

//////////////////
////////////////////////////
///////////////////////SB VS EP
{ "name": "SBOB02OEP", "color": "shove", "cards": SBOB02OEP },
{ "name": "SBOB24OEP", "color": "shove", "cards": SBOB24OEP },
{ "name": "SBOB46OEP", "color": "shove", "cards": SBOB46OEP },
{ "name": "SBOB68OEP", "color": "shove", "cards": SBOB68OEP },
{ "name": "SBOB810OEP", "color": "shove", "cards": SBOB810OEP },
{ "name": "SBOB1012OEP", "color": "shove", "cards": SBOB1012OEP },
{ "name": "SBOB1215OEP", "color": "shove", "cards": SBOB1215OEP },
{ "name": "SBOB1520OEP", "color": "shove", "cards": SBOB1520OEP },
//
{ "name": "SBOB2025OEP", "color": "shove", "cards": SBOB2025OEP },
{ "name": "SBOB2535OEP", "color": "call", "cards": SBOB2535OEPCALL },

{ "name": "SBOB2535OEP", "color": "raise", "cards": SBOB2535OEPRAISE },
{ "name": "SBOB2535OEP", "color": "raisecall25", "cards": SBOB2535OEPRAISECALL25 },

{ "name": "SBOB2535OEP", "color": "betfoldcall50", "cards": SBOB2535OEP3BETFOLDCALL50 },
{ "name": "SBOB2535OEP", "color": "betfold75", "cards": SBOB2535OEP3BETFOLD75 },
{ "name": "SBOB2535OEP", "color": "betfold50", "cards": SBOB2535OEP3BETFOLD50 },

//
{ "name": "SBOB3650OEP", "color": "call", "cards": SBOB3650OEPCALL },
{ "name": "SBOB3650OEP", "color": "betfold", "cards": SBOB3650OEP3BETFOLD },

{ "name": "SBOB3650OEP", "color": "raise", "cards": SBOB3650OEPRAISE },
{ "name": "SBOB3650OEP", "color": "betfold50", "cards": SBOB3650OEP3BETFOLD50 },

{ "name": "SBOB5180OEP", "color": "raise", "cards": SBOB5180OEPRAISE },
{ "name": "SBOB5180OEP", "color": "call", "cards": SBOB5180OEPCALL },
{ "name": "SBOB5180OEP", "color": "betfold50", "cards": SBOB5180OEP3BETFOLD50 },
{ "name": "SBOB5180OEP", "color": "betfold", "cards": SBOB5180OEP3BETFOLD },


{ "name": "SBOB81OEP", "color": "call", "cards": SBOB81OEPCALL },
{ "name": "SBOB81OEP", "color": "raise", "cards": SBOB81OEPRAISE },
{ "name": "SBOB81OEP", "color": "betfold", "cards": SBOB81OEP3BETFOLD },
{ "name": "SBOB81OEP", "color": "betfold50", "cards": SBOB81OEP3BETFOLD50 },
{ "name": "SBOB81OEP", "color": "callfold50", "cards": SBOB81OEPCALLFOLD50 },



//////////////////
////////////////////////////
///////////////////////SB VS MP
{ "name": "SBOB02OMP", "color": "shove", "cards": SBOB02OMP },
{ "name": "SBOB24OMP", "color": "shove", "cards": SBOB24OMP },
{ "name": "SBOB46OMP", "color": "shove", "cards": SBOB46OMP },
{ "name": "SBOB68OMP", "color": "shove", "cards": SBOB68OMP },
{ "name": "SBOB810OMP", "color": "shove", "cards": SBOB810OMP },
{ "name": "SBOB1012OMP", "color": "shove", "cards": SBOB1012OMP },
{ "name": "SBOB1215OMP", "color": "shove", "cards": SBOB1215OMP },
{ "name": "SBOB1520OMP", "color": "shove", "cards": SBOB1520OMP },
{ "name": "SBOB2025OMP", "color": "shove", "cards": SBOB2025OMP },
///////////
{ "name": "SBOB2535OMP", "color": "call", "cards": SBOB2535OMPCALL },

{ "name": "SBOB2535OMP", "color": "raise", "cards": SBOB2535OMPRAISE },
{ "name": "SBOB2535OMP", "color": "raisecall50", "cards": SBOB2535OMPRAISECALL50 },

{ "name": "SBOB2535OMP", "color": "betfoldcall25", "cards": SBOB2535OMP3BETFOLDCALL25 },
{ "name": "SBOB2535OMP", "color": "betfold", "cards": SBOB2535OMP3BETFOLD },
{ "name": "SBOB2535OMP", "color": "betfold50", "cards": SBOB2535OMP3BETFOLD50 },

//
{ "name": "SBOB3650OMP", "color": "call", "cards": SBOB3650OMPCALL },
{ "name": "SBOB3650OMP", "color": "betfold", "cards": SBOB3650OMP3BETFOLD },

{ "name": "SBOB3650OMP", "color": "raise", "cards": SBOB3650OMPRAISE },
{ "name": "SBOB3650OMP", "color": "raisecall50", "cards": SBOB3650OMPRAISECALL50 },
{ "name": "SBOB3650OMP", "color": "betfold50", "cards": SBOB3650OMP3BETFOLD50 },
{ "name": "SBOB3650OMP", "color": "callfold50", "cards": SBOB3650OMPCALLFOLD50 },

//////
{ "name": "SBOB5180OMP", "color": "call", "cards": SBOB5180OMPCALL },
{ "name": "SBOB5180OMP", "color": "callfold75", "cards": SBOB5180OMPCALLFOLD75 },
{ "name": "SBOB5180OMP", "color": "betfold75", "cards": SBOB5180OMP3BETFOLD75 },
{ "name": "SBOB5180OMP", "color": "betfold", "cards": SBOB5180OMP3BETFOLD },
{ "name": "SBOB5180OMP", "color": "raise", "cards": SBOB5180OMPRAISE },

/////
{ "name": "SBOB81OMP", "color": "call", "cards": SBOB81OMPCALL },
{ "name": "SBOB81OMP", "color": "raise", "cards": SBOB81OMPRAISE },
{ "name": "SBOB81OMP", "color": "betfold", "cards": SBOB81OMP3BETFOLD },
{ "name": "SBOB81OMP", "color": "betfold75", "cards": SBOB81OMP3BETFOLD75 },


//////////////////
////////////////////////////
///////////////////////SB VS CO
{ "name": "SBOB02OCO", "color": "shove", "cards": SBOB02OCO },
{ "name": "SBOB24OCO", "color": "shove", "cards": SBOB24OCO },
{ "name": "SBOB46OCO", "color": "shove", "cards": SBOB46OCO },
{ "name": "SBOB68OCO", "color": "shove", "cards": SBOB68OCO },
{ "name": "SBOB810OCO", "color": "shove", "cards": SBOB810OCO },
{ "name": "SBOB1012OCO", "color": "shove", "cards": SBOB1012OCO },
{ "name": "SBOB1215OCO", "color": "shove", "cards": SBOB1215OCO },
{ "name": "SBOB1520OCO", "color": "shove", "cards": SBOB1520OCO },
{ "name": "SBOB2025OCO", "color": "shove", "cards": SBOB2025OCO },
///////////
{ "name": "SBOB2535OCO", "color": "call", "cards": SBOB2535OCOCALL },

{ "name": "SBOB2535OCO", "color": "raise", "cards": SBOB2535OCORAISE },
{ "name": "SBOB2535OCO", "color": "raisecall50", "cards": SBOB2535OCORAISECALL50 },

{ "name": "SBOB2535OCO", "color": "callfold50", "cards": SBOB2535OCOCALLFOLD50 },
{ "name": "SBOB2535OCO", "color": "betfold", "cards": SBOB2535OCO3BETFOLD },
{ "name": "SBOB2535OCO", "color": "betfold50", "cards": SBOB2535OCO3BETFOLD50 },
{ "name": "SBOB2535OCO", "color": "shove50", "cards": SBOB2535OCOSHOVE50 },
{ "name": "SBOB2535OCO", "color": "shoveraise2550", "cards": SBOB2535OCOSHOVERAISE2550 },

//
{ "name": "SBOB3650OCO", "color": "call", "cards": SBOB3650OCOCALL },
{ "name": "SBOB3650OCO", "color": "betfold", "cards": SBOB3650OCO3BETFOLD },

{ "name": "SBOB3650OCO", "color": "raise", "cards": SBOB3650OCORAISE },
{ "name": "SBOB3650OCO", "color": "betfoldcall50", "cards": SBOB3650OCO3BETFOLDCALL50 },
{ "name": "SBOB3650OCO", "color": "betfold50", "cards": SBOB3650OCO3BETFOLD50 },
{ "name": "SBOB3650OCO", "color": "callfold50", "cards": SBOB3650OCOCALLFOLD50 },

//////
{ "name": "SBOB5180OCO", "color": "call", "cards": SBOB5180OCOCALL },
{ "name": "SBOB5180OCO", "color": "betfoldcall75", "cards": SBOB5180OCO3BETFOLDCALL75 },
{ "name": "SBOB5180OCO", "color": "betfoldcall25", "cards": SBOB5180OCO3BETFOLDCALL25 },
{ "name": "SBOB5180OCO", "color": "betfold50", "cards": SBOB5180OCO3BETFOLD50 },
{ "name": "SBOB5180OCO", "color": "betfold", "cards": SBOB5180OCO3BETFOLD },
{ "name": "SBOB5180OCO", "color": "raise", "cards": SBOB5180OCORAISE },
{ "name": "SBOB5180OCO", "color": "raisecall50", "cards": SBOB5180OCORAISECALL50 },
{ "name": "SBOB5180OCO", "color": "raisecall25", "cards": SBOB5180OCORAISECALL25 },
{ "name": "SBOB5180OCO", "color": "callfold50", "cards": SBOB5180OCOCALLFOLD50 },

/////
{ "name": "SBOB81OCO", "color": "call", "cards": SBOB81OCOCALL },
{ "name": "SBOB81OCO", "color": "callfold50", "cards": SBOB81OCOCALLFOLD50 },
{ "name": "SBOB81OCO", "color": "callfold75", "cards": SBOB81OCOCALLFOLD75 },
{ "name": "SBOB81OCO", "color": "raise", "cards": SBOB81OCORAISE },
{ "name": "SBOB81OCO", "color": "raisecall50", "cards": SBOB81OCORAISECALL50 },
{ "name": "SBOB81OCO", "color": "raisecall75", "cards": SBOB81OCORAISECALL75 },
{ "name": "SBOB81OCO", "color": "betfold", "cards": SBOB81OCO3BETFOLD },
{ "name": "SBOB81OCO", "color": "betfold50", "cards": SBOB81OCO3BETFOLD50 },
{ "name": "SBOB81OCO", "color": "betfoldcall50", "cards": SBOB81OCO3BETFOLDCALL50 },


//////////////////
////////////////////////////
///////////////////////SB VS BTN
{ "name": "SBOB02OBTN", "color": "shove", "cards": SBOB02OBTN },
{ "name": "SBOB24OBTN", "color": "shove", "cards": SBOB24OBTN },
{ "name": "SBOB46OBTN", "color": "shove", "cards": SBOB46OBTN },
{ "name": "SBOB68OBTN", "color": "shove", "cards": SBOB68OBTN },
{ "name": "SBOB810OBTN", "color": "shove", "cards": SBOB810OBTN },
{ "name": "SBOB1012OBTN", "color": "shove", "cards": SBOB1012OBTN },
{ "name": "SBOB1215OBTN", "color": "shove", "cards": SBOB1215OBTN },
{ "name": "SBOB1520OBTN", "color": "shove", "cards": SBOB1520OBTN },
{ "name": "SBOB2025OBTN", "color": "shove", "cards": SBOB2025OBTN },
///////////
{ "name": "SBOB2535OBTN", "color": "call", "cards": SBOB2535OBTNCALL },
{ "name": "SBOB2535OBTN", "color": "callfold50", "cards": SBOB2535OBTNCALLFOLD50 },
{ "name": "SBOB2535OBTN", "color": "raise", "cards": SBOB2535OBTNRAISE },
{ "name": "SBOB2535OBTN", "color": "raisecall50", "cards": SBOB2535OBTNRAISECALL50 },
{ "name": "SBOB2535OBTN", "color": "betfold", "cards": SBOB2535OBTN3BETFOLD },
{ "name": "SBOB2535OBTN", "color": "shove", "cards": SBOB2535OBTNSHOVE },

//
{ "name": "SBOB3650OBTN", "color": "call", "cards": SBOB3650OBTNCALL },
{ "name": "SBOB3650OBTN", "color": "betfold", "cards": SBOB3650OBTN3BETFOLD },

{ "name": "SBOB3650OBTN", "color": "raise", "cards": SBOB3650OBTNRAISE },
{ "name": "SBOB3650OBTN", "color": "raisecall50", "cards": SBOB3650OBTNRAISECALL50 },
{ "name": "SBOB3650OBTN", "color": "raisecall25", "cards": SBOB3650OBTNRAISECALL25 },
{ "name": "SBOB3650OBTN", "color": "betfoldcall50", "cards": SBOB3650OBTN3BETFOLDCALL50 },
{ "name": "SBOB3650OBTN", "color": "betfold50", "cards": SBOB3650OBTN3BETFOLD50 },

//////
{ "name": "SBOB5180OBTN", "color": "call", "cards": SBOB5180OBTNCALL },
{ "name": "SBOB5180OBTN", "color": "betfold", "cards": SBOB5180OBTN3BETFOLD },
{ "name": "SBOB5180OBTN", "color": "raise", "cards": SBOB5180OBTNRAISE },
{ "name": "SBOB5180OBTN", "color": "raisecall50", "cards": SBOB5180OBTNRAISECALL50 },
{ "name": "SBOB5180OBTN", "color": "callfold50", "cards": SBOB5180OBTNCALLFOLD50 },

/////
{ "name": "SBOB81OBTN", "color": "call", "cards": SBOB81OBTNCALL },
{ "name": "SBOB81OBTN", "color": "callfold50", "cards": SBOB81OBTNCALLFOLD50 },
{ "name": "SBOB81OBTN", "color": "callfold75", "cards": SBOB81OBTNCALLFOLD75 },
{ "name": "SBOB81OBTN", "color": "callfold25", "cards": SBOB81OBTNCALLFOLD25 },
{ "name": "SBOB81OBTN", "color": "raise", "cards": SBOB81OBTNRAISE },
{ "name": "SBOB81OBTN", "color": "raisecall50", "cards": SBOB81OBTNRAISECALL50 },
{ "name": "SBOB81OBTN", "color": "betfold", "cards": SBOB81OBTN3BETFOLD },
{ "name": "SBOB81OBTN", "color": "betfold50", "cards": SBOB81OBTN3BETFOLD50 },
{ "name": "SBOB81OBTN", "color": "betfoldcall50", "cards": SBOB81OBTN3BETFOLDCALL50 },


///
//// BB VS EP
/////
{ "name": "BBOB03OEP", "color": "shove", "cards": BBOB03OEPSHOVE },
{ "name": "BBOB46OEP", "color": "shove", "cards": BBOB46OEPSHOVE },
{ "name": "BBOB68OEP", "color": "shove", "cards": BBOB68OEPSHOVE },
{ "name": "BBOB68OEP", "color": "call", "cards": BBOB68OEPCALL },
//
{ "name": "BBOB810OEP", "color": "shove", "cards": BBOB810OEPSHOVE },
{ "name": "BBOB810OEP", "color": "call", "cards": BBOB810OEPCALL },

//
{ "name": "BBOB1016OEP", "color": "call", "cards": BBOB1016OEPCALL },
{ "name": "BBOB1016OEP", "color": "shove", "cards": BBOB1016OEPSHOVE },
{ "name": "BBOB1016OEP", "color": "shovecall50", "cards": BBOB1016OEPSHOVECALL50 },

{ "name": "BBOB1725OEP", "color": "shove", "cards": BBOB1725OEPSHOVE },
{ "name": "BBOB1725OEP", "color": "shoveraise50", "cards": BBOB1725OEPSHOVERAISE50 },
{ "name": "BBOB1725OEP", "color": "shoveraise75", "cards": BBOB1725OEPSHOVERAISE75 },
{ "name": "BBOB1725OEP", "color": "shovecall50", "cards": BBOB1725OEPSHOVECALL50 },
{ "name": "BBOB1725OEP", "color": "call", "cards": BBOB1725OEPCALL },
{ "name": "BBOB1725OEP", "color": "betfoldcall50", "cards": BBOB1725OEP3BETFOLDCALL50 },
{ "name": "BBOB1725OEP", "color": "betfold", "cards": BBOB1725OEP3BETFOLD },
{ "name": "BBOB1725OEP", "color": "raise", "cards": BBOB1725OEPRAISE },
{ "name": "BBOB1725OEP", "color": "raisecall50", "cards": BBOB1725OEPRAISECALL50 },

//////26-35
{ "name": "BBOB2635OEP", "color": "call", "cards": BBOB2635OEPCALL },
{ "name": "BBOB2635OEP", "color": "betfold50", "cards": BBOB2635OEP3BETFOLD50 },
{ "name": "BBOB2635OEP", "color": "betfold", "cards": BBOB2635OEP3BETFOLD },
{ "name": "BBOB2635OEP", "color": "raise", "cards": BBOB2635OEPRAISE },
{ "name": "BBOB2635OEP", "color": "raisecall50", "cards": BBOB2635OEPRAISECALL50 },
{ "name": "BBOB2635OEP", "color": "raisecall75", "cards": BBOB2635OEPRAISECALL75 },
{ "name": "BBOB2635OEP", "color": "raisecall25", "cards": BBOB2635OEPRAISECALL25 },

///////////////36-50
{ "name": "BBOB3650OEP", "color": "call", "cards": BBOB3650OEPCALL },
{ "name": "BBOB3650OEP", "color": "betfoldcall50", "cards": BBOB3650OEP3BETFOLDCALL50 },
{ "name": "BBOB3650OEP", "color": "raise", "cards": BBOB3650OEPRAISE },
{ "name": "BBOB3650OEP", "color": "raisecall75", "cards": BBOB3650OEPRAISECALL75 },


////////////51
{ "name": "BBOB51OEP", "color": "call", "cards": BBOB51OEPCALL },
{ "name": "BBOB51OEP", "color": "betfoldcall50", "cards": BBOB51OEP3BETFOLDCALL50 },
{ "name": "BBOB51OEP", "color": "betfoldcall25", "cards": BBOB51OEP3BETFOLDCALL25 },
{ "name": "BBOB51OEP", "color": "raise", "cards": BBOB51OEPRAISE },
///
//// BB VS MP
/////
{ "name": "BBOB03OMP", "color": "shove", "cards": BBOB03OMPSHOVE },
{ "name": "BBOB46OMP", "color": "shovelight", "cards": BBOB46OMPSHOVELIGHT },
{ "name": "BBOB46OMP", "color": "shove", "cards": BBOB46OMPSHOVE },
{ "name": "BBOB68OMP", "color": "shove", "cards": BBOB68OMPSHOVE },
{ "name": "BBOB68OMP", "color": "call", "cards": BBOB68OMPCALL },
//
{ "name": "BBOB810OMP", "color": "shove", "cards": BBOB810OMPSHOVE },
{ "name": "BBOB810OMP", "color": "call", "cards": BBOB810OMPCALL },

//
{ "name": "BBOB1016OMP", "color": "call", "cards": BBOB1016OMPCALL },
{ "name": "BBOB1016OMP", "color": "shove", "cards": BBOB1016OMPSHOVE },
{ "name": "BBOB1016OMP", "color": "shovecall50", "cards": BBOB1016OMPSHOVECALL50 },

{ "name": "BBOB1725OMP", "color": "shove", "cards": BBOB1725OMPSHOVE },
{ "name": "BBOB1725OMP", "color": "shoveraise50", "cards": BBOB1725OMPSHOVERAISE50 },
{ "name": "BBOB1725OMP", "color": "shovecall50", "cards": BBOB1725OMPSHOVECALL50 },
{ "name": "BBOB1725OMP", "color": "call", "cards": BBOB1725OMPCALL },
{ "name": "BBOB1725OMP", "color": "betfoldcall50", "cards": BBOB1725OMP3BETFOLDCALL50 },
{ "name": "BBOB1725OMP", "color": "betfold", "cards": BBOB1725OMP3BETFOLD },
{ "name": "BBOB1725OMP", "color": "raise", "cards": BBOB1725OMPRAISE },

//////26-35
{ "name": "BBOB2635OMP", "color": "call", "cards": BBOB2635OMPCALL },
{ "name": "BBOB2635OMP", "color": "betfold50", "cards": BBOB2635OMP3BETFOLD50 },
{ "name": "BBOB2635OMP", "color": "betfoldcall50", "cards": BBOB2635OMP3BETFOLDCALL50 },
{ "name": "BBOB2635OMP", "color": "betfoldcall25", "cards": BBOB2635OMP3BETFOLDCALL25 },
{ "name": "BBOB2635OMP", "color": "betfoldcallfold255025", "cards": BBOB2635OMP3BETFOLDCALLFOLD255025 },

{ "name": "BBOB2635OMP", "color": "raise", "cards": BBOB2635OMPRAISE },
{ "name": "BBOB2635OMP", "color": "raisecall50", "cards": BBOB2635OMPRAISECALL50 },

///////////////36-50
{ "name": "BBOB3650OMP", "color": "call", "cards": BBOB3650OMPCALL },
{ "name": "BBOB3650OMP", "color": "callfold25", "cards": BBOB3650OMPCALL25 },
{ "name": "BBOB3650OMP", "color": "betfoldcall50", "cards": BBOB3650OMP3BETFOLDCALL50 },
{ "name": "BBOB3650OMP", "color": "betfoldcall25", "cards": BBOB3650OMP3BETFOLDCALL25 },
{ "name": "BBOB3650OMP", "color": "raise", "cards": BBOB3650OMPRAISE },
{ "name": "BBOB3650OMP", "color": "raisecall50", "cards": BBOB3650OMPRAISECALL50 },


////////////51
{ "name": "BBOB51OMP", "color": "call", "cards": BBOB51OMPCALL },
{ "name": "BBOB51OMP", "color": "betfoldcall50", "cards": BBOB51OMP3BETFOLDCALL50 },
{ "name": "BBOB51OMP", "color": "betfoldcall25", "cards": BBOB51OMP3BETFOLDCALL25 },
{ "name": "BBOB51OMP", "color": "raise", "cards": BBOB51OMPRAISE },

///
//// BB VS BTN
/////
{ "name": "BBOB03OBTN", "color": "shove", "cards": BBOB03OBTNSHOVE },
{ "name": "BBOB46OBTN", "color": "shove", "cards": BBOB46OBTNSHOVE },
{ "name": "BBOB68OBTN", "color": "shove", "cards": BBOB68OBTNSHOVE },
{ "name": "BBOB68OBTN", "color": "call", "cards": BBOB68OBTNCALL },
//
{ "name": "BBOB810OBTN", "color": "shove", "cards": BBOB810OBTNSHOVE },
{ "name": "BBOB810OBTN", "color": "call", "cards": BBOB810OBTNCALL },

//
{ "name": "BBOB1016OBTN", "color": "call", "cards": BBOB1016OBTNCALL },
{ "name": "BBOB1016OBTN", "color": "shove", "cards": BBOB1016OBTNSHOVE },

{ "name": "BBOB1725OBTN", "color": "shove", "cards": BBOB1725OBTNSHOVE },
{ "name": "BBOB1725OBTN", "color": "betfold25", "cards": BBOB1725OBTN3BETFOLD25 },
{ "name": "BBOB1725OBTN", "color": "call", "cards": BBOB1725OBTNCALL },
{ "name": "BBOB1725OBTN", "color": "betfold50", "cards": BBOB1725OBTN3BETFOLD50 },
{ "name": "BBOB1725OBTN", "color": "raise", "cards": BBOB1725OBTNRAISE },

//////26-35
{ "name": "BBOB2635OBTN", "color": "call", "cards": BBOB2635OBTNCALL },
{ "name": "BBOB2635OBTN", "color": "betfold50", "cards": BBOB2635OBTN3BETFOLD50 },
{ "name": "BBOB2635OBTN", "color": "betfoldcall50", "cards": BBOB2635OBTN3BETFOLDCALL50 },
{ "name": "BBOB2635OBTN", "color": "betfoldcall25", "cards": BBOB2635OBTN3BETFOLDCALL25 },

{ "name": "BBOB2635OBTN", "color": "raise", "cards": BBOB2635OBTNRAISE },

///////////////36-50
{ "name": "BBOB3650OBTN", "color": "call", "cards": BBOB3650OBTNCALL },
{ "name": "BBOB3650OBTN", "color": "betfoldcall50", "cards": BBOB3650OBTN3BETFOLDCALL50 },
{ "name": "BBOB3650OBTN", "color": "betfoldcall25", "cards": BBOB3650OBTN3BETFOLDCALL25 },
{ "name": "BBOB3650OBTN", "color": "raise", "cards": BBOB3650OBTNRAISE },
{ "name": "BBOB3650OBTN", "color": "raisecall50", "cards": BBOB3650OBTNRAISECALL50 },


////////////51
{ "name": "BBOB51OBTN", "color": "call", "cards": BBOB51OBTNCALL },
{ "name": "BBOB51OBTN", "color": "betfoldcall50", "cards": BBOB51OBTN3BETFOLDCALL50 },
{ "name": "BBOB51OBTN", "color": "betfoldcall25", "cards": BBOB51OBTN3BETFOLDCALL25 },
{ "name": "BBOB51OBTN", "color": "raise", "cards": BBOB51OBTNRAISE },
{ "name": "BBOB51OBTN", "color": "raisecall50", "cards": BBOB51OBTNRAISECALL50 },


///
//// BB VS CO
/////
{ "name": "BBOB03OCO", "color": "shove", "cards": BBOB03OCOSHOVE },
{ "name": "BBOB46OCO", "color": "shove", "cards": BBOB46OCOSHOVE },
{ "name": "BBOB68OCO", "color": "shove", "cards": BBOB68OCOSHOVE },
{ "name": "BBOB68OCO", "color": "call", "cards": BBOB68OCOCALL },
//
{ "name": "BBOB810OCO", "color": "shove", "cards": BBOB810OCOSHOVE },
{ "name": "BBOB810OCO", "color": "call", "cards": BBOB810OCOCALL },

//
{ "name": "BBOB1016OCO", "color": "call", "cards": BBOB1016OCOCALL },
{ "name": "BBOB1016OCO", "color": "shove", "cards": BBOB1016OCOSHOVE },

{ "name": "BBOB1725OCO", "color": "shove", "cards": BBOB1725OCOSHOVE },
{ "name": "BBOB1725OCO", "color": "shovecall50", "cards": BBOB1725OCOSHOVECALL50 },
{ "name": "BBOB1725OCO", "color": "call", "cards": BBOB1725OCOCALL },
{ "name": "BBOB1725OCO", "color": "betfold50", "cards": BBOB1725OCO3BETFOLD50 },
{ "name": "BBOB1725OCO", "color": "raise", "cards": BBOB1725OCORAISE },

//////26-35
{ "name": "BBOB2635OCO", "color": "call", "cards": BBOB2635OCOCALL },
{ "name": "BBOB2635OCO", "color": "betfold50", "cards": BBOB2635OCO3BETFOLD50 },
{ "name": "BBOB2635OCO", "color": "betfoldcall50", "cards": BBOB2635OCO3BETFOLDCALL50 },
{ "name": "BBOB2635OCO", "color": "betfoldcall25", "cards": BBOB2635OCO3BETFOLDCALL25 },

{ "name": "BBOB2635OCO", "color": "raise", "cards": BBOB2635OCORAISE },
{ "name": "BBOB2635OCO", "color": "raisecall50", "cards": BBOB2635OCORAISECALL50 },

///////////////36-50
{ "name": "BBOB3650OCO", "color": "call", "cards": BBOB3650OCOCALL },
{ "name": "BBOB3650OCO", "color": "betfoldcall50", "cards": BBOB3650OCO3BETFOLDCALL50 },
{ "name": "BBOB3650OCO", "color": "betfoldcall25", "cards": BBOB3650OCO3BETFOLDCALL25 },
{ "name": "BBOB3650OCO", "color": "raise", "cards": BBOB3650OCORAISE },
{ "name": "BBOB3650OCO", "color": "raisecall25", "cards": BBOB3650OCORAISECALL25 },


////////////51
{ "name": "BBOB51OCO", "color": "call", "cards": BBOB51OCOCALL },
{ "name": "BBOB51OCO", "color": "betfoldcall50", "cards": BBOB51OCO3BETFOLDCALL50 },
{ "name": "BBOB51OCO", "color": "betfoldcall25", "cards": BBOB51OCO3BETFOLDCALL25 },
{ "name": "BBOB51OCO", "color": "raise", "cards": BBOB51OCORAISE },
{ "name": "BBOB51OCO", "color": "raisecall25", "cards": BBOB51OCORAISECALL25 },
{ "name": "BBOB51OCO", "color": "callfold50", "cards": BBOB51OCOCALLFOLD50 },


///
//// BB VS SB
/////
{ "name": "BBOB03OSB", "color": "shove", "cards": BBOB03OSBSHOVE },
{ "name": "BBOB46OSB", "color": "shove", "cards": BBOB46OSBSHOVE },
{ "name": "BBOB46OSB", "color": "shovelight", "cards": BBOB46OSBSHOVELIGHT },

{ "name": "BBOB68OSB", "color": "shove", "cards": BBOB68OSBSHOVE },
{ "name": "BBOB810OSB", "color": "shove", "cards": BBOB810OSBSHOVE },

//
{ "name": "BBOB1016OSB", "color": "call", "cards": BBOB1016OSBCALL },
{ "name": "BBOB1016OSB", "color": "shove", "cards": BBOB1016OSBSHOVE },

////
{ "name": "BBOB1725OSB", "color": "shove", "cards": BBOB1725OSBSHOVE },
{ "name": "BBOB1725OSB", "color": "shovecall50", "cards": BBOB1725OSBSHOVECALL50 },

{ "name": "BBOB1725OSB", "color": "betfold25", "cards": BBOB1725OSB3BETFOLD25 },
{ "name": "BBOB1725OSB", "color": "call", "cards": BBOB1725OSBCALL },
{ "name": "BBOB1725OSB", "color": "betfoldcall25", "cards": BBOB1725OSB3BETFOLDCALL25 },
{ "name": "BBOB1725OSB", "color": "raise", "cards": BBOB1725OSBRAISE },

////26-35
{ "name": "BBOB2635OSB", "color": "call", "cards": BBOB2635OSBCALL },
{ "name": "BBOB2635OSB", "color": "betfold50", "cards": BBOB2635OSB3BETFOLD50 },
{ "name": "BBOB2635OSB", "color": "betfoldcall25", "cards": BBOB2635OSB3BETFOLDCALL25 },
{ "name": "BBOB2635OSB", "color": "betfoldcall50", "cards": BBOB2635OSB3BETFOLDCALL50 },
{ "name": "BBOB2635OSB", "color": "raise", "cards": BBOB2635OSBRAISE },
{ "name": "BBOB2635OSB", "color": "shove", "cards": BBOB2635OSBSHOVE },
///////////////36-50
{ "name": "BBOB3650OSB", "color": "call", "cards": BBOB3650OSBCALL },
{ "name": "BBOB3650OSB", "color": "callfold50", "cards": BBOB3650OSBCALLFOLD50 },
{ "name": "BBOB3650OSB", "color": "betfoldcall50", "cards": BBOB3650OSB3BETFOLDCALL50 },
{ "name": "BBOB3650OSB", "color": "betfoldcall25", "cards": BBOB3650OSB3BETFOLDCALL25 },
{ "name": "BBOB3650OSB", "color": "raise", "cards": BBOB3650OSBRAISE },
{ "name": "BBOB3650OSB", "color": "raisecall50", "cards": BBOB3650OSBRAISECALL50 },
{ "name": "BBOB3650OSB", "color": "raisecall25", "cards": BBOB3650OSBRAISECALL25 },
{ "name": "BBOB3650OSB", "color": "betfold50", "cards": BBOB3650OSB3BETFOLD50 },

////////////51
{ "name": "BBOB51OSB", "color": "call", "cards": BBOB51OSBCALL },
{ "name": "BBOB51OSB", "color": "callfold50", "cards": BBOB51OSBCALLFOLD50 },
{ "name": "BBOB51OSB", "color": "callfold75", "cards": BBOB51OSBCALLFOLD75 },
{ "name": "BBOB51OSB", "color": "betfoldcall50", "cards": BBOB51OSB3BETFOLDCALL50 },
{ "name": "BBOB51OSB", "color": "betfoldcall25", "cards": BBOB51OSB3BETFOLDCALL25 },
{ "name": "BBOB51OSB", "color": "raise", "cards": BBOB51OSBRAISE },
{ "name": "BBOB51OSB", "color": "raisecall50", "cards": BBOB51OSBRAISECALL50 },
{ "name": "BBOB51OSB", "color": "raisecall25", "cards": BBOB51OSBRAISECALL25 },

{ "name": "BBOB51OSB", "color": "betfold50", "cards": BBOB51OSB3BETFOLD50 },


/////////////////////////
////////////////////////
/// 3BET AHO EP VS EP


{ "name": "EP3BAHO1620OEP", "color": "call", "cards": EP3BAHO1620OEPCALL },
{ "name": "EP3BAHO1620OEP", "color": "fold", "cards": EP3BAHO1620OEPFOLD },
{ "name": "EP3BAHO1620OEP", "color": "shove", "cards": EP3BAHO1620OEPSHOVE },


/////20-25
{ "name": "EP3BAHO2125OEP", "color": "call", "cards": EP3BAHO2125OEPCALL },
{ "name": "EP3BAHO2125OEP", "color": "fold", "cards": EP3BAHO2125OEPFOLD },
{ "name": "EP3BAHO2125OEP", "color": "shove", "cards": EP3BAHO2125OEPSHOVE },
{ "name": "EP3BAHO2125OEP", "color": "shovecall50", "cards": EP3BAHO2125OEPSHOVECALL50 },
{ "name": "EP3BAHO2125OEP", "color": "shove50", "cards": EP3BAHO2125OEPSHOVE50 },

/////26-35
{ "name": "EP3BAHO2635OEP", "color": "call", "cards": EP3BAHO2635OEPCALL },
{ "name": "EP3BAHO2635OEP", "color": "fold", "cards": EP3BAHO2635OEPFOLD },
{ "name": "EP3BAHO2635OEP", "color": "shove", "cards": EP3BAHO2635OEPSHOVE },
{ "name": "EP3BAHO2635OEP", "color": "shovecall50", "cards": EP3BAHO2635OEPSHOVECALL50 },
{ "name": "EP3BAHO2635OEP", "color": "shove50", "cards": EP3BAHO2635OEPSHOVE50 },

//36-50
{ "name": "EP3BAHO3660OEP", "color": "call", "cards": EP3BAHO3660OEPCALL },
{ "name": "EP3BAHO3660OEP", "color": "fold", "cards": EP3BAHO3660OEPFOLD },
{ "name": "EP3BAHO3660OEP", "color": "raise", "cards": EP3BAHO3660OEPRAISE },
{ "name": "EP3BAHO3660OEP", "color": "raisecall50", "cards": EP3BAHO3660OEPRAISE50 },
{ "name": "EP3BAHO3660OEP", "color": "betfold", "cards": EP3BAHO3660OEP4BETFOLD },
{ "name": "EP3BAHO3660OEP", "color": "betfold50", "cards": EP3BAHO3660OEP4BETFOLD50 },


////61

{ "name": "EP3BAHO61OEP", "color": "call", "cards": EP3BAHO61OEPCALL },
{ "name": "EP3BAHO61OEP", "color": "fold", "cards": EP3BAHO61OEPFOLD },
{ "name": "EP3BAHO61OEP", "color": "raise", "cards": EP3BAHO61OEPRAISE },
{ "name": "EP3BAHO61OEP", "color": "raisecall50", "cards": EP3BAHO61OEPRAISE50 },
{ "name": "EP3BAHO61OEP", "color": "betfold", "cards": EP3BAHO61OEP4BETFOLD },

/////////////////////////
////////////////////////
/// 3BET AHO EP VS MP
{ "name": "EP3BAHO1620OMP", "color": "call", "cards": EP3BAHO1620OMPCALL },
{ "name": "EP3BAHO1620OMP", "color": "fold", "cards": EP3BAHO1620OMPFOLD },
{ "name": "EP3BAHO1620OMP", "color": "shove", "cards": EP3BAHO1620OMPSHOVE },


/////20-25
{ "name": "EP3BAHO2125OMP", "color": "call", "cards": EP3BAHO2125OMPCALL },
{ "name": "EP3BAHO2125OMP", "color": "fold", "cards": EP3BAHO2125OMPFOLD },
{ "name": "EP3BAHO2125OMP", "color": "shove", "cards": EP3BAHO2125OMPSHOVE },
{ "name": "EP3BAHO2125OMP", "color": "shovecall50", "cards": EP3BAHO2125OMPSHOVECALL50 },
{ "name": "EP3BAHO2125OMP", "color": "shove50", "cards": EP3BAHO2125OMPSHOVE50 },

/////26-35
{ "name": "EP3BAHO2635OMP", "color": "call", "cards": EP3BAHO2635OMPCALL },
{ "name": "EP3BAHO2635OMP", "color": "fold", "cards": EP3BAHO2635OMPFOLD },
{ "name": "EP3BAHO2635OMP", "color": "shove", "cards": EP3BAHO2635OMPSHOVE },
{ "name": "EP3BAHO2635OMP", "color": "shovecall50", "cards": EP3BAHO2635OMPSHOVECALL50 },
{ "name": "EP3BAHO2635OMP", "color": "shove50", "cards": EP3BAHO2635OMPSHOVE50 },

//36-50
{ "name": "EP3BAHO3660OMP", "color": "call", "cards": EP3BAHO3660OMPCALL },
{ "name": "EP3BAHO3660OMP", "color": "fold", "cards": EP3BAHO3660OMPFOLD },
{ "name": "EP3BAHO3660OMP", "color": "raise", "cards": EP3BAHO3660OMPRAISE },
{ "name": "EP3BAHO3660OMP", "color": "raisecall50", "cards": EP3BAHO3660OMPRAISE50 },
{ "name": "EP3BAHO3660OMP", "color": "betfold", "cards": EP3BAHO3660OMP4BETFOLD },
{ "name": "EP3BAHO3660OMP", "color": "betfold50", "cards": EP3BAHO3660OMP4BETFOLD50 },


////61

{ "name": "EP3BAHO61OMP", "color": "call", "cards": EP3BAHO61OMPCALL },
{ "name": "EP3BAHO61OMP", "color": "fold", "cards": EP3BAHO61OMPFOLD },
{ "name": "EP3BAHO61OMP", "color": "raise", "cards": EP3BAHO61OMPRAISE },
{ "name": "EP3BAHO61OMP", "color": "raisecall50", "cards": EP3BAHO61OMPRAISE50 },
{ "name": "EP3BAHO61OMP", "color": "betfold", "cards": EP3BAHO61OMP4BETFOLD },

/////////////////////////
////////////////////////
/// 3BET AHO EP VS CO
{ "name": "EP3BAHO1620OCO", "color": "call", "cards": EP3BAHO1620OCOCALL },
{ "name": "EP3BAHO1620OCO", "color": "fold", "cards": EP3BAHO1620OCOFOLD },
{ "name": "EP3BAHO1620OCO", "color": "shove", "cards": EP3BAHO1620OCOSHOVE },


/////20-25
{ "name": "EP3BAHO2125OCO", "color": "call", "cards": EP3BAHO2125OCOCALL },
{ "name": "EP3BAHO2125OCO", "color": "fold", "cards": EP3BAHO2125OCOFOLD },
{ "name": "EP3BAHO2125OCO", "color": "shove", "cards": EP3BAHO2125OCOSHOVE },
{ "name": "EP3BAHO2125OCO", "color": "shovecall50", "cards": EP3BAHO2125OCOSHOVECALL50 },
{ "name": "EP3BAHO2125OCO", "color": "shove50", "cards": EP3BAHO2125OCOSHOVE50 },

/////26-35
{ "name": "EP3BAHO2635OCO", "color": "call", "cards": EP3BAHO2635OCOCALL },
{ "name": "EP3BAHO2635OCO", "color": "fold", "cards": EP3BAHO2635OCOFOLD },
{ "name": "EP3BAHO2635OCO", "color": "shove", "cards": EP3BAHO2635OCOSHOVE },
{ "name": "EP3BAHO2635OCO", "color": "shovecall50", "cards": EP3BAHO2635OCOSHOVECALL50 },
{ "name": "EP3BAHO2635OCO", "color": "shove50", "cards": EP3BAHO2635OCOSHOVE50 },

//36-50
{ "name": "EP3BAHO3660OCO", "color": "call", "cards": EP3BAHO3660OCOCALL },
{ "name": "EP3BAHO3660OCO", "color": "fold", "cards": EP3BAHO3660OCOFOLD },
{ "name": "EP3BAHO3660OCO", "color": "raise", "cards": EP3BAHO3660OCORAISE },
{ "name": "EP3BAHO3660OCO", "color": "raisecall50", "cards": EP3BAHO3660OCORAISE50 },
{ "name": "EP3BAHO3660OCO", "color": "betfold", "cards": EP3BAHO3660OCO4BETFOLD },
{ "name": "EP3BAHO3660OCO", "color": "betfold50", "cards": EP3BAHO3660OCO4BETFOLD50 },


////61

{ "name": "EP3BAHO61OCO", "color": "call", "cards": EP3BAHO61OCOCALL },
{ "name": "EP3BAHO61OCO", "color": "fold", "cards": EP3BAHO61OCOFOLD },
{ "name": "EP3BAHO61OCO", "color": "raise", "cards": EP3BAHO61OCORAISE },
{ "name": "EP3BAHO61OCO", "color": "raisecall50", "cards": EP3BAHO61OCORAISE50 },
{ "name": "EP3BAHO61OCO", "color": "betfold", "cards": EP3BAHO61OCO4BETFOLD },

/////////////////////////
////////////////////////
/// 3BET AHO EP VS BTN

{ "name": "EP3BAHO1620OBTN", "color": "call", "cards": EP3BAHO1620OBTNCALL },
{ "name": "EP3BAHO1620OBTN", "color": "fold", "cards": EP3BAHO1620OBTNFOLD },
{ "name": "EP3BAHO1620OBTN", "color": "shove", "cards": EP3BAHO1620OBTNSHOVE },


/////20-25
{ "name": "EP3BAHO2125OBTN", "color": "call", "cards": EP3BAHO2125OBTNCALL },
{ "name": "EP3BAHO2125OBTN", "color": "fold", "cards": EP3BAHO2125OBTNFOLD },
{ "name": "EP3BAHO2125OBTN", "color": "shove", "cards": EP3BAHO2125OBTNSHOVE },
{ "name": "EP3BAHO2125OBTN", "color": "shovecall50", "cards": EP3BAHO2125OBTNSHOVECALL50 },
{ "name": "EP3BAHO2125OBTN", "color": "shove50", "cards": EP3BAHO2125OBTNSHOVE50 },

/////26-35
{ "name": "EP3BAHO2635OBTN", "color": "call", "cards": EP3BAHO2635OBTNCALL },
{ "name": "EP3BAHO2635OBTN", "color": "fold", "cards": EP3BAHO2635OBTNFOLD },
{ "name": "EP3BAHO2635OBTN", "color": "shove", "cards": EP3BAHO2635OBTNSHOVE },
{ "name": "EP3BAHO2635OBTN", "color": "shovecall50", "cards": EP3BAHO2635OBTNSHOVECALL50 },
{ "name": "EP3BAHO2635OBTN", "color": "shove50", "cards": EP3BAHO2635OBTNSHOVE50 },

//36-50
{ "name": "EP3BAHO3660OBTN", "color": "call", "cards": EP3BAHO3660OBTNCALL },
{ "name": "EP3BAHO3660OBTN", "color": "fold", "cards": EP3BAHO3660OBTNFOLD },
{ "name": "EP3BAHO3660OBTN", "color": "raise", "cards": EP3BAHO3660OBTNRAISE },
{ "name": "EP3BAHO3660OBTN", "color": "raisecall50", "cards": EP3BAHO3660OBTNRAISE50 },
{ "name": "EP3BAHO3660OBTN", "color": "betfold", "cards": EP3BAHO3660OBTN4BETFOLD },
{ "name": "EP3BAHO3660OBTN", "color": "betfold50", "cards": EP3BAHO3660OBTN4BETFOLD50 },


////61

{ "name": "EP3BAHO61OBTN", "color": "call", "cards": EP3BAHO61OBTNCALL },
{ "name": "EP3BAHO61OBTN", "color": "fold", "cards": EP3BAHO61OBTNFOLD },
{ "name": "EP3BAHO61OBTN", "color": "raise", "cards": EP3BAHO61OBTNRAISE },
{ "name": "EP3BAHO61OBTN", "color": "raisecall50", "cards": EP3BAHO61OBTNRAISE50 },
{ "name": "EP3BAHO61OBTN", "color": "betfold", "cards": EP3BAHO61OBTN4BETFOLD },

/////////////////////////
////////////////////////
/// 3BET AHO EP VS SB


{ "name": "EP3BAHO1620OSB", "color": "call", "cards": EP3BAHO1620OSBCALL },
{ "name": "EP3BAHO1620OSB", "color": "fold", "cards": EP3BAHO1620OSBFOLD },
{ "name": "EP3BAHO1620OSB", "color": "shove", "cards": EP3BAHO1620OSBSHOVE },


/////20-25
{ "name": "EP3BAHO2125OSB", "color": "call", "cards": EP3BAHO2125OSBCALL },
{ "name": "EP3BAHO2125OSB", "color": "fold", "cards": EP3BAHO2125OSBFOLD },
{ "name": "EP3BAHO2125OSB", "color": "shove", "cards": EP3BAHO2125OSBSHOVE },
{ "name": "EP3BAHO2125OSB", "color": "shovecall50", "cards": EP3BAHO2125OSBSHOVECALL50 },
{ "name": "EP3BAHO2125OSB", "color": "shove50", "cards": EP3BAHO2125OSBSHOVE50 },

/////26-35
{ "name": "EP3BAHO2635OSB", "color": "call", "cards": EP3BAHO2635OSBCALL },
{ "name": "EP3BAHO2635OSB", "color": "callfold50", "cards": EP3BAHO2635OSBCALLFOLD50 },
{ "name": "EP3BAHO2635OSB", "color": "fold", "cards": EP3BAHO2635OSBFOLD },
{ "name": "EP3BAHO2635OSB", "color": "shove", "cards": EP3BAHO2635OSBSHOVE },
{ "name": "EP3BAHO2635OSB", "color": "shovecall50", "cards": EP3BAHO2635OSBSHOVECALL50 },
{ "name": "EP3BAHO2635OSB", "color": "shove50", "cards": EP3BAHO2635OSBSHOVE50 },

//36-50
{ "name": "EP3BAHO3660OSB", "color": "call", "cards": EP3BAHO3660OSBCALL },
{ "name": "EP3BAHO3660OSB", "color": "fold", "cards": EP3BAHO3660OSBFOLD },
{ "name": "EP3BAHO3660OSB", "color": "raise", "cards": EP3BAHO3660OSBRAISE },
{ "name": "EP3BAHO3660OSB", "color": "raisecall50", "cards": EP3BAHO3660OSBRAISE50 },
{ "name": "EP3BAHO3660OSB", "color": "betfold", "cards": EP3BAHO3660OSB4BETFOLD },
{ "name": "EP3BAHO3660OSB", "color": "betfold50", "cards": EP3BAHO3660OSB4BETFOLD50 },
{ "name": "EP3BAHO3660OSB", "color": "betfoldcall50", "cards": EP3BAHO3660OSB4BETFOLDCALL50 },


////61

{ "name": "EP3BAHO61OSB", "color": "call", "cards": EP3BAHO61OSBCALL },
{ "name": "EP3BAHO61OSB", "color": "fold", "cards": EP3BAHO61OSBFOLD },
{ "name": "EP3BAHO61OSB", "color": "raise", "cards": EP3BAHO61OSBRAISE },
{ "name": "EP3BAHO61OSB", "color": "raisecall50", "cards": EP3BAHO61OSBRAISE50 },
{ "name": "EP3BAHO61OSB", "color": "betfold", "cards": EP3BAHO61OSB4BETFOLD },

/////////////////////////
////////////////////////
/// 3BET AHO EP VS BB
{ "name": "EP3BAHO1620OBB", "color": "call", "cards": EP3BAHO1620OBBCALL },
{ "name": "EP3BAHO1620OBB", "color": "fold", "cards": EP3BAHO1620OBBFOLD },
{ "name": "EP3BAHO1620OBB", "color": "shove", "cards": EP3BAHO1620OBBSHOVE },
{ "name": "EP3BAHO1620OBB", "color": "shove50", "cards": EP3BAHO1620OBBSHOVE50 },


/////21-25
{ "name": "EP3BAHO2125OBB", "color": "call", "cards": EP3BAHO2125OBBCALL },
{ "name": "EP3BAHO2125OBB", "color": "fold", "cards": EP3BAHO2125OBBFOLD },
{ "name": "EP3BAHO2125OBB", "color": "shove", "cards": EP3BAHO2125OBBSHOVE },
{ "name": "EP3BAHO2125OBB", "color": "shove50", "cards": EP3BAHO2125OBBSHOVE50 },

/////26-35
{ "name": "EP3BAHO2635OBB", "color": "call", "cards": EP3BAHO2635OBBCALL },
{ "name": "EP3BAHO2635OBB", "color": "callfold50", "cards": EP3BAHO2635OBBCALLFOLD50 },
{ "name": "EP3BAHO2635OBB", "color": "fold", "cards": EP3BAHO2635OBBFOLD },
{ "name": "EP3BAHO2635OBB", "color": "shove", "cards": EP3BAHO2635OBBSHOVE },

//36-50
{ "name": "EP3BAHO3660OBB", "color": "call", "cards": EP3BAHO3660OBBCALL },
{ "name": "EP3BAHO3660OBB", "color": "fold", "cards": EP3BAHO3660OBBFOLD },
{ "name": "EP3BAHO3660OBB", "color": "raise", "cards": EP3BAHO3660OBBRAISE },
{ "name": "EP3BAHO3660OBB", "color": "raisecall50", "cards": EP3BAHO3660OBBRAISE50 },
{ "name": "EP3BAHO3660OBB", "color": "betfold25", "cards": EP3BAHO3660OBB4BETFOLD25 },
{ "name": "EP3BAHO3660OBB", "color": "betfold50", "cards": EP3BAHO3660OBB4BETFOLD50 },
{ "name": "EP3BAHO3660OBB", "color": "callfold50", "cards": EP3BAHO3660OBBCALLFOLD50 },


////61

{ "name": "EP3BAHO61OBB", "color": "call", "cards": EP3BAHO61OBBCALL },
{ "name": "EP3BAHO61OBB", "color": "fold", "cards": EP3BAHO61OBBFOLD },
{ "name": "EP3BAHO61OBB", "color": "raise", "cards": EP3BAHO61OBBRAISE },
{ "name": "EP3BAHO61OBB", "color": "raisecall50", "cards": EP3BAHO61OBBRAISE50 },
{ "name": "EP3BAHO61OBB", "color": "betfold50", "cards": EP3BAHO61OBB4BETFOLD50 },
{ "name": "EP3BAHO61OBB", "color": "betfold25", "cards": EP3BAHO61OBB4BETFOLD25 },
{ "name": "EP3BAHO61OBB", "color": "betfoldcall50", "cards": EP3BAHO61OBB4BETFOLDCALL50 },

/////////////////////////
////////////////////////
/// 3BET AHO MP VS MP
{ "name": "MP3BAHO1620OMP", "color": "call", "cards": MP3BAHO1620OMPCALL },
{ "name": "MP3BAHO1620OMP", "color": "fold", "cards": MP3BAHO1620OMPFOLD },
{ "name": "MP3BAHO1620OMP", "color": "shove", "cards": MP3BAHO1620OMPSHOVE },


/////20-25
{ "name": "MP3BAHO2125OMP", "color": "call", "cards": MP3BAHO2125OMPCALL },
{ "name": "MP3BAHO2125OMP", "color": "fold", "cards": MP3BAHO2125OMPFOLD },
{ "name": "MP3BAHO2125OMP", "color": "shove", "cards": MP3BAHO2125OMPSHOVE },
{ "name": "MP3BAHO2125OMP", "color": "shovecall50", "cards": MP3BAHO2125OMPSHOVECALL50 },
{ "name": "MP3BAHO2125OMP", "color": "shove50", "cards": MP3BAHO2125OMPSHOVE50 },

/////26-35
{ "name": "MP3BAHO2635OMP", "color": "call", "cards": MP3BAHO2635OMPCALL },
{ "name": "MP3BAHO2635OMP", "color": "fold", "cards": MP3BAHO2635OMPFOLD },
{ "name": "MP3BAHO2635OMP", "color": "shove", "cards": MP3BAHO2635OMPSHOVE },
{ "name": "MP3BAHO2635OMP", "color": "shovecall50", "cards": MP3BAHO2635OMPSHOVECALL50 },
{ "name": "MP3BAHO2635OMP", "color": "shove50", "cards": MP3BAHO2635OMPSHOVE50 },

//36-50
{ "name": "MP3BAHO3660OMP", "color": "call", "cards": MP3BAHO3660OMPCALL },
{ "name": "MP3BAHO3660OMP", "color": "fold", "cards": MP3BAHO3660OMPFOLD },
{ "name": "MP3BAHO3660OMP", "color": "raise", "cards": MP3BAHO3660OMPRAISE },
{ "name": "MP3BAHO3660OMP", "color": "raisecall50", "cards": MP3BAHO3660OMPRAISE50 },
{ "name": "MP3BAHO3660OMP", "color": "betfold", "cards": MP3BAHO3660OMP4BETFOLD },
{ "name": "MP3BAHO3660OMP", "color": "betfold50", "cards": MP3BAHO3660OMP4BETFOLD50 },


////61

{ "name": "MP3BAHO61OMP", "color": "call", "cards": MP3BAHO61OMPCALL },
{ "name": "MP3BAHO61OMP", "color": "fold", "cards": MP3BAHO61OMPFOLD },
{ "name": "MP3BAHO61OMP", "color": "raise", "cards": MP3BAHO61OMPRAISE },
{ "name": "MP3BAHO61OMP", "color": "raisecall50", "cards": MP3BAHO61OMPRAISE50 },
{ "name": "MP3BAHO61OMP", "color": "betfold", "cards": MP3BAHO61OMP4BETFOLD },

/////////////////////////
////////////////////////
/// 3BET AHO MP VS CO
{ "name": "MP3BAHO1620OCO", "color": "call", "cards": MP3BAHO1620OCOCALL },
{ "name": "MP3BAHO1620OCO", "color": "fold", "cards": MP3BAHO1620OCOFOLD },
{ "name": "MP3BAHO1620OCO", "color": "shove", "cards": MP3BAHO1620OCOSHOVE },
{ "name": "MP3BAHO1620OCO", "color": "shove50", "cards": MP3BAHO1620OCOSHOVE50 },
{ "name": "MP3BAHO1620OCO", "color": "callfold50", "cards": MP3BAHO1620OCOCALLFOLD50 },

/////20-25
{ "name": "MP3BAHO2125OCO", "color": "call", "cards": MP3BAHO2125OCOCALL },
{ "name": "MP3BAHO2125OCO", "color": "fold", "cards": MP3BAHO2125OCOFOLD },
{ "name": "MP3BAHO2125OCO", "color": "shove", "cards": MP3BAHO2125OCOSHOVE },
{ "name": "MP3BAHO2125OCO", "color": "shovecall50", "cards": MP3BAHO2125OCOSHOVECALL50 },
{ "name": "MP3BAHO2125OCO", "color": "shove50", "cards": MP3BAHO2125OCOSHOVE50 },
{ "name": "MP3BAHO2125OCO", "color": "shove25", "cards": MP3BAHO2125OCOSHOVE25 },

/////26-35
{ "name": "MP3BAHO2635OCO", "color": "call", "cards": MP3BAHO2635OCOCALL },
{ "name": "MP3BAHO2635OCO", "color": "callfold50", "cards": MP3BAHO2635OCOCALLFOLD50 },
{ "name": "MP3BAHO2635OCO", "color": "fold", "cards": MP3BAHO2635OCOFOLD },
{ "name": "MP3BAHO2635OCO", "color": "shove", "cards": MP3BAHO2635OCOSHOVE },
{ "name": "MP3BAHO2635OCO", "color": "shovecall50", "cards": MP3BAHO2635OCOSHOVECALL50 },
{ "name": "MP3BAHO2635OCO", "color": "shove50", "cards": MP3BAHO2635OCOSHOVE50 },
{ "name": "MP3BAHO2635OCO", "color": "shove25", "cards": MP3BAHO2635OCOSHOVE25 },
{ "name": "MP3BAHO2635OCO", "color": "shove75", "cards": MP3BAHO2635OCOSHOVE75 },
{ "name": "MP3BAHO2635OCO", "color": "shovecall75", "cards": MP3BAHO2635OCOSHOVECALL75 },
//36-50
{ "name": "MP3BAHO3660OCO", "color": "call", "cards": MP3BAHO3660OCOCALL },
{ "name": "MP3BAHO3660OCO", "color": "callfold50", "cards": MP3BAHO3660OCOCALLFOLD50 },
{ "name": "MP3BAHO3660OCO", "color": "fold", "cards": MP3BAHO3660OCOFOLD },
{ "name": "MP3BAHO3660OCO", "color": "raise", "cards": MP3BAHO3660OCORAISE },
{ "name": "MP3BAHO3660OCO", "color": "raisecall50", "cards": MP3BAHO3660OCORAISE50 },
{ "name": "MP3BAHO3660OCO", "color": "betfold", "cards": MP3BAHO3660OCO4BETFOLD },
{ "name": "MP3BAHO3660OCO", "color": "betfold50", "cards": MP3BAHO3660OCO4BETFOLD50 },


////61

{ "name": "MP3BAHO61OCO", "color": "call", "cards": MP3BAHO61OCOCALL },
{ "name": "MP3BAHO61OCO", "color": "callfold50", "cards": MP3BAHO61OCOCALLFOLD50 },
{ "name": "MP3BAHO61OCO", "color": "fold", "cards": MP3BAHO61OCOFOLD },
{ "name": "MP3BAHO61OCO", "color": "raise", "cards": MP3BAHO61OCORAISE },
{ "name": "MP3BAHO61OCO", "color": "raisecall50", "cards": MP3BAHO61OCORAISE50 },
{ "name": "MP3BAHO61OCO", "color": "betfold", "cards": MP3BAHO61OCO4BETFOLD },

/////////////////////////
////////////////////////
/// 3BET AHO MP VS btN
{ "name": "MP3BAHO1620OBTN", "color": "call", "cards": MP3BAHO1620OBTNCALL },
{ "name": "MP3BAHO1620OBTN", "color": "fold", "cards": MP3BAHO1620OBTNFOLD },
{ "name": "MP3BAHO1620OBTN", "color": "shove", "cards": MP3BAHO1620OBTNSHOVE },
{ "name": "MP3BAHO1620OBTN", "color": "shove50", "cards": MP3BAHO1620OBTNSHOVE50 },
{ "name": "MP3BAHO1620OBTN", "color": "callfold50", "cards": MP3BAHO1620OBTNCALLFOLD50 },

/////20-25
{ "name": "MP3BAHO2125OBTN", "color": "call", "cards": MP3BAHO2125OBTNCALL },
{ "name": "MP3BAHO2125OBTN", "color": "fold", "cards": MP3BAHO2125OBTNFOLD },
{ "name": "MP3BAHO2125OBTN", "color": "shove", "cards": MP3BAHO2125OBTNSHOVE },
{ "name": "MP3BAHO2125OBTN", "color": "shovecall50", "cards": MP3BAHO2125OBTNSHOVECALL50 },
{ "name": "MP3BAHO2125OBTN", "color": "shove50", "cards": MP3BAHO2125OBTNSHOVE50 },
{ "name": "MP3BAHO2125OBTN", "color": "shove25", "cards": MP3BAHO2125OBTNSHOVE25 },

/////26-35
{ "name": "MP3BAHO2635OBTN", "color": "call", "cards": MP3BAHO2635OBTNCALL },
{ "name": "MP3BAHO2635OBTN", "color": "callfold50", "cards": MP3BAHO2635OBTNCALLFOLD50 },
{ "name": "MP3BAHO2635OBTN", "color": "fold", "cards": MP3BAHO2635OBTNFOLD },
{ "name": "MP3BAHO2635OBTN", "color": "shove", "cards": MP3BAHO2635OBTNSHOVE },
{ "name": "MP3BAHO2635OBTN", "color": "shovecall50", "cards": MP3BAHO2635OBTNSHOVECALL50 },
{ "name": "MP3BAHO2635OBTN", "color": "shove50", "cards": MP3BAHO2635OBTNSHOVE50 },
{ "name": "MP3BAHO2635OBTN", "color": "shove25", "cards": MP3BAHO2635OBTNSHOVE25 },
{ "name": "MP3BAHO2635OBTN", "color": "shove75", "cards": MP3BAHO2635OBTNSHOVE75 },
{ "name": "MP3BAHO2635OBTN", "color": "shovecall75", "cards": MP3BAHO2635OBTNSHOVECALL75 },
//36-50
{ "name": "MP3BAHO3660OBTN", "color": "call", "cards": MP3BAHO3660OBTNCALL },
{ "name": "MP3BAHO3660OBTN", "color": "callfold50", "cards": MP3BAHO3660OBTNCALLFOLD50 },
{ "name": "MP3BAHO3660OBTN", "color": "fold", "cards": MP3BAHO3660OBTNFOLD },
{ "name": "MP3BAHO3660OBTN", "color": "raise", "cards": MP3BAHO3660OBTNRAISE },
{ "name": "MP3BAHO3660OBTN", "color": "raisecall50", "cards": MP3BAHO3660OBTNRAISE50 },
{ "name": "MP3BAHO3660OBTN", "color": "betfold", "cards": MP3BAHO3660OBTN4BETFOLD },
{ "name": "MP3BAHO3660OBTN", "color": "betfold50", "cards": MP3BAHO3660OBTN4BETFOLD50 },


////61

{ "name": "MP3BAHO61OBTN", "color": "call", "cards": MP3BAHO61OBTNCALL },
{ "name": "MP3BAHO61OBTN", "color": "callfold50", "cards": MP3BAHO61OBTNCALLFOLD50 },
{ "name": "MP3BAHO61OBTN", "color": "fold", "cards": MP3BAHO61OBTNFOLD },
{ "name": "MP3BAHO61OBTN", "color": "raise", "cards": MP3BAHO61OBTNRAISE },
{ "name": "MP3BAHO61OBTN", "color": "raisecall50", "cards": MP3BAHO61OBTNRAISE50 },
{ "name": "MP3BAHO61OBTN", "color": "betfold", "cards": MP3BAHO61OBTN4BETFOLD },

/////////////////////////
////////////////////////
/// 3BET AHO MP VS SB
{ "name": "MP3BAHO1620OSB", "color": "call", "cards": MP3BAHO1620OSBCALL },
{ "name": "MP3BAHO1620OSB", "color": "fold", "cards": MP3BAHO1620OSBFOLD },
{ "name": "MP3BAHO1620OSB", "color": "shove", "cards": MP3BAHO1620OSBSHOVE },
{ "name": "MP3BAHO1620OSB", "color": "shove50", "cards": MP3BAHO1620OSBSHOVE50 },
{ "name": "MP3BAHO1620OSB", "color": "callfold50", "cards": MP3BAHO1620OSBCALLFOLD50 },

/////21-25
{ "name": "MP3BAHO2125OSB", "color": "call", "cards": MP3BAHO2125OSBCALL },
{ "name": "MP3BAHO2125OSB", "color": "fold", "cards": MP3BAHO2125OSBFOLD },
{ "name": "MP3BAHO2125OSB", "color": "shove", "cards": MP3BAHO2125OSBSHOVE },
{ "name": "MP3BAHO2125OSB", "color": "shove50", "cards": MP3BAHO2125OSBSHOVE50 },
{ "name": "MP3BAHO2125OSB", "color": "shove25", "cards": MP3BAHO2125OSBSHOVE25 },

/////26-35
{ "name": "MP3BAHO2635OSB", "color": "call", "cards": MP3BAHO2635OSBCALL },
{ "name": "MP3BAHO2635OSB", "color": "callfold50", "cards": MP3BAHO2635OSBCALLFOLD50 },
{ "name": "MP3BAHO2635OSB", "color": "fold", "cards": MP3BAHO2635OSBFOLD },
{ "name": "MP3BAHO2635OSB", "color": "shove", "cards": MP3BAHO2635OSBSHOVE },
{ "name": "MP3BAHO2635OSB", "color": "shovecall50", "cards": MP3BAHO2635OSBSHOVECALL50 },
{ "name": "MP3BAHO2635OSB", "color": "shove50", "cards": MP3BAHO2635OSBSHOVE50 },
{ "name": "MP3BAHO2635OSB", "color": "shove25", "cards": MP3BAHO2635OSBSHOVE25 },
//36-50
{ "name": "MP3BAHO3660OSB", "color": "call", "cards": MP3BAHO3660OSBCALL },
{ "name": "MP3BAHO3660OSB", "color": "callfold50", "cards": MP3BAHO3660OSBCALLFOLD50 },
{ "name": "MP3BAHO3660OSB", "color": "fold", "cards": MP3BAHO3660OSBFOLD },
{ "name": "MP3BAHO3660OSB", "color": "raise", "cards": MP3BAHO3660OSBRAISE },
{ "name": "MP3BAHO3660OSB", "color": "raisecall50", "cards": MP3BAHO3660OSBRAISE50 },
{ "name": "MP3BAHO3660OSB", "color": "raisecall75", "cards": MP3BAHO3660OSBRAISE75 },
{ "name": "MP3BAHO3660OSB", "color": "betfold", "cards": MP3BAHO3660OSB4BETFOLD },
{ "name": "MP3BAHO3660OSB", "color": "betfoldcall50", "cards": MP3BAHO3660OSB4BETFOLDCALL50 },

////61
{ "name": "MP3BAHO61OSB", "color": "call", "cards": MP3BAHO61OSBCALL },
{ "name": "MP3BAHO61OSB", "color": "callfold50", "cards": MP3BAHO61OSBCALLFOLD50 },
{ "name": "MP3BAHO61OSB", "color": "fold", "cards": MP3BAHO61OSBFOLD },
{ "name": "MP3BAHO61OSB", "color": "raise", "cards": MP3BAHO61OSBRAISE },
{ "name": "MP3BAHO61OSB", "color": "raisecall50", "cards": MP3BAHO61OSBRAISE50 },
{ "name": "MP3BAHO61OSB", "color": "betfold", "cards": MP3BAHO61OSB4BETFOLD },
{ "name": "MP3BAHO61OSB", "color": "betfoldcall50", "cards": MP3BAHO61OSB4BETFOLDCALL50 },
{ "name": "MP3BAHO61OSB", "color": "betfold50", "cards": MP3BAHO61OSB4BETFOLD50 },
/////////////////////////
////////////////////////
/// 3BET AHO MP VS BB
// { "name": "MP3BAHO1620OBB", "color": "call", "cards": MP3BAHO1620OBBCALL },
// { "name": "MP3BAHO1620OBB", "color": "fold", "cards": MP3BAHO1620OBBFOLD },
// { "name": "MP3BAHO1620OBB", "color": "shove", "cards": MP3BAHO1620OBBSHOVE },

//21-25
{ "name": "MP3BAHO2125OBB", "color": "call", "cards": MP3BAHO2125OBBCALL },
{ "name": "MP3BAHO2125OBB", "color": "fold", "cards": MP3BAHO2125OBBFOLD },
{ "name": "MP3BAHO2125OBB", "color": "shove", "cards": MP3BAHO2125OBBSHOVE },
{ "name": "MP3BAHO2125OBB", "color": "shove50", "cards": MP3BAHO2125OBBSHOVE50 },

/////26-35
{ "name": "MP3BAHO2635OBB", "color": "call", "cards": MP3BAHO2635OBBCALL },
{ "name": "MP3BAHO2635OBB", "color": "fold", "cards": MP3BAHO2635OBBFOLD },
{ "name": "MP3BAHO2635OBB", "color": "shove", "cards": MP3BAHO2635OBBSHOVE },
{ "name": "MP3BAHO2635OBB", "color": "shove50", "cards": MP3BAHO2635OBBSHOVE50 },
{ "name": "MP3BAHO2635OBB", "color": "shovecall50", "cards": MP3BAHO2635OBBSHOVECALL50 },


//36-50
{ "name": "MP3BAHO3660OBB", "color": "call", "cards": MP3BAHO3660OBBCALL },
{ "name": "MP3BAHO3660OBB", "color": "fold", "cards": MP3BAHO3660OBBFOLD },
{ "name": "MP3BAHO3660OBB", "color": "raise", "cards": MP3BAHO3660OBBRAISE },
{ "name": "MP3BAHO3660OBB", "color": "raisecall50", "cards": MP3BAHO3660OBBRAISE50 },
{ "name": "MP3BAHO3660OBB", "color": "raisecall75", "cards": MP3BAHO3660OBBRAISE75 },
{ "name": "MP3BAHO3660OBB", "color": "betfoldcall50", "cards": MP3BAHO3660OBB4BETFOLDCALL50 },


////61
{ "name": "MP3BAHO61OBB", "color": "call", "cards": MP3BAHO61OBBCALL },
{ "name": "MP3BAHO61OBB", "color": "callfold50", "cards": MP3BAHO61OBBCALLFOLD50 },
{ "name": "MP3BAHO61OBB", "color": "fold", "cards": MP3BAHO61OBBFOLD },
{ "name": "MP3BAHO61OBB", "color": "raise", "cards": MP3BAHO61OBBRAISE },
{ "name": "MP3BAHO61OBB", "color": "betfoldcall50", "cards": MP3BAHO61OBB4BETFOLDCALL50 },
{ "name": "MP3BAHO61OBB", "color": "betfold50", "cards": MP3BAHO61OBB4BETFOLD50 }
]



$(('input[type=radio][name=position]')).change(function () {
    markStartedHand();
});
$(('input[type=radio][name=stak]')).change(function () {
    markStartedHand();

});
$(('input[type=radio][name=action]')).change(function () {
    markStartedHand();
});
$(('input[type=radio][name=position-enemy]')).change(function () {
    markStartedHand();
});
$(('input[type=radio][name=openstak]')).change(function () {
    markStartedHand();
});
$(('input[type=radio][name=bbstak]')).change(function () {
    markStartedHand();
});
$(('input[type=radio][name=3bstak]')).change(function () {
    markStartedHand();
});

$(function () {
    markStartedHand();
    test();
});

function markStartedHand() {
    var position = $('input[name=position]:checked').val();
    var action = $('input[name=action]:checked').val();
    var stak = $('input[name=stak]:checked').val();
    var enemyposition = $('input[name=position-enemy]:checked').val();
    var openstak = $('input[name=openstak]:checked').val();
    var bbstak = $('input[name=bbstak]:checked').val();
    var b3betstak = $('input[name=3bstak]:checked').val();
    console.log($('input[name=3bstak]:checked').val());
    if(action === 'OB') {
        $('#position-enemy-wrapper').show();
        $('#openlegend').show();
        $('#nolegend').hide();
        $('#openstack').show();
        $('#noopenstack').hide();
        $('#3bstak').hide();
        $('#b3blegend').hide();

        stak = '';
        b3betstak = '';
        $('#bbstak').hide();
        if(position !== 'BB') {
            bbstak = '';
        }
            console.log(""+$('input[name=3bstak]:checked').val());
    }
    else if (action === 'NOB') {
        $('#position-enemy-wrapper').hide();
        $('#openlegend').hide();
        $('#nolegend').show();
        $('#openstack').hide();
        $('#noopenstack').show();
        $('#bbstak').hide();
        $('#3bstak').hide();
        $('#b3blegend').hide();
    }
    else if(action === '3BAHO') {
        $('#position-enemy-wrapper').show();
        $('#3bstak').show();
        $('#b3blegend').show();
        $('#openlegend').hide();
        $('#nolegend').hide();
        $('#openstack').hide();
        $('#noopenstack').hide();
        stak = '';
        openstak = '';
        $('#bbstak').hide();
        if(position !== 'BB') {
            bbstak = '';
        }
    }
    if(position === "BB" && action === "OB") {
        $('#position-enemy-wrapper').show();
        $('#openlegend').show();
        $('#nolegend').hide();
        $('#openstack').hide();
        $('#noopenstack').hide();
        $('#bbstak').show();
        $('#3bstak').hide();
        $('#b3blegend').hide();
        stak = '';
        openstak = '';
        b3betstak = '';
    }
    if(action === 'NOB') {
        enemyposition = '';
        openstak = '';
        bbstak = '';
        b3betstak = '';
    }

    $('#main td').removeClass();
    var requiredList = position + action + stak + openstak+ bbstak+b3betstak + enemyposition;
    console.log(requiredList)
    for (var i = 0; i < fullList.length; i++) {
        if (fullList[i].name === requiredList) {
            $('#main td').each(function () {
                var id = $(this).attr('id');
                if (jQuery.inArray(id, fullList[i].cards) !== -1) {
                    $(this).addClass(fullList[i].color);
                }
            });
        }
    }
}

function test() {
	var cards = [];
	$("#main").on("click", "td", function() {
     var id = $(this).attr('id');
     cards.push(id);
     console.log(cards);
   });
	$("#btnx").on("click", function() {
     localStorage.setItem('testObject', JSON.stringify(cards));
   });
}